--------------------------------------------------
1.4
--------------------------------------------------

Database:

Import table roles;
Import table privileges;
Import table roles_privileges;
Import table users_roles;
Import table users_topics

ALTER TABLE `users` ADD `enabled` BOOLEAN NOT NULL DEFAULT TRUE AFTER `id`;

Make username unique

Add following to application.properties:
users.admin.default.name=
users.admin.default.password=
