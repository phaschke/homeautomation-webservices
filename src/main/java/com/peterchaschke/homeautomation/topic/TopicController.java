package com.peterchaschke.homeautomation.topic;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Topics")
@Validated
@RestController
@RequestMapping("/api/topics")
public class TopicController {
	
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private TopicResourceAssembler topicResourceAssembler;


	@GetMapping("")
	public ResponseEntity<CollectionModel<TopicDTO>> all() {

		List<Topic> topics = topicService.getAllTopicsForDisplay();
		
		return new ResponseEntity<>(topicResourceAssembler.toCollectionModel(topics), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> one(@PathVariable Long id) {

		Topic topic = topicService.getOneTopic(id);
		return new ResponseEntity<TopicDTO>(topicResourceAssembler.toModel(topic), HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<?> createTopic(@Valid @RequestBody TopicDTO topic) {
		
		Topic savedTopic = topicService.addTopic(topic);
		return new ResponseEntity<TopicDTO>(topicResourceAssembler.toModel(savedTopic), HttpStatus.CREATED);
	}

	@PutMapping("")
	public ResponseEntity<?> updateTopic(@Valid @RequestBody TopicDTO topic) {

		Topic updatedTopic = topicService.updateTopic(topic);
		return new ResponseEntity<TopicDTO>(topicResourceAssembler.toModel(updatedTopic), HttpStatus.OK);
	}

	@PatchMapping("/reorder")
	public ResponseEntity<?> updateTopicDisplayOrder(
			@RequestBody 
			@NotEmpty(message = "Topic list cannot be empty.")
			List<@Valid TopicDTO> topics) {
		
		List<Topic> updatedTopicList = topicService.updateTopicDisplayOrder(topics);
		return new ResponseEntity<>(topicResourceAssembler.toCollectionModel(updatedTopicList), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteTopic(@NotNull @PathVariable Long id) {

		topicService.deleteTopic(id);

		return ResponseEntity.noContent().build();
	}

}
