package com.peterchaschke.homeautomation.topic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
	
	List<Topic> findAllByOrderByDisplayOrderAsc();
	List<Topic> findByActiveAndUsers_IdOrderByDisplayOrderAsc(int active, Long userId);
	Topic findByTopic(String topic);

}
