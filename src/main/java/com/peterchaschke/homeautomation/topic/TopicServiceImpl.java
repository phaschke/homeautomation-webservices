package com.peterchaschke.homeautomation.topic;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.users.AppUser;
import com.peterchaschke.homeautomation.users.AppUserRepository;

@Service
@Transactional
public class TopicServiceImpl implements TopicService {

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	private ControlRepository controlRepository;

	@Autowired
	private AppUserRepository appUserRepository;

	public List<Topic> getAllTopics() {

		return topicRepository.findAll();
	}

	public List<Topic> getAllTopicsForDisplay() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {

			return topicRepository.findAllByOrderByDisplayOrderAsc();
		}
		if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_GUEST"))) {

			String username = auth.getName();
			AppUser appUser = appUserRepository.findByUsername(username);
			return topicRepository.findByActiveAndUsers_IdOrderByDisplayOrderAsc(1, appUser.getId());

		}

		return null;
	}

	public Topic getOneTopic(@NotNull Long id) {

		return topicRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("id", id, "No topics found."));
	}

	public Topic addTopic(@NotNull TopicDTO topic) {

		if (topicRepository.findByTopic(topic.getTopic()) != null) {
			throw new UniqueConstraintViolationException("topic", topic.getTopic(), "Topic must be a unique string.");
		}
		return topicRepository.save(topic.toEntity());
	}

	public Topic updateTopic(@NotNull TopicDTO topicDTO) {

		Topic existingTopic = topicRepository.findById(topicDTO.getId())
				.orElseThrow(() -> new ResourceNotFoundException("id", topicDTO.getId(), "No topics found."));

		Topic topicWithSameTopic = topicRepository.findByTopic(topicDTO.getTopic());
		if(topicWithSameTopic != null) {
			if (topicWithSameTopic.getId() != existingTopic.getId()) {
				throw new UniqueConstraintViolationException("topic", topicDTO.getTopic(),
						"Topic must be a unique string.");
			}
		}

		existingTopic.setTopic(topicDTO.getTopic());
		existingTopic.setActive(topicDTO.getActive());
		existingTopic.setTopicTitle(topicDTO.getTopicTitle());

		return topicRepository.save(existingTopic);
	}

	public List<Topic> updateTopicDisplayOrder(List<TopicDTO> topics) {

		List<Topic> updatedTopicList = new ArrayList<Topic>();

		topics.forEach(topic -> {

			Topic existingTopic = topicRepository.findById(topic.getId())
					.orElseThrow(() -> new ResourceNotFoundException("id", topic.getId(), "No topics found."));
			existingTopic.setDisplayOrder(topic.getDisplayOrder());

			Topic updatedTopic = topicRepository.save(existingTopic);

			updatedTopicList.add(updatedTopic);
		});

		return updatedTopicList;
	}

	public void deleteTopic(@NotNull Long id) {

		List<Control> controls = controlRepository.findAllByTopicIdOrderByDisplayOrderAsc(id);
		for (int i = 0; i < controls.size(); i++) {
			controlRepository.delete(controls.get(i));
		}

		topicRepository.deleteById(id);

	}
}
