package com.peterchaschke.homeautomation.topic;

import javax.validation.constraints.NotBlank;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.homeautomation.validators.ObjectNameConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonRootName(value = "topic")
@Relation(collectionRelation = "topics")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicDTO extends RepresentationModel<TopicDTO> {
	
	private Long id;
	
	private int active;
	
	private int displayOrder;
	
	@ObjectNameConstraint(message = "Topic name must be greater than length 1 and only include alphanumeric characters, dashes and underscores.")
	private String topic;
	
	@NotBlank(message = "Field topic title must not be blank.")
	private String topicTitle;
	
	
	public Topic toEntity() {
		
		Topic topic = new Topic();
		
		topic.setId(this.id);
		topic.setActive(this.active);
		topic.setDisplayOrder(this.displayOrder);
		topic.setTopic(this.topic);
		topic.setTopicTitle(this.topicTitle);
		
		return topic;
	}

}
