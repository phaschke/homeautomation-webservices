package com.peterchaschke.homeautomation.topic;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

public interface TopicService {
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<Topic> getAllTopicsForDisplay();
	
	@PreAuthorize("permitAll()")
	public Topic getOneTopic(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	Topic addTopic(TopicDTO newTopic);
	
	@PreAuthorize("hasRole('ADMIN')")
	public Topic updateTopic(TopicDTO updatedTopic);
	
	@PreAuthorize("hasRole('ADMIN')")
	public List<Topic> updateTopicDisplayOrder(List<TopicDTO> topics);
	
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteTopic(Long id);
}
