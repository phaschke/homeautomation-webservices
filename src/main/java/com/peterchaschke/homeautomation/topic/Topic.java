package com.peterchaschke.homeautomation.topic;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.peterchaschke.homeautomation.users.AppUser;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "topics")
@Getter
@Setter
@RequiredArgsConstructor
public class Topic {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int active;
	
	@Column(name = "display_order")
	private int displayOrder;

	@NotBlank
	@Column(unique=true)
	private String topic;

	@NotBlank
	private String topicTitle;
	
	@ManyToMany(mappedBy = "topics")
    @JsonBackReference
	private Collection<AppUser> users;
	
	//@OneToMany(mappedBy = "controls", fetch = FetchType.LAZY)
    //@JsonBackReference
	//private Collection<Control> controls;
	
	public Topic(Long id, String topic, String topicTitle, int active) {
		this.id = id;
		this.topic = topic;
		this.topicTitle = topicTitle;
		this.active = active;
	}
	
	public TopicDTO toDTO() {
		
		TopicDTO topicDTO = new TopicDTO();
		
		topicDTO.setId(this.id);
		topicDTO.setActive(this.active);
		topicDTO.setDisplayOrder(this.displayOrder);
		topicDTO.setTopic(this.topic);
		topicDTO.setTopicTitle(this.topicTitle);
		
		return topicDTO;
	}

}
