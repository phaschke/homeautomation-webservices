package com.peterchaschke.homeautomation.topic;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class TopicResourceAssembler extends RepresentationModelAssemblerSupport<Topic, TopicDTO> {

	public TopicResourceAssembler() {
		super(TopicController.class, TopicDTO.class);
	}

	@Override
	public TopicDTO toModel(Topic entity) {

		TopicDTO topicModel = instantiateModel(entity);

		topicModel.add(linkTo(methodOn(TopicController.class).one(entity.getId())).withSelfRel());

		topicModel.setId(entity.getId());
		topicModel.setActive(entity.getActive());
		topicModel.setDisplayOrder(entity.getDisplayOrder());
		topicModel.setTopic(entity.getTopic());
		topicModel.setTopicTitle(entity.getTopicTitle());

		return topicModel;
	}

	@Override
	public CollectionModel<TopicDTO> toCollectionModel(Iterable<? extends Topic> entities) {

		CollectionModel<TopicDTO> topicsModel = super.toCollectionModel(entities);

		topicsModel.add(linkTo(methodOn(TopicController.class).all()).withSelfRel());

		return topicsModel;
	}
}