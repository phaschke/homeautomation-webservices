package com.peterchaschke.homeautomation.controls;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ControlActionParameter {

	private String parameterName;
	private Object parameter;

}
