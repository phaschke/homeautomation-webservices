package com.peterchaschke.homeautomation.controls;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.ActionExecutorObject;
import com.peterchaschke.mqttClient.MQTTClient;

public abstract class BaseControlService extends ActionExecutorObject {

	protected ControlRepository controlRepository;
	protected Control control;

	protected BaseControlService(Control control, ControlRepository controlRepository) {
		this.control = control;
		this.controlRepository = controlRepository;
	}

	public abstract String getStatusList(ArrayList<String> requestedStatuses) throws Exception;

	public abstract ControlResource updateState(JSONObject parsedStateJSON);

	public abstract ControlResource doAction(ControlAction action) throws Exception;

	public abstract ControlResource getDisplayStatus() throws Exception;

	//public abstract ActionConditionalExecutionResult executeIfAction(ParsedActionConditional parsedActionIf, boolean debug);

	//public abstract ControlActionThenExecutionResult executeThenAction(ParsedActionTask parsedActionThen,
	//		boolean debug);

	protected JSONObject makeRequest(JSONObject mqttRequestJSON) {

		String topic = control.getTopic().getTopic() + "/" + control.getSubtopic();
		MQTTClient mqttClient = new MQTTClient(control.getBroker(), topic);

		/*
		 * System.out.println("Sending message");
		 * System.out.println(control.getBroker()); System.out.println(topic);
		 * System.out.println("");
		 */

		String responseMessage = mqttClient.sendMessage(mqttRequestJSON);

		try {
			Thread.sleep(6);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return new JSONObject(responseMessage);
	}

	protected boolean isJSONArrayValid(String test) {
		try {
			new JSONObject(test);

		} catch (JSONException ex) {

			try {
				new JSONArray(test);

			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}

	protected ArrayList<String> getDisplayStatusList() {

		ArrayList<String> displayStatusList = new ArrayList<>();

		if (isJSONArrayValid(this.control.getStatusProperties())) {

			JSONArray statusArrayJSON = new JSONArray(this.control.getStatusProperties());

			for (int i = 0; i < statusArrayJSON.length(); i++) {

				ControlStatusProperty statusProperty = new ControlStatusProperty(statusArrayJSON.getJSONObject(i));
				if (statusProperty.isDisplay()) {
					displayStatusList.add(statusProperty.getStatus());
				}
			}
		}

		return displayStatusList;
	}

	protected boolean isStatusResponseError(JSONObject responseJSON) {

		if (responseJSON.has("error")) {
			return true;
		}
		return false;
	}

	protected boolean inputStatusListEmpty(ArrayList<String> requestedStatuses) {
		return (requestedStatuses.size() == 0);
	}

	protected JSONObject buildStatusRequestJSON(ArrayList<String> requestedStatuses) {

		JSONArray statusArrayJSON = new JSONArray(requestedStatuses);

		JSONObject mqttRequestJSON = new JSONObject();
		mqttRequestJSON.put("status", statusArrayJSON);

		return mqttRequestJSON;
	}

	protected boolean compareActionStringValues(String comparator, String comparedTo, String actual) throws Exception {

		try {

			switch (comparator.toUpperCase()) {

			case "STR_EQ":

				return actual.equals(comparedTo);

			case "NUM_GT":
				return Double.parseDouble(actual) > Double.parseDouble(comparedTo);

			case "NUM_LT":
				return Double.parseDouble(actual) < Double.parseDouble(comparedTo);

			case "NUM_EQ":
				return Double.parseDouble(actual) == Double.parseDouble(comparedTo);

			case "NUM_GE":
				return Double.parseDouble(actual) >= Double.parseDouble(comparedTo);

			case "NUM_LE":
				return Double.parseDouble(actual) <= Double.parseDouble(comparedTo);

			default:
				throw new Exception("Invalid comparator value");
			}

		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			
			throw new Exception("Failed to compare if values: " + e.getMessage());

		}

	}

}
