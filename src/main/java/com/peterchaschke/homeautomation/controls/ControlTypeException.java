package com.peterchaschke.homeautomation.controls;

public class ControlTypeException extends RuntimeException {

	private static final long serialVersionUID = 5440866662192166962L;

	ControlTypeException(Long id, String action) {
		super("Failed to perfom action: " + action + ", on control " + id + ". Invalid control type");
	}

}
