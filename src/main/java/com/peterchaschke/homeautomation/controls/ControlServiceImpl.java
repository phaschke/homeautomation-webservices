package com.peterchaschke.homeautomation.controls;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.peterchaschke.homeautomation.InvalidPermissionsException;
import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.topic.Topic;
import com.peterchaschke.homeautomation.topic.TopicRepository;
import com.peterchaschke.homeautomation.topic.TopicService;

@Service
public class ControlServiceImpl implements ControlService {

	@Autowired
	private ControlRepository controlRepository;
	
	@Autowired
	private TopicRepository topicRepository;
	
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private ControlServiceFactory serviceFactory;
	
	@Autowired
	private ControlBuilderServiceFactory builderServiceFactory;

	
	public List<Control> getAllControls() {

		return controlRepository.findAll();
	}

	public List<Control> getControlsByTopicId(@NotNull Long topicId, Optional<Boolean> active, 
			Optional<Boolean> ignorePageSize, Pageable pageable) {
		
		Topic topic = topicRepository.findById(topicId)
				.orElseThrow(() -> new ResourceNotFoundException("topicId", topicId, "No topics with id found."));
		
		List<Topic> grantedTopics = topicService.getAllTopicsForDisplay();
		if(grantedTopics.contains(topic) == false) {
			throw new InvalidPermissionsException("topic", topic.getId(), "User does not have permissions for topic");
		}
		
		int totalControls = controlRepository.countAllByTopicId(topicId);
		
		if(ignorePageSize.isPresent() && (ignorePageSize.get() == true)) {
			pageable = PageRequest.of(0, totalControls+1, pageable.getSort());
		}

		List<Control> foundControls = null;

		// Only get active
		if (active.isPresent() && (active.get() == true)) {
			foundControls = controlRepository.findAllByTopicIdAndActiveOrderByDisplayOrderAsc(topic.getId(), 1, pageable);
		} else {
			foundControls = controlRepository.findAllByTopicIdOrderByDisplayOrderAsc(topic.getId(), pageable);
		}

		return foundControls;
		
	}

	public Control getOneControl(@NotNull Long id) {

		Control control = controlRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("controlId", id, "No controls found."));
		
		List<Topic> grantedTopics = topicService.getAllTopicsForDisplay();
		if(grantedTopics.contains(control.getTopic()) == false) {
			throw new InvalidPermissionsException("control", control.getId(), "User does not have permissions for topic associated with control");
		}
		
		return control;
	}
	
	public Control createControl(@NotNull ControlResource controlResource) {
		
		controlResource.setId(null);
		
		Topic topic = topicRepository.findById(controlResource.getTopicId())
				.orElseThrow(() -> new ResourceNotFoundException("topicId", controlResource.getTopicId(), "No topics with id found."));
		controlResource.setTopic(topic);
		
		if (controlRepository.findByUniqueName(controlResource.getUniqueName()) != null) {
			throw new UniqueConstraintViolationException("uniqueName", controlResource.getUniqueName(), "Control name must be a unique string.");
		}

		BaseControlBuilderService builderService = builderServiceFactory.createControlBuilderService(controlResource);

		return builderService.createControl();
	}
	
	public Control updateControl(@NotNull ControlResource updatedControlDTO) {

		Control control = controlRepository.findById(updatedControlDTO.getId())
				.orElseThrow(() -> new ResourceNotFoundException("controlId", updatedControlDTO.getId(), "No controls found."));
		
		Topic topic = topicRepository.findById(updatedControlDTO.getTopicId())
				.orElseThrow(() -> new ResourceNotFoundException("topicId", updatedControlDTO.getTopicId(), "No topics with id found."));
		updatedControlDTO.setTopic(topic);
		
		Control controlWithSameName = controlRepository.findByUniqueName(updatedControlDTO.getUniqueName());
		if (controlWithSameName.getId() != control.getId()) {
			throw new UniqueConstraintViolationException("uniqueName", control.getUniqueName(), "Control name must be a unique string.");
		}

		BaseControlBuilderService builderService = builderServiceFactory.createControlBuilderService(control.toResource());

		return builderService.editControl(updatedControlDTO);
	}

	public List<Control> updateControlDisplayOrder(@NotEmpty List<ControlResource> controlResources) {

		List<Control> updatedControlList = new ArrayList<Control>();

		controlResources.forEach(controlResource -> {
			
			Control existingControl = controlRepository.findById(controlResource.getId())
					.orElseThrow(() -> new ResourceNotFoundException("controlId", controlResource.getId(), "No controls found."));

			existingControl.setDisplayOrder(controlResource.getDisplayOrder());

			controlRepository.save(existingControl);

			updatedControlList.add(existingControl);
		});

		return updatedControlList;
	}
	
	public void deleteControl(@NotNull Long controlId) {

		Optional<Control> optControl = controlRepository.findById(controlId);
		if(optControl.isEmpty()) {
			return;
		}
		controlRepository.flush();
		
		BaseControlBuilderService builderService = builderServiceFactory.createControlBuilderService(optControl.get().toResource());
		builderService.deleteControl();
	}
	
	public ControlResource doAction(Long controlId, ControlAction action) throws Exception {
		
		Control control = controlRepository.findById(controlId)
				.orElseThrow(() -> new ResourceNotFoundException("controlId", controlId, "No controls found."));
		BaseControlService controlService = serviceFactory.createControlService(control);
		
		return controlService.doAction(action);
	}
	
	
	public ControlResource getStatus(Long controlId) throws Exception {
		
		Control control = controlRepository.findById(controlId)
				.orElseThrow(() -> new ResourceNotFoundException("controlId", controlId, "No controls found."));
		
		BaseControlService controlService = serviceFactory.createControlService(control);

		return controlService.getDisplayStatus();
		
	}
	
	public ControlResource updateControlState(Long controlId, String state) throws Exception {

		Control control = controlRepository.findById(controlId)
				.orElseThrow(() -> new ResourceNotFoundException("controlId", controlId, "No controls found."));
		
		JSONObject stateJSON = parseStringToJSON(state);
	
		BaseControlService controlService = serviceFactory.createControlService(control);
		
		return controlService.updateState(stateJSON);
		
	}
	
	private JSONObject parseStringToJSON(String state) throws Exception {

		try {
			JSONObject stateJSON = new JSONObject(state);
			return stateJSON;

		} catch (Exception e) {

			throw new Exception(e);
		}
	}

}
