package com.peterchaschke.homeautomation.controls;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ControlAction {

	private String action;
	private List<ControlActionParameter> parameters;
}
