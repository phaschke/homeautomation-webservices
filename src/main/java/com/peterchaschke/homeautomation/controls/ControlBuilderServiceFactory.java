package com.peterchaschke.homeautomation.controls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.controls.H801.H801BuilderService;
import com.peterchaschke.homeautomation.controls.relayswitch.SwitchBuilderService;
import com.peterchaschke.homeautomation.controls.sensor.SensorBuilderService;
import com.peterchaschke.homeautomation.event.EventRepository;

@Service
public class ControlBuilderServiceFactory {

	@Autowired
	private ControlRepository controlRepository;

	@Autowired
	private ActionRepository actionRepository;

	@Autowired
	private EventRepository eventRepository;

	public BaseControlBuilderService createControlBuilderService(ControlResource control) {

		BaseControlBuilderService controlBuilderService = null;

		switch (control.getType()) {

		case SWITCH:
			controlBuilderService = createSwitchBuilderService(control);
			break;
		case SENSOR:
			controlBuilderService = createStatusBuilderService(control);
			break;
		case LED_CONTROLLER:
			controlBuilderService = createLEDControllerBuilderService(control);
			break;
		}

		return controlBuilderService;

	}

	public SwitchBuilderService createSwitchBuilderService(ControlResource control) {

		if (control.getType() != ControlTypes.SWITCH) {
			throw new ControlTypeException(control.getId(), "Failed to create switch builder service");
		}

		return new SwitchBuilderService(control, controlRepository, actionRepository, eventRepository);
	}

	public SensorBuilderService createStatusBuilderService(ControlResource control) {

		if (control.getType() != ControlTypes.SENSOR) {
			throw new ControlTypeException(control.getId(), "Failed to create status builder service");
		}

		return new SensorBuilderService(control, controlRepository, actionRepository, eventRepository);
	}

	public H801BuilderService createLEDControllerBuilderService(ControlResource control) {

		if (control.getType() != ControlTypes.LED_CONTROLLER) {
			throw new ControlTypeException(control.getId(), "Failed to create LED Controller builder service");
		}

		return new H801BuilderService(control, controlRepository, actionRepository, eventRepository);
	}

}
