package com.peterchaschke.homeautomation.controls;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ControlService {

	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<Control> getAllControls();

	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<Control> getControlsByTopicId(Long topicId, Optional<Boolean> active, Optional<Boolean> ignorePageSize, Pageable pageable);

	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public Control getOneControl(Long id);

	@PreAuthorize("hasRole('ADMIN')")
	public Control createControl(ControlResource control);

	@PreAuthorize("hasRole('ADMIN')")
	public Control updateControl(ControlResource updatedControl);

	@PreAuthorize("hasRole('ADMIN')")
	public List<Control> updateControlDisplayOrder(List<ControlResource> controlDTOs);

	@PreAuthorize("hasRole('ADMIN')")
	public void deleteControl(Long controlId);

	@PreAuthorize("permitAll()")
	public ControlResource doAction(Long controlId, ControlAction actions) throws Exception;

	@PreAuthorize("permitAll()")
	public ControlResource updateControlState(Long controlId, String state) throws Exception;

	@PreAuthorize("permitAll()")
	public ControlResource getStatus(Long controlId) throws Exception;

}
