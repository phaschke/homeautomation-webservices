package com.peterchaschke.homeautomation.controls;

import org.json.JSONObject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class ControlStatusProperty {

	private static final String STATUS_PROPERTY_TITLE_KEY = "title";
	private static final String STATUS_PROPERTY_STATUS_KEY = "status";
	private static final String STATUS_PROPERTY_DISPLAY_KEY = "display";
	private static final String STATUS_PROPERTY_EDITABLE_KEY = "editable";
	private static final String STATUS_PROPERTY_FORMATTER_KEY = "formatter";

	private String title;
	private String status;
	private boolean display;
	private boolean editable;
	private String formatter;

	public ControlStatusProperty(JSONObject jsonObject) {

		if (jsonObject.has(STATUS_PROPERTY_TITLE_KEY)) {
			this.title = jsonObject.getString(STATUS_PROPERTY_TITLE_KEY);
		}
		if (jsonObject.has(STATUS_PROPERTY_STATUS_KEY)) {
			this.status = jsonObject.getString(STATUS_PROPERTY_STATUS_KEY);
		}
		if (jsonObject.has(STATUS_PROPERTY_DISPLAY_KEY)) {
			this.display = jsonObject.getBoolean(STATUS_PROPERTY_DISPLAY_KEY);
		}
		if (jsonObject.has(STATUS_PROPERTY_EDITABLE_KEY)) {
			this.editable = jsonObject.getBoolean(STATUS_PROPERTY_EDITABLE_KEY);
		}
		if (jsonObject.has(STATUS_PROPERTY_FORMATTER_KEY)) {
			this.formatter = jsonObject.getString(STATUS_PROPERTY_FORMATTER_KEY);
		}
	}
}
