package com.peterchaschke.homeautomation.controls.relayswitch;

import java.util.ArrayList;
import java.util.Optional;

import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.ActionConditionalExecutionResult;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionConditional;
import com.peterchaschke.homeautomation.action.ParsedActionParameter;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlAction;
import com.peterchaschke.homeautomation.controls.ControlActionParameter;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.InvalidControlActionException;

public class SwitchService extends BaseControlService {

	private final String TOGGLE_SUCCESS_KEY = "toggleSuccess";

	public SwitchService(Control control, ControlRepository controlRepository) {
		super(control, controlRepository);
	}

	@Override
	public ControlResource doAction(ControlAction action) throws Exception {

		JSONObject responseStatus = new JSONObject();

		switch (action.getAction()) {

		case "TOGGLE_ON":

			Optional<Integer> duration = Optional.empty();

			for (int i = 0; i < action.getParameters().size(); i++) {
				ControlActionParameter parameter = action.getParameters().get(i);
				if (parameter.getParameterName().equals("duration")) {
					if (parameter.getParameter() != null) {
						int durationParam = (int) parameter.getParameter();
						duration = Optional.of(durationParam);
					}
				}
			}

			try {
				turnOn(duration);

			} catch (Exception e) {

				responseStatus.put("error", e.getMessage());
			}

			break;

		case "TOGGLE_OFF":

			try {
				turnOff();
			} catch (Exception e) {
				responseStatus.put("error", e.getMessage());
			}

			break;

		default:

			throw new InvalidControlActionException("Control Action", action.getAction(),
					"Invalid control action for Switch");

		}

		ControlResource controlResource = control.toResource();
		controlResource.setStatus(responseStatus.toString());

		return controlResource;

	}

	@Override
	public ControlResource updateState(JSONObject parsedStateJSON) {
		return control.toResource();
	}

	@Override
	public ControlResource getDisplayStatus() throws Exception {

		ArrayList<String> displayStatusList = getDisplayStatusList();

		ControlResource controlResource = this.control.toResource();

		String statusResult = "";
		try {
			statusResult = getStatusList(displayStatusList);

		} catch (Exception e) {

			JSONObject parsedResponseJSON = new JSONObject();
			parsedResponseJSON.put("error", e.getMessage());

			statusResult = parsedResponseJSON.toString();
		}

		controlResource.setStatus(statusResult);

		return controlResource;
	}

	public void turnOn(Optional<Integer> duration) throws Exception {

		JSONObject mqttRequestJSON = generateToggleOnMQTTRequest(duration);

		try {

			JSONObject response = makeRequest(mqttRequestJSON);

			if (!(response.getInt(TOGGLE_SUCCESS_KEY) == 1)) {
				throw new Exception("Device response code not 1");
			}

			// If persist state update state of control
			if (control.getPersistState() == 1) {

				control.setState(generateToggleOnState(duration));
				control = controlRepository.save(control);

			}

		} catch (Exception e) {

			throw new Exception("Failed to toggle control on: " + e.getMessage());
		}

	}

	public void turnOff() throws Exception {

		JSONObject mqttRequestJSON = generateToggleOffMQTTRequest();

		try {

			JSONObject response = makeRequest(mqttRequestJSON);

			if (!(response.getInt(TOGGLE_SUCCESS_KEY) == 1)) {
				throw new Exception("Failed to toggle control off. Device response code not 1");
			}

			// If persist state update state of control
			if (control.getPersistState() == 1) {

				control.setState(generateToggleOffState());
				control = controlRepository.save(control);
			}

		} catch (Exception e) {

			throw new Exception("Failed to toggle control off.");
		}

	}

	private JSONObject generateToggleOnMQTTRequest(Optional<Integer> duration) {

		JSONObject mqttRequestParametersJSON = new JSONObject();
		mqttRequestParametersJSON.put("state", 1);

		if (durationPresent(duration)) {
			mqttRequestParametersJSON.put("duration", duration.get());
		}

		JSONObject mqttRequestJSON = new JSONObject();
		mqttRequestJSON.put("toggle", mqttRequestParametersJSON);

		return mqttRequestJSON;
	}

	private JSONObject generateToggleOffMQTTRequest() {

		JSONObject mqttRequestParametersJSON = new JSONObject();
		mqttRequestParametersJSON.put("state", -1);
		JSONObject mqttRequestJSON = new JSONObject();
		mqttRequestJSON.put("toggle", mqttRequestParametersJSON);

		return mqttRequestJSON;
	}

	private String generateToggleOnState(Optional<Integer> duration) {

		JSONObject stateJSON = new JSONObject();
		stateJSON.put("relay", 1);
		if (durationPresent(duration)) {
			stateJSON.put("timer", duration.get());
		} else {
			stateJSON.put("timer", 0);
		}
		return stateJSON.toString();
	}

	private String generateToggleOffState() {

		JSONObject stateJSON = new JSONObject();
		stateJSON.put("relay", 0);
		return stateJSON.toString();
	}

	private boolean durationPresent(Optional<Integer> duration) {
		if (duration.isPresent()) {
			return true;
		}
		return false;
	}

	public String getStatusList(ArrayList<String> requestedStatuses) throws Exception {

		if (inputStatusListEmpty(requestedStatuses)) {
			throw new Exception("No requested statuses.");
		}

		JSONObject responseJSON = makeRequest(buildStatusRequestJSON(requestedStatuses));

		if (isStatusResponseError(responseJSON)) {

			throw new Exception(responseJSON.getString("error").toString());
		}

		JSONObject parsedResponseJSON = parseStatusListResponse(responseJSON);

		return parsedResponseJSON.toString();
	}

	/**
	 * TODO: Plan to make the persisted status fields dynamic. As of today they are
	 * hard coded for relay and timer.
	 */
	private JSONObject parseStatusListResponse(JSONObject responseJSON) {

		JSONObject persistStateRequest = new JSONObject();

		JSONObject statusObject = responseJSON.getJSONObject("status");

		// Make this use a dynamic list of persisted fields...
		if (control.getPersistState() == 1 && statusObject.has("relay")) {
			persistStateRequest.put("relay", statusObject.get("relay"));
		}
		if (control.getPersistState() == 1 && statusObject.has("timer")) {
			persistStateRequest.put("timer", statusObject.get("timer"));
		}

		control.setState(persistStateRequest.toString());
		controlRepository.save(control);

		return statusObject;

	}

	@Override
	public ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf, boolean debug) {

		switch (parsedActionIf.getConditional()) {

		case "GET_VALUE":

			return getValue(parsedActionIf);

		case "GET_STATUS":

			return getStatus(parsedActionIf);

		default:

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
					parsedActionIf.getParameter(), "Unknown control IF action");
		}
	}

	private ActionConditionalExecutionResult getValue(ParsedActionConditional parsedActionIf) {

		String comparator = parsedActionIf.getComparator();
		String input = parsedActionIf.getValue();
		String field = parsedActionIf.getParameter();

		try {

			if (field.equals("active")) {

				String output = String.valueOf(control.getActive());

				if (compareActionStringValues(comparator, input, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, input,
							comparator, output);

				} else {
					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, input,
							comparator, output);
				}
			}

			if (field.startsWith("state")) {

				// Parameter key in form of state.jsonProperty
				if (field.split("\\.").length > 0) {
					String jsonProperty = field.split("\\.")[1];

					JSONObject parsedState = new JSONObject(control.getState());

					if (jsonProperty.equals("relay") || jsonProperty.equals("timer")) {
						String output = String.valueOf(parsedState.getInt(jsonProperty));

						// compare the whole state
						if (compareActionStringValues(comparator, input, output)) {

							return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
									input, comparator, output);

						} else {

							return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
									input, comparator, output);
						}

					} else {
						throw new Exception("Unknown or unimplemented state parameter");
					}

				} else {

					String output = control.getState();

					// compare the whole state
					if (compareActionStringValues(comparator, input, output)) {

						return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, input,
								comparator, output);

					} else {

						return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
								input, comparator, output);
					}
				}

			}

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, "Unknown value");

		} catch (Exception e) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, e.getMessage());
		}
	}

	private ActionConditionalExecutionResult getStatus(ParsedActionConditional parsedActionIf) {

		String comparator = parsedActionIf.getComparator();
		String input = parsedActionIf.getValue();
		String field = parsedActionIf.getParameter();

		ArrayList<String> requestedStatus = new ArrayList<String>();
		requestedStatus.add(parsedActionIf.getParameter());

		String statusResult = "";
		try {
			statusResult = getStatusList(requestedStatus);

		} catch (Exception e) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, e.getMessage());

		}

		JSONObject responseJSON = new JSONObject(statusResult);

		if (responseJSON.has(field)) {

			try {
				String output = responseJSON.get(field).toString();

				if (compareActionStringValues(comparator, input, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, input,
							comparator, output);

				} else {

					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field, input,
							comparator, output);
				}

			} catch (Exception e) {

				return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
						e.getMessage());

			}
		}
		return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
				"Response missing field");
		// }
	}

	private ActionConditionalExecutionResult generateTrueIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(true);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "TRUE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateFalseIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "FALSE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateErrorIfReturnMessage(String controlTitle, String action,
			String parameter, String error) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(true);
		result.setResultMessage(
				String.format("IF>> [%s] %s(%s): ERROR: %s", new Object[] { controlTitle, action, parameter, error }));

		return result;
	}

	@Override
	public ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskMode, boolean debug) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();

		String format = "";
		Object[] formatParameters = {};
		boolean isError = false;
		ArrayList<ParsedActionParameter> parameters;

		switch (parsedActionThen.getAction()) {

		case "SET_VALUE":

			return setValueThen(parsedActionThen, taskMode);

		case "TOGGLE_ON":

			try {

				Optional<Integer> duration = Optional.empty();

				parameters = parsedActionThen.getParameters();

				for (int i = 0; i < parameters.size(); i++) {

					ParsedActionParameter parameter = parameters.get(i);
					if (parameter.getKey().equals("duration")) {
						if (!parameter.getValue().isEmpty()) {
							duration = Optional.of(Integer.parseInt(parameter.getValue()));
						}
					}
				}

				turnOn(duration);

				if (duration.isPresent()) {
					format = "%s>> [%s] %s: %s = %s";
					formatParameters = new Object[] { taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON", "duration",
							duration.get().toString() };
				} else {
					format = "%s>> [%s] %s";
					formatParameters = new Object[] { taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON" };
				}

				isError = false;

			} catch (Exception e) {

				if (debug) {
					format = "%s>> [%s] %s: ERROR: %s";
					formatParameters = new Object[] { taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON", e.getMessage() };
				}
				isError = true;
			}

			break;

		case "TOGGLE_OFF":

			try {

				turnOff();

				format = "%s>> [%s] %s";
				formatParameters = new Object[] { taskMode.toUpperCase(), control.getTitle(), "TOGGLE_OFF" };

			} catch (Exception e) {

				if (debug) {
					format = "%s>> [%s] %s: ERROR: %s";
					formatParameters = new Object[] { taskMode.toUpperCase(), control.getTitle(), "TOGGLE_OFF", e.getMessage() };
				}
				isError = true;

			}

			break;

		default:

		}

		result.setErrorFlag(isError);

		if (debug) {
			String debugMessage = String.format(format, formatParameters);
			result.setResultMessage(debugMessage);
		}

		return result;
	}

	private ActionTaskExecutionResult setValueThen(ParsedActionTask parsedActionThen, String taskMode) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();
		ArrayList<ParsedActionParameter> parameters = parsedActionThen.getParameters();

		try {

			ParsedActionParameter parameter = parameters.get(0);
			String value = parameter.getValue();
			String field = "";

			String key = parameter.getKey().toLowerCase();

			if (key.equals("active")) {

				int activeFlag = 0;
				if (value.equals("1"))
					activeFlag = 1;

				field = "active";

				control.setActive(activeFlag);
				controlRepository.save(control);

			}

			if (key.startsWith("state")) {

				field = "state";

				JSONObject parsedState = new JSONObject(control.getState());

				// Parameter key in form of state.jsonProperty
				if (key.split("\\.").length > 1) {

					String jsonProperty = key.split("\\.")[1];

					parsedState.put(jsonProperty, value);

				} else {

					// Overwrite existing state
					parsedState = new JSONObject(value);
				}

				control.setState(parsedState.toString());
				controlRepository.save(control);
			}

			result.setErrorFlag(false);

			String debugMessage = String.format("%s>> [%s] %s: %s = %s", taskMode.toUpperCase(), control.getTitle(), "SET_VALUE", field,
					value);
			result.setResultMessage(debugMessage);

			return result;

		} catch (Exception e) {

			result.setErrorFlag(true);

			String debugMessage = String.format("%s>> [%s] %s: ERROR: %s", taskMode.toUpperCase(), control.getTitle(), "SET_VALUE",
					e.getMessage());
			result.setResultMessage(debugMessage);

			return result;

		}
	}

}
