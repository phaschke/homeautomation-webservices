package com.peterchaschke.homeautomation.controls.relayswitch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionObjectType;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.action.ParameterDefinition;
import com.peterchaschke.homeautomation.action.ParsedAction;
import com.peterchaschke.homeautomation.action.ParsedActionParameter;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.BaseControlBuilderService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlBuilderService;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlStatusProperty;
import com.peterchaschke.homeautomation.event.EventProperty;
import com.peterchaschke.homeautomation.event.EventRepository;

public class SwitchBuilderService extends BaseControlBuilderService implements ControlBuilderService {

	public SwitchBuilderService(ControlResource control, ControlRepository controlRepository,
			ActionRepository actionRepository, EventRepository eventRepository) {
		super(control, controlRepository, actionRepository, eventRepository);
	}

	@Override
	public Control createControl() {

		validateJSONProperties();

		generateControlProperties();
		generateControlState();

		Control createdControl = controlRepository.save(control.toEntity());
		control = createdControl.toResource();

		if (control.getPersistState() == 1) {
			addOrUpdatePersistanceEvents();
		}

		return control.toEntity();
	}

	@Override
	public Control editControl(ControlResource updatedControlDTO) {

		Control updatedControl = updatedControlDTO.toEntity();

		if (updatedControl.getPersistState() == 1) {
			addOrUpdatePersistanceEvents();
		} else {
			inactivatePersistanceEvents();
		}

		control.setTopicId(updatedControl.getTopicId());
		control.setActive(updatedControl.getActive());
		control.setDisplayOrder(updatedControl.getDisplayOrder());
		control.setTitle(updatedControl.getTitle());
		control.setUniqueName(updatedControl.getUniqueName());
		control.setType(updatedControl.getType());
		control.setBroker(updatedControl.getBroker());
		control.setSubtopic(updatedControl.getSubtopic());
		control.setPersistState(updatedControl.getPersistState());
		control.setProperties(updatedControl.getProperties());
		control.setEventProperties(updatedControl.getEventProperties());
		control.setStatusProperties(updatedControl.getStatusProperties());
		control.setActionProperties(updatedControl.getActionProperties());
		control.setDescription(updatedControl.getDescription());

		generateControlProperties();
		generateControlState();

		return controlRepository.save(control.toEntity());
	}

	private void generateControlProperties() {

		control.setEventProperties(generateEventProperties());
		control.setStatusProperties(generateStatusProperties());
		control.setActionProperties(generateActionProperties());
	}

	private void generateControlState() {

		if (control.getState() == null || isJSONObjectValid(control.getState()) == false) {
			control.setState(new JSONObject().toString());
		}

		JSONObject controlState = new JSONObject(control.getState());
		if (!controlState.has("relay")) {
			controlState.put("relay", 0);
		}
		if (!controlState.has("timer")) {
			controlState.put("timer", 0);
		}
		control.setState(controlState.toString());
	}

	private String generateEventProperties() {

		List<String> defaultEventNames = Arrays.asList("PRESS_ON", "PRESS_OFF", "TIMER_END");
		JSONArray eventProperties = new JSONArray();

		if (control.getEventProperties() != null) {

			JSONArray eventPropertiesJSONArray = new JSONArray(control.getEventProperties());

			for (int i = 0; i < eventPropertiesJSONArray.length(); i++) {
				JSONObject json = eventPropertiesJSONArray.getJSONObject(i);

				if (!json.has(EVENT_PROPERTY_NAME_KEY))
					continue;

				if (!defaultEventNames.contains(json.getString(EVENT_PROPERTY_NAME_KEY))) {
					EventProperty eventProperty = new EventProperty(json.getString(EVENT_PROPERTY_NAME_KEY).toString(),
							true);
					eventProperties.put(new JSONObject(eventProperty));
				}
			}
		}

		if (control.getPersistState() == 1) {

			defaultEventNames.forEach(event -> {
				EventProperty eventProperty = new EventProperty(event, false);
				eventProperties.put(new JSONObject(eventProperty));
			});
		}

		if (eventProperties.length() <= 0) {
			return null;
		}

		return eventProperties.toString();
	}

	private String generateStatusProperties() {

		List<String> defaultStatusNames = Arrays.asList("ping", "relay", "timer");

		JSONArray statusProperties = new JSONArray();

		if (control.getStatusProperties() != null) {

			JSONArray statusPropertiesJSONArray = new JSONArray(control.getStatusProperties());

			for (int i = 0; i < statusPropertiesJSONArray.length(); i++) {
				JSONObject json = statusPropertiesJSONArray.getJSONObject(i);

				if (!json.has(STATUS_PROPERTY_STATUS_KEY))
					continue;

				if (!defaultStatusNames.contains(json.getString(STATUS_PROPERTY_STATUS_KEY))) {

					String title = json.getString(STATUS_PROPERTY_TITLE_KEY);
					String status = json.getString(STATUS_PROPERTY_STATUS_KEY);
					boolean display = json.getBoolean(STATUS_PROPERTY_DISPLAY_KEY);
					String formatter = json.getString(STATUS_PROPERTY_FORMATTER_KEY);

					ControlStatusProperty statusProperty = new ControlStatusProperty(title, status, display, true,
							formatter);

					statusProperties.put(new JSONObject(statusProperty));
				}
			}
		}

		ControlStatusProperty statusProperty = new ControlStatusProperty("Ping", "ping", false, false, "");
		statusProperties.put(new JSONObject(statusProperty));
		statusProperty = new ControlStatusProperty("Relay", "relay", true, false, "");
		statusProperties.put(new JSONObject(statusProperty));
		statusProperty = new ControlStatusProperty("Timer", "timer", true, false, "");
		statusProperties.put(new JSONObject(statusProperty));

		if (statusProperties.length() <= 0) {
			return null;
		}

		return statusProperties.toString();
	}

	private String generateActionProperties() {

		JSONObject allowedActionsJSON = generateDefaultActionProperties();

		allowedActionsJSON.put(ALLOWED_ACTION_TASK_KEY, generateTaskDefintions());
		allowedActionsJSON.put(ALLOWED_ACTION_CONDITIONAL_KEY, generateConditionalDefinitions());
		
		return allowedActionsJSON.toString();
	}
	
	private JSONObject generateConditionalDefinitions() {
		
		JSONObject conditionalDefinitionObject = new JSONObject();
		conditionalDefinitionObject.put("GET_STATUS", new JSONObject(generateGetStatusParameterDefinition()));
		conditionalDefinitionObject.put("GET_VALUE", new JSONObject(generateGetValueParameterDefinition()));
		
		return conditionalDefinitionObject;
	}
	
	private ParameterDefinition generateGetStatusParameterDefinition() {
		
		ArrayList<String> statusArray = getStatusList();
		return new ParameterDefinition(statusArray, true);
	}
	
	private ParameterDefinition generateGetValueParameterDefinition() {
		
		ArrayList<String> parameterKeys = new ArrayList<String>(Arrays.asList("state", "state.timer", "state.relay", "active"));
		return new ParameterDefinition(parameterKeys, true);
	}

	private JSONObject generateTaskDefintions() {

		JSONObject taskDefinitionObject = new JSONObject();

		taskDefinitionObject.put("TOGGLE_ON", new JSONObject(generateToggleOnParameterDefintion()));
		taskDefinitionObject.put("TOGGLE_OFF", new JSONObject(generateToggleOffParameterDefintion()));
		taskDefinitionObject.put("SET_VALUE", new JSONObject(generateSetValueParameterDefintion()));

		return taskDefinitionObject;
	}

	private ParameterDefinition generateToggleOnParameterDefintion() {
		
		ArrayList<String> parameterKeys = new ArrayList<String>(Arrays.asList("duration"));
		return new ParameterDefinition(parameterKeys, false);
	}

	private ParameterDefinition generateToggleOffParameterDefintion() {
		
		return new ParameterDefinition();
	}

	private ParameterDefinition generateSetValueParameterDefintion() {
		
		ArrayList<String> parameterKeys = getValueArray();
		return new ParameterDefinition(parameterKeys, true);
	}

	private ArrayList<String> getValueArray() {

		ArrayList<String> valueArrayList = new ArrayList<String>();
		valueArrayList.add("state");
		valueArrayList.add("active");

		return valueArrayList;
	}

	public void addOrUpdatePersistanceEvents() {

		buildPersistanceEvent("PRESS_ON", new ParsedActionParameter("state", "state", "{\"relay\": 1, \"timer\": 0}"));
		buildPersistanceEvent("PRESS_OFF", new ParsedActionParameter("state", "state", "{\"relay\": 0}"));
		buildPersistanceEvent("TIMER_END", new ParsedActionParameter("state", "state", "{\"relay\": 0}"));
	}

	@Transactional
	private void buildPersistanceEvent(String eventName, ParsedActionParameter parsedActionParameter) {

		ArrayList<ParsedActionParameter> parsedActionParameterArray = new ArrayList<ParsedActionParameter>(
				Arrays.asList(parsedActionParameter));
		
		ParsedActionTask actionThen = new ParsedActionTask();
		actionThen.setObjectId(control.getId());
		actionThen.setObjectType(ActionObjectType.CONTROL);
		actionThen.setAction("SET_VALUE");
		actionThen.setParameters(parsedActionParameterArray);

		ArrayList<ParsedActionTask> parsedActionThenArray = new ArrayList<ParsedActionTask>(Arrays.asList(actionThen));

		ParsedAction parsedAction = new ParsedAction();
		parsedAction.setThens(parsedActionThenArray);

		Action savedAction = addOrUpdateAction(eventName, parsedAction);

		addOrUpdateEvent(eventName, savedAction);
	}

	private void inactivatePersistanceEvents() {

		inactivateEvent("PRESS_ON", GENERATED_RESOURCE_FLAG);
		inactivateEvent("PRESS_OFF", GENERATED_RESOURCE_FLAG);
		inactivateEvent("TIMER_END", GENERATED_RESOURCE_FLAG);
	}

}
