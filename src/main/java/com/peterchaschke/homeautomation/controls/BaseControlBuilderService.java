package com.peterchaschke.homeautomation.controls;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.homeautomation.MalformedDataException;
import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionExecution;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.action.ParsedAction;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.event.EventRepository;

public abstract class BaseControlBuilderService implements ControlBuilderService {

	protected static final String EVENT_PROPERTY_NAME_KEY = "name";

	protected static final String STATUS_PROPERTY_TITLE_KEY = "title";
	protected static final String STATUS_PROPERTY_STATUS_KEY = "status";
	protected static final String STATUS_PROPERTY_DISPLAY_KEY = "display";
	protected static final String STATUS_PROPERTY_EDITABLE_KEY = "editable";
	protected static final String STATUS_PROPERTY_FORMATTER_KEY = "formatter";

	protected static final String ALLOWED_ACTION_CONDITIONAL_KEY = "conditional";
	protected static final String ALLOWED_ACTION_TASK_KEY = "task";

	protected static final int GENERATED_RESOURCE_FLAG = 0;

	protected ControlResource control;
	protected ControlRepository controlRepository;
	protected ActionRepository actionRepository;
	protected EventRepository eventRepository;

	protected BaseControlBuilderService(ControlResource control, ControlRepository controlRepository,
			ActionRepository actionRepository, EventRepository eventRepository) {
		this.control = control;
		this.controlRepository = controlRepository;
		this.actionRepository = actionRepository;
		this.eventRepository = eventRepository;
	}

	public ControlResource getControl() {
		return this.control;
	}

	public abstract Control createControl();

	public abstract Control editControl(ControlResource updatedControl);

	@Transactional
	public void deleteControl() {

		String generatedActionName = "GENERATED-" + control.getId();
		List<Action> actionsToRemove = actionRepository.findByNameContainingAndUserCreated(generatedActionName, 0);
		actionsToRemove.forEach(action -> {
			actionRepository.deleteById(action.getId());
		});
		actionRepository.flush();

		List<Event> eventsToRemove = eventRepository.findByControlId(control.getId());
		eventsToRemove.forEach(event -> {
			eventRepository.deleteById(event.getId());
		});
		eventRepository.flush();

		controlRepository.deleteById(control.getId());
	}

	protected void validateJSONProperties() {

		if (control.getProperties() != null) {
			if (!isJSONObjectValid(control.getProperties())) {
				throw new MalformedDataException("Properties", control.getProperties(), "Could not parse JSONObject");
			}
		}

		if (control.getEventProperties() != null) {
			if (!isJSONArrayValid(control.getEventProperties())) {
				throw new MalformedDataException("EventProperties", control.getEventProperties(),
						"Could not parse JSONArray");
			}
		}

		if (control.getStatusProperties() != null) {
			if (!isJSONArrayValid(control.getStatusProperties())) {
				throw new MalformedDataException("StatusProperties", control.getStatusProperties(),
						"Could not parse JSONArray");
			}
		}

		if (control.getActionProperties() != null) {
			if (!isJSONObjectValid(control.getActionProperties())) {
				throw new MalformedDataException("ActionProperties", control.getActionProperties(),
						"Could not parse JSONObject");
			}
		}

	}

	protected boolean isJSONObjectValid(String test) {
		try {
			new JSONObject(test);

		} catch (JSONException ex) {

			return false;
		}
		return true;
	}

	private boolean isJSONArrayValid(String test) {
		try {
			new JSONObject(test);

		} catch (JSONException ex) {

			try {
				new JSONArray(test);

			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}

	protected Action addOrUpdateAction(String eventName, ParsedAction parsedAction) {

		JSONArray actionThenJsonObj = new JSONArray(parsedAction.getThens());

		String actionName = generateGeneratedActionName(eventName, control.getId());
		String actionTitle = generateGeneratedActionTitle(eventName, control.getUniqueName());

		Action existingAction = actionRepository.findByNameAndUserCreated(actionName, GENERATED_RESOURCE_FLAG);

		Action newAction = new Action();
		newAction.setName(actionName);
		newAction.setTitle(actionTitle);
		newAction.setIfExecution(ActionExecution.PARALLEL);
		newAction.setThenExecution(ActionExecution.SERIAL);
		newAction.setElseExecution(ActionExecution.PARALLEL);
		newAction.setThens(actionThenJsonObj.toString());
		newAction.setUserCreated(GENERATED_RESOURCE_FLAG);

		if (existingAction != null) {
			newAction.setId(existingAction.getId());
		}

		return actionRepository.save(newAction);
	}

	private String generateGeneratedActionName(String eventName, Long controlId) {

		StringBuilder sb = new StringBuilder();
		sb.append("GENERATED-");
		sb.append(controlId);
		sb.append("-");
		sb.append(eventName);

		return sb.toString();
	}

	private String generateGeneratedActionTitle(String eventName, String controlName) {

		StringBuilder sb = new StringBuilder();
		sb.append("Control-");
		sb.append(controlName.replaceAll("\\s", ""));
		sb.append("-");
		sb.append(eventName);

		return sb.toString();
	}

	protected Event addOrUpdateEvent(String eventName, Action action) {

		Event existingEvent = eventRepository.findByControlIdAndEventAndUserCreated(control.getId(), eventName,
				GENERATED_RESOURCE_FLAG);

		Event newEvent = new Event(control.getId(), action.getId(), eventName, 1, 0, "", "");

		if (existingEvent != null) {
			newEvent.setId(existingEvent.getId());
		}

		return eventRepository.save(newEvent);

	}

	public Event inactivateEvent(String eventName, int userCreated) {

		Event event = eventRepository.findByControlIdAndEventAndUserCreated(control.getId(), eventName, userCreated);
		if (event == null)
			return null;

		event.setActive(0);
		return eventRepository.save(event);
	}

	protected JSONObject generateDefaultActionProperties() {

		JSONObject allowedActionsJSON = new JSONObject();

		allowedActionsJSON.put(ALLOWED_ACTION_CONDITIONAL_KEY, new JSONObject());
		allowedActionsJSON.put(ALLOWED_ACTION_TASK_KEY, new JSONObject());

		return allowedActionsJSON;
	}
	
	protected ArrayList<String> getStatusList() {
		
		ArrayList<String> statusArray = new ArrayList<String>();
		
		if (control.getStatusProperties() != null) {
			
			JSONArray statusListJSON = new JSONArray(control.getStatusProperties());

			for (int i = 0; i < statusListJSON.length(); i++) {
				ControlStatusProperty statusProperty = new ControlStatusProperty(statusListJSON.getJSONObject(i));
				statusArray.add(statusProperty.getStatus());
			}
		}
		
		return statusArray;
	}

}
