package com.peterchaschke.homeautomation.controls;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Controls")
@Validated
@Transactional
@RestController
@RequestMapping("/api/controls")
public class ControlController {

	@Autowired
	private ControlService controlService;

	@Autowired
	private ControlResourceAssembler assembler;

	@GetMapping("")
	public ResponseEntity<CollectionModel<ControlResource>> all(@RequestParam("status") Optional<Integer> status) {

		List<Control> controls = controlService.getAllControls();

		return new ResponseEntity<>(assembler.toCollectionModel(controls), HttpStatus.OK);

	}

	@GetMapping("/bytopic/{topicId}")
	public ResponseEntity<CollectionModel<ControlResource>> controlsByTopic(@NotNull @PathVariable Long topicId,
			@RequestParam(name = "active", required = false) Optional<Boolean> active, @RequestParam("ignorePageSize") Optional<Boolean> ignorePageSize, Pageable pageable) {

		List<Control> controls = controlService.getControlsByTopicId(topicId, active, ignorePageSize, pageable);

		return new ResponseEntity<>(assembler.toCollectionModel(controls), HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public ResponseEntity<ControlResource> one(@PathVariable Long id) {

		Control control = controlService.getOneControl(id);

		return new ResponseEntity<ControlResource>(assembler.toModel(control), HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<ControlResource> createControl(@Valid @RequestBody ControlResource controlDTO) {

		Control createdControl = controlService.createControl(controlDTO);

		return new ResponseEntity<ControlResource>(assembler.toModel(createdControl), HttpStatus.CREATED);
	}

	@PutMapping("")
	public ResponseEntity<ControlResource> updateControl(@Valid @RequestBody ControlResource updatedControlDTO) {

		Control updatedControl = controlService.updateControl(updatedControlDTO);

		return new ResponseEntity<ControlResource>(assembler.toModel(updatedControl), HttpStatus.OK);
	}

	@PatchMapping("/reorder")
	public ResponseEntity<?> updateControlDisplayOrder(
			@RequestBody @NotEmpty(message = "Control list cannot be empty.") List<@Valid ControlResource> controlDTOs) {

		List<Control> updatedControlList = controlService.updateControlDisplayOrder(controlDTOs);
		return new ResponseEntity<>(assembler.toCollectionModel(updatedControlList), HttpStatus.OK);

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteControl(@NotNull @PathVariable Long id) throws Exception {

		controlService.deleteControl(id);

		return ResponseEntity.noContent().build();
	}

	@PatchMapping("/{id}/action")
	public ResponseEntity<?> doControlAction(@PathVariable Long id, @RequestBody ControlAction action)
			throws Exception {

		ControlResource controlResource = controlService.doAction(id, action);

		return new ResponseEntity<ControlResource>(assembler.addLink(controlResource), HttpStatus.OK);
	}

	@PatchMapping("/{id}/state")
	public ResponseEntity<?> updateControlState(@PathVariable Long id, @RequestBody String state) throws Exception {

		ControlResource controlResource = controlService.updateControlState(id, state);

		return new ResponseEntity<ControlResource>(assembler.addLink(controlResource), HttpStatus.OK);
	}

	@GetMapping("/{id}/status")
	public ResponseEntity<ControlResource> status(@PathVariable Long id) throws Exception {

		ControlResource controlResource = controlService.getStatus(id);

		return new ResponseEntity<ControlResource>(assembler.addLink(controlResource), HttpStatus.OK);
	}

}
