package com.peterchaschke.homeautomation.controls;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.peterchaschke.homeautomation.topic.Topic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "controls")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Control {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NotNull
	@Column(name = "topic_id", nullable = false)
	private Long topicId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "topic_id", insertable = false, updatable = false)
	@JsonBackReference
	private Topic topic;

	@NotBlank
	@Column(name = "unique_name", unique=true, nullable = false)
	private String uniqueName;
	
	@Column(name = "active")
	private int active;
	
	@Column(name = "display_order", nullable = false)
	private int displayOrder;

	@NotBlank
	@Column(name = "title", nullable = false)
	private String title;
	
	@NotNull
	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private ControlTypes type;

	@NotBlank
	@Column(name = "broker", nullable = false)
	private String broker;

	@NotBlank
	@Column(name = "subtopic", nullable = false)
	private String subtopic;

	@Column(name = "state")
	private String state;

	@Column(name = "persist_state", nullable = false)
	private int persistState;

	@Column(name = "description")
	private String description;
	
	@Column(name = "properties")
	private String properties;
	
	@Column(name = "event_properties")
	private String eventProperties;
	
	@Column(name = "status_properties")
	private String statusProperties;
	
	@Column(name = "action_properties")
	private String actionProperties;
	
	// TODO: JPA relation
	//private Set<Event> events;
	
	// TODO: JPA relation
	//private Set<Action> actions;
	
	public ControlResource toResource() {
		
		ControlResource controlResource = new ControlResource();
		
		controlResource.setId(this.id);
		controlResource.setTopicId(this.topicId);
		controlResource.setUniqueName(this.uniqueName);
		controlResource.setActive(this.active);
		controlResource.setDisplayOrder(this.displayOrder);
		controlResource.setTitle(this.title);
		controlResource.setType(this.type);
		controlResource.setBroker(this.broker);
		controlResource.setSubtopic(this.subtopic);
		controlResource.setDescription(this.description);
		controlResource.setState(this.state);
		controlResource.setPersistState(this.persistState);
		controlResource.setProperties(this.properties);
		controlResource.setEventProperties(this.eventProperties);
		controlResource.setStatusProperties(this.statusProperties);
		controlResource.setActionProperties(this.actionProperties);
		controlResource.setTopic(topic);
		// TODO: set after jpa relations are created
		controlResource.setEvents(null);
		controlResource.setActions(null);
		
		return controlResource;
	}

}
