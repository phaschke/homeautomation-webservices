package com.peterchaschke.homeautomation.controls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.controls.H801.H801Service;
import com.peterchaschke.homeautomation.controls.relayswitch.SwitchService;
import com.peterchaschke.homeautomation.controls.sensor.SensorService;

@Service
public class ControlServiceFactory {

	@Autowired
	private ControlRepository controlRepository;

	public BaseControlService createControlService(Control control) {

		BaseControlService controlService = null;

		switch (control.getType()) {

		case SWITCH:
			controlService = createSwitchService(control);
			break;
		case SENSOR:
			controlService = createStatusService(control);
			break;
		case LED_CONTROLLER:
			controlService = createLEDControllerService(control);
			break;
		}

		return controlService;

	}

	public SwitchService createSwitchService(Control control) {

		return new SwitchService(control, controlRepository);
	}

	public SensorService createStatusService(Control control) {

		return new SensorService(control, controlRepository);
	}

	public H801Service createLEDControllerService(Control control) {

		return new H801Service(control, controlRepository);
	}

}
