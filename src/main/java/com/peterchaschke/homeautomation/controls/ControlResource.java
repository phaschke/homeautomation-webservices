package com.peterchaschke.homeautomation.controls;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.topic.Topic;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@JsonRootName(value = "control")
@Relation(collectionRelation = "controls")
@Getter
@Setter
@RequiredArgsConstructor
public class ControlResource extends RepresentationModel<ControlResource> implements Serializable {

	private static final long serialVersionUID = 3706984549313131838L;

	private Long id;
	
	private Long topicId;
	
	@NotBlank(message = "Field uniqueName must not be blank.")
	private String uniqueName;
	
	private int active;
	
	private int displayOrder;
	
	@NotBlank(message = "Field title must not be blank.")
	private String title;
	
	@NotNull(message = "Field type must not be null.")
	private ControlTypes type;
	
	@NotBlank(message = "Field broker must not be blank.")
	private String broker;
	
	@NotBlank(message = "Field subtopic must not be blank.")
	private String subtopic;
	
	private String description;
	
	private String state;
	
	private int persistState = 0;
	
	private String properties;
	
	private String eventProperties;
	
	private String statusProperties;
	
	private String actionProperties;
	
	private String status;
	
	private Topic topic;
	
	private Set<Event> events;
	
	private Set<Action> actions;
	
	public Control toEntity() {
		
		Control control = new Control();
		
		control.setId(this.id);
		control.setTopicId(this.topicId);
		control.setTopic(topic);
		control.setUniqueName(this.uniqueName);
		control.setActive(this.active);
		control.setDisplayOrder(this.displayOrder);
		control.setTitle(this.title);
		control.setType(this.type);
		control.setBroker(this.broker);
		control.setSubtopic(this.subtopic);
		control.setProperties(this.properties);
		control.setEventProperties(this.eventProperties);
		control.setStatusProperties(this.statusProperties);
		control.setActionProperties(this.actionProperties);
		control.setDescription(this.description);
		control.setState(this.state);
		control.setPersistState(this.persistState);
		
		return control;
	}

}
