package com.peterchaschke.homeautomation.controls.H801;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionObjectType;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.action.ParameterDefinition;
import com.peterchaschke.homeautomation.action.ParsedAction;
import com.peterchaschke.homeautomation.action.ParsedActionParameter;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.BaseControlBuilderService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlBuilderService;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlStatusProperty;
import com.peterchaschke.homeautomation.event.EventProperty;
import com.peterchaschke.homeautomation.event.EventRepository;

public class H801BuilderService extends BaseControlBuilderService implements ControlBuilderService {

	private static final String RGB_KEY = "rgb";
	private static final String TUNABLE_KEY = "tunable";
	private static final String WW_KEY = "ww";
	private static final String CW_KEY = "cw";

	private static final String ENABLED_KEY = "enabled";

	private static final String BRIGHTNESS_KEY = "brightness";
	private static final String RGB_BRIGHTNESS_KEY = "rgb_brightness";
	private static final String TUNABLE_BRIGHTNESS_KEY = "tunable_brightness";
	private static final String WW_BRIGHTNESS_KEY = "ww_brightness";
	private static final String CW_BRIGHTNESS_KEY = "cw_brightness";
	private static final String LED_KEY = "led";

	public H801BuilderService(ControlResource control, ControlRepository controlRepository,
			ActionRepository actionRepository, EventRepository eventRepository) {
		super(control, controlRepository, actionRepository, eventRepository);
	}

	@Override
	public Control createControl() {

		JSONObject existingJSON = new JSONObject();
		JSONObject inputJSON = new JSONObject();
		String LEDState = generateLEDState(existingJSON, inputJSON);
		control.setState(LEDState);

		generateControlProperties();

		Control createdControl = controlRepository.save(control.toEntity());

		control = createdControl.toResource();

		generatePersistanceEvents();

		return control.toEntity();
	}

	@Override
	public Control editControl(ControlResource updatedControlResource) {

		Control updatedControl = updatedControlResource.toEntity();

		control.setTopicId(updatedControl.getTopicId());
		control.setActive(updatedControl.getActive());
		control.setDisplayOrder(updatedControl.getDisplayOrder());
		control.setTitle(updatedControl.getTitle());
		control.setUniqueName(updatedControl.getUniqueName());
		control.setType(updatedControl.getType());
		control.setBroker(updatedControl.getBroker());
		control.setSubtopic(updatedControl.getSubtopic());
		control.setPersistState(updatedControl.getPersistState());
		control.setProperties(updatedControl.getProperties());
		control.setEventProperties(updatedControl.getEventProperties());
		control.setStatusProperties(updatedControl.getStatusProperties());
		control.setActionProperties(updatedControl.getActionProperties());
		control.setDescription(updatedControl.getDescription());

		generateControlProperties();
		generatePersistanceEvents();

		JSONObject existingState = new JSONObject();
		if (control.getState() != null) {
			existingState = new JSONObject(control.getState());
		}
		JSONObject inputJSON = new JSONObject();
		String LEDState = generateLEDState(existingState, inputJSON);
		control.setState(LEDState);

		return controlRepository.save(control.toEntity());
	}

	private String generateLEDState(JSONObject existingJSON, JSONObject inputJSON) {

		H801State ledStatusObj = new H801State();
		ledStatusObj.parseJSON(existingJSON, inputJSON);
		JSONObject combinedState = new JSONObject(ledStatusObj);

		return combinedState.toString();
	}

	private void generateControlProperties() {

		control.setProperties(generateProperties());
		control.setEventProperties(generateEventProperties());
		control.setStatusProperties(generateStatusProperties());
		control.setActionProperties(generateActionProperties());
	}

	private String generateProperties() {

		if (control.getProperties() == null)
			return null;

		JSONObject incomingPropertiesJSON = new JSONObject(control.getProperties());

		if (!incomingPropertiesJSON.has("led")) {
			return incomingPropertiesJSON.toString();
		}

		JSONObject ledJSON = incomingPropertiesJSON.getJSONObject(LED_KEY);

		if (ledJSON.has(RGB_KEY)) {
			JSONObject rgbJSON = ledJSON.getJSONObject(RGB_KEY);
			mapBooleanIfNotExists(rgbJSON, ENABLED_KEY, false);
			mapBooleanIfNotExists(rgbJSON, BRIGHTNESS_KEY, false);
		}
		if (ledJSON.has(TUNABLE_KEY)) {
			JSONObject tunableJSON = ledJSON.getJSONObject(TUNABLE_KEY);
			mapBooleanIfNotExists(tunableJSON, ENABLED_KEY, false);
			mapBooleanIfNotExists(tunableJSON, BRIGHTNESS_KEY, false);
			mapIntegerIfNotExists(tunableJSON, "minTemp", 0);
			mapIntegerIfNotExists(tunableJSON, "maxTemp", 0);
		}
		if (ledJSON.has(WW_KEY)) {
			JSONObject wwJSON = ledJSON.getJSONObject(WW_KEY);
			mapBooleanIfNotExists(wwJSON, ENABLED_KEY, false);
			mapBooleanIfNotExists(wwJSON, BRIGHTNESS_KEY, false);
		}
		if (ledJSON.has(CW_KEY)) {
			JSONObject cwJSON = ledJSON.getJSONObject(CW_KEY);
			mapBooleanIfNotExists(cwJSON, ENABLED_KEY, false);
			mapBooleanIfNotExists(cwJSON, BRIGHTNESS_KEY, false);
		}

		incomingPropertiesJSON.put(LED_KEY, ledJSON);

		return incomingPropertiesJSON.toString();
	}

	private String generateEventProperties() {

		List<String> defaultEventNames = Arrays.asList("TIMER_END_RGB", "TIMER_END_TUNABLE", "TIMER_END_WW",
				"TIMER_END_CW");

		JSONArray eventProperties = new JSONArray();

		if (control.getEventProperties() != null) {

			JSONArray eventPropertiesJSONArray = new JSONArray(control.getEventProperties());

			for (int i = 0; i < eventPropertiesJSONArray.length(); i++) {
				JSONObject json = eventPropertiesJSONArray.getJSONObject(i);

				if (!json.has(EVENT_PROPERTY_NAME_KEY))
					continue;

				if (!defaultEventNames.contains(json.getString(EVENT_PROPERTY_NAME_KEY))) {
					EventProperty eventProperty = new EventProperty(json.getString(EVENT_PROPERTY_NAME_KEY).toString(),
							true);
					eventProperties.put(new JSONObject(eventProperty));
				}
			}
		}

		JSONObject ledPropertiesJSON = getLEDProperties();

		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, RGB_KEY, ENABLED_KEY)) {
			EventProperty eventProperty = new EventProperty("TIMER_END_RGB", false);
			eventProperties.put(new JSONObject(eventProperty));
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, TUNABLE_KEY, ENABLED_KEY)) {
			EventProperty eventProperty = new EventProperty("TIMER_END_TUNABLE", false);
			eventProperties.put(new JSONObject(eventProperty));
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, WW_KEY, ENABLED_KEY)) {
			EventProperty eventProperty = new EventProperty("TIMER_END_WW", false);
			eventProperties.put(new JSONObject(eventProperty));
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, CW_KEY, ENABLED_KEY)) {
			EventProperty eventProperty = new EventProperty("TIMER_END_CW", false);
			eventProperties.put(new JSONObject(eventProperty));
		}

		if (eventProperties.length() <= 0) {
			return null;
		}

		return eventProperties.toString();
	}

	private String generateStatusProperties() {
		List<String> defaultStatusNames = new ArrayList<String>();
		defaultStatusNames.addAll(Arrays.asList("ping", "state", "timers"));

		JSONObject ledPropertiesJSON = getLEDProperties();

		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, RGB_KEY, ENABLED_KEY)
				&& ledModeExistsAndGetBooleanKey(ledPropertiesJSON, RGB_KEY, BRIGHTNESS_KEY)) {
			defaultStatusNames.add(RGB_BRIGHTNESS_KEY);
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, TUNABLE_KEY, ENABLED_KEY)
				&& ledModeExistsAndGetBooleanKey(ledPropertiesJSON, TUNABLE_KEY, BRIGHTNESS_KEY)) {
			defaultStatusNames.add(TUNABLE_BRIGHTNESS_KEY);
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, WW_KEY, ENABLED_KEY)
				&& ledModeExistsAndGetBooleanKey(ledPropertiesJSON, WW_KEY, BRIGHTNESS_KEY)) {
			defaultStatusNames.add(WW_BRIGHTNESS_KEY);
		}
		if (ledModeExistsAndGetBooleanKey(ledPropertiesJSON, CW_KEY, ENABLED_KEY)
				&& ledModeExistsAndGetBooleanKey(ledPropertiesJSON, CW_KEY, BRIGHTNESS_KEY)) {
			defaultStatusNames.add(CW_BRIGHTNESS_KEY);
		}

		JSONArray statusProperties = new JSONArray();

		if (control.getStatusProperties() != null) {

			JSONArray statusPropertiesJSONArray = new JSONArray(control.getStatusProperties());

			for (int i = 0; i < statusPropertiesJSONArray.length(); i++) {
				JSONObject json = statusPropertiesJSONArray.getJSONObject(i);

				if (!json.has(STATUS_PROPERTY_STATUS_KEY))
					continue;

				if (!defaultStatusNames.contains(json.getString(STATUS_PROPERTY_STATUS_KEY))) {

					String title = json.getString(STATUS_PROPERTY_TITLE_KEY);
					String status = json.getString(STATUS_PROPERTY_STATUS_KEY);
					boolean display = json.getBoolean(STATUS_PROPERTY_DISPLAY_KEY);
					String formatter = json.getString(STATUS_PROPERTY_FORMATTER_KEY);

					ControlStatusProperty statusProperty = new ControlStatusProperty(title, status, display, true,
							formatter);

					statusProperties.put(new JSONObject(statusProperty));
				}
			}
		}

		defaultStatusNames.forEach(status -> {

			String title = status.substring(0, 1).toUpperCase() + status.substring(1);
			ControlStatusProperty statusProperty = new ControlStatusProperty(title, status, false, false, "");
			statusProperties.put(new JSONObject(statusProperty));
		});

		if (statusProperties.length() <= 0) {
			return null;
		}

		return statusProperties.toString();
	}

	private String generateActionProperties() {

		JSONObject allowedActionsJSON = generateDefaultActionProperties();

		allowedActionsJSON.put(ALLOWED_ACTION_TASK_KEY, generateTaskDefintions());
		allowedActionsJSON.put(ALLOWED_ACTION_CONDITIONAL_KEY, generateConditionalDefinitions());
		
		return allowedActionsJSON.toString();
	}
	
	private JSONObject generateConditionalDefinitions() {
		
		JSONObject conditionalDefinitionObject = new JSONObject();
		conditionalDefinitionObject.put("GET_STATUS", new JSONObject(generateGetStatusParameterDefinition()));
		conditionalDefinitionObject.put("GET_VALUE", new JSONObject(generateGetValueParameterDefinition()));
		
		return conditionalDefinitionObject;
	}
	
	private ParameterDefinition generateGetStatusParameterDefinition() {
		
		ArrayList<String> statusArray = getStatusList();
		
		return new ParameterDefinition(statusArray, true);
	}
	
	private ParameterDefinition generateGetValueParameterDefinition() {
		
		return new ParameterDefinition(getValueArray(), true);
	}

	private JSONObject generateTaskDefintions() {

		JSONObject taskDefinitionObject = new JSONObject();

		taskDefinitionObject.put("TOGGLE_ON", new JSONObject(generateToggleOnParameterDefinition()));
		taskDefinitionObject.put("TOGGLE_OFF", new JSONObject(generateToggleOffParameterDefinition()));
		taskDefinitionObject.put("SET_VALUE", new JSONObject(generateSetValueParameterDefinition()));

		return taskDefinitionObject;
	}

	private ParameterDefinition generateToggleOnParameterDefinition() {

		ArrayList<String> ledModes = new ArrayList<String>();

		JSONObject ledJSON = getLEDProperties();

		if (ledJSON.has("rgb")) {
			JSONObject rgbJSON = ledJSON.getJSONObject("rgb");

			if (rgbJSON.getBoolean("enabled"))
				ledModes.add("rgb");
		}
		if (ledJSON.has("tunable")) {
			JSONObject tunableJSON = ledJSON.getJSONObject("tunable");
			if (tunableJSON.getBoolean("enabled"))
				ledModes.add("tunable");
		}
		if (ledJSON.has("ww")) {
			JSONObject wwJSON = ledJSON.getJSONObject("ww");
			if (wwJSON.getBoolean("enabled"))
				ledModes.add("ww");
		}
		if (ledJSON.has("cw")) {
			JSONObject cwJSON = ledJSON.getJSONObject("cw");
			if (cwJSON.getBoolean("enabled"))
				ledModes.add("cw");
		}

		ArrayList<String> parameterKeys = new ArrayList<String>(Arrays.asList("duration"));
		
		return new ParameterDefinition(parameterKeys, false);
	}

	private ParameterDefinition generateToggleOffParameterDefinition() {

		return new ParameterDefinition();
	}

	private ParameterDefinition generateSetValueParameterDefinition() {

		return new ParameterDefinition(getValueArray(), true);
	}

	private ArrayList<String> getValueArray() {

		ArrayList<String> valueArrayList = new ArrayList<String>();
		valueArrayList.add("state");
		valueArrayList.add("active");

		JSONObject ledJSON = getLEDProperties();

		if (ledJSON.has("rgb")) {
			JSONObject rgbJSON = ledJSON.getJSONObject("rgb");

			if (rgbJSON.getBoolean("enabled")) {
				valueArrayList.add("state.rgb");
				valueArrayList.add("state.rgb_r");
				valueArrayList.add("state.rgb_g");
				valueArrayList.add("state.rgb_b");
				if (rgbJSON.getBoolean("brightness"))
					valueArrayList.add("state.rgb_bright");
			}

		}
		if (ledJSON.has("tunable")) {
			JSONObject tunableJSON = ledJSON.getJSONObject("tunable");
			if (tunableJSON.getBoolean("enabled")) {
				valueArrayList.add("state.tunable");
				valueArrayList.add("state.tunable_kelvin");
				if (tunableJSON.getBoolean("brightness"))
					valueArrayList.add("state.tunable_bright");
			}
		}
		if (ledJSON.has("ww")) {
			JSONObject wwJSON = ledJSON.getJSONObject("ww");
			if (wwJSON.getBoolean("enabled")) {
				valueArrayList.add("state.ww");
				if (wwJSON.getBoolean("brightness"))
					valueArrayList.add("state.ww_bright");
			}

		}
		if (ledJSON.has("cw")) {
			JSONObject cwJSON = ledJSON.getJSONObject("cw");
			if (cwJSON.getBoolean("enabled")) {
				valueArrayList.add("state.cw");
				if (cwJSON.getBoolean("brightness"))
					valueArrayList.add("state.cw_bright");
			}
		}

		return valueArrayList;
	}

	private void generatePersistanceEvents() {

		JSONObject ledJSON = getLEDProperties();

		if (ledModeExistsAndGetBooleanKey(ledJSON, RGB_KEY, ENABLED_KEY)) {

			ArrayList<ParsedActionParameter> parsedActionParameterArray = new ArrayList<ParsedActionParameter>();

			parsedActionParameterArray.add(new ParsedActionParameter("state_status_rgb", "state.rgb", "0"));
			parsedActionParameterArray.add(new ParsedActionParameter("state_timer_rgb", "state.rgb_timer", "0"));

			buildPersistanceEvent("TIMER_END_RGB", parsedActionParameterArray);
		}
		if (ledModeExistsAndGetBooleanKey(ledJSON, TUNABLE_KEY, ENABLED_KEY)) {

			ArrayList<ParsedActionParameter> parsedActionParameterArray = new ArrayList<ParsedActionParameter>();

			parsedActionParameterArray.add(new ParsedActionParameter("state_status_tunable", "state.tunable", "0"));
			parsedActionParameterArray
					.add(new ParsedActionParameter("state_timer_tunable", "state.tunable_timer", "0"));

			buildPersistanceEvent("TIMER_END_TUNABLE", parsedActionParameterArray);
		}
		if (ledModeExistsAndGetBooleanKey(ledJSON, WW_KEY, ENABLED_KEY)) {

			ArrayList<ParsedActionParameter> parsedActionParameterArray = new ArrayList<ParsedActionParameter>();

			parsedActionParameterArray.add(new ParsedActionParameter("state_status_ww", "state.ww", "0"));
			parsedActionParameterArray.add(new ParsedActionParameter("state_timer_ww", "state.ww_timer", "0"));

			buildPersistanceEvent("TIMER_END_WW", parsedActionParameterArray);
		}
		if (ledModeExistsAndGetBooleanKey(ledJSON, CW_KEY, ENABLED_KEY)) {

			ArrayList<ParsedActionParameter> parsedActionParameterArray = new ArrayList<ParsedActionParameter>();

			parsedActionParameterArray.add(new ParsedActionParameter("state_status_cw", "state.cw", "0"));
			parsedActionParameterArray.add(new ParsedActionParameter("state_timer_cw", "state.cw_timer", "0"));

			buildPersistanceEvent("TIMER_END_CW", parsedActionParameterArray);
		}
	}

	private void buildPersistanceEvent(String eventName, ArrayList<ParsedActionParameter> parsedActionParameterArray) {

		ArrayList<ParsedActionTask> parsedActionThenArray = new ArrayList<ParsedActionTask>();

			
		ParsedActionTask actionThen = new ParsedActionTask();
		actionThen.setObjectId(control.getId());
		actionThen.setObjectType(ActionObjectType.CONTROL);
		
		actionThen.setAction("SET_VALUE");
		actionThen.setParameters(parsedActionParameterArray);
		
		parsedActionThenArray.add(actionThen);

		ParsedAction parsedAction = new ParsedAction();
		parsedAction.setThens(parsedActionThenArray);

		Action savedAction = addOrUpdateAction(eventName, parsedAction);

		addOrUpdateEvent(eventName, savedAction);

	}

	private void mapBooleanIfNotExists(JSONObject optionsJSON, String key, boolean value) {
		if (!optionsJSON.has(key)) {
			optionsJSON.put(key, value);
		}
	}

	private void mapIntegerIfNotExists(JSONObject optionsJSON, String key, int value) {
		if (!optionsJSON.has(key)) {
			optionsJSON.put(key, value);
		}
	}

	private JSONObject getLEDProperties() {

		if (control.getProperties() != null) {

			JSONObject propertiesJSON = new JSONObject(control.getProperties());

			if (propertiesJSON.has(LED_KEY)) {

				return propertiesJSON.getJSONObject(LED_KEY);
			}
		}

		return new JSONObject();
	}

	private boolean ledModeExistsAndGetBooleanKey(JSONObject ledPropertiesJSON, String mode, String key) {

		if (ledPropertiesJSON.has(mode)) {
			if (ledPropertiesJSON.getJSONObject(mode).has(key)) {

				try {
					return ledPropertiesJSON.getJSONObject(mode).getBoolean(key);

				} catch (JSONException e) {
					return false;
				}
			}
		}
		return false;
	}

}
