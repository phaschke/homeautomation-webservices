package com.peterchaschke.homeautomation.controls.H801;

import org.json.JSONException;
import org.json.JSONObject;

public class H801State {

	private final String RGB_STATE_KEY = "rgb";
	private final String RGB_TIMER_KEY = "rgb_timer";
	private final String RGB_R_VAL_KEY = "rgb_r";
	private final String RGB_G_VAL_KEY = "rgb_g";
	private final String RGB_B_VAL_KEY = "rgb_b";
	private final String RGB_BRIGHT_VAL_KEY = "rgb_bright";
	private final String TUNABLE_STATE_KEY = "tunable";
	private final String TUNABLE_TIMER_KEY = "tunable_timer";
	private final String TUNABLE_KELVIN_VAL_KEY = "tunable_kelvin";
	private final String TUNABLE_BRIGHT_VAL_KEY = "tunable_bright";
	private final String WW_STATE_KEY = "ww";
	private final String WW_TIMER_KEY = "ww_timer";
	private final String WW_BRIGHT_VAL_KEY = "ww_bright";
	private final String CW_STATE_KEY = "cw";
	private final String CW_TIMER_KEY = "cw_timer";
	private final String CW_BRIGHT_VAL_KEY = "cw_bright";

	private final String RGB_R_VAL_DEVICE_KEY = "r";
	private final String RGB_G_VAL_DEVICE_KEY = "g";
	private final String RGB_B_VAL_DEVICE_KEY = "b";
	private final String TUNABLE_VAL_DEVICE_KEY = "tun";
	private final String BRIGHT_VAL_KEY = "br";
	private final String TIMER_VAL_KEY = "timer";

	private int rgb;
	private int rgb_timer;
	private int rgb_r;
	private int rgb_g;
	private int rgb_b;
	private int rgb_bright;
	private int tunable;
	private int tunable_timer;
	private int tunable_kelvin;
	private int tunable_bright;
	private int ww;
	private int ww_timer;
	private int ww_bright;
	private int cw;
	private int cw_timer;
	private int cw_bright;

	public int getRgb() {
		return rgb;
	}

	public void setRgb(int rgb) {
		this.rgb = rgb;
	}

	public int getRgb_timer() {
		return rgb_timer;
	}

	public void setRgb_timer(int rgb_timer) {
		this.rgb_timer = rgb_timer;
	}

	public int getRgb_r() {
		return rgb_r;
	}

	public void setRgb_r(int rgb_r) {
		this.rgb_r = rgb_r;
	}

	public int getRgb_g() {
		return rgb_g;
	}

	public void setRgb_g(int rgb_g) {
		this.rgb_g = rgb_g;
	}

	public int getRgb_b() {
		return rgb_b;
	}

	public void setRgb_b(int rgb_b) {
		this.rgb_b = rgb_b;
	}

	public int getRgb_bright() {
		return rgb_bright;
	}

	public void setRgb_bright(int rgb_bright) {
		this.rgb_bright = rgb_bright;
	}

	public int getTunable() {
		return tunable;
	}

	public void setTunable(int tunable) {
		this.tunable = tunable;
	}

	public int getTunable_timer() {
		return tunable_timer;
	}

	public void setTunable_timer(int tunable_timer) {
		this.tunable_timer = tunable_timer;
	}

	public int getTunable_kelvin() {
		return tunable_kelvin;
	}

	public void setTunable_kelvin(int tunable_kelvin) {
		this.tunable_kelvin = tunable_kelvin;
	}

	public int getTunable_bright() {
		return tunable_bright;
	}

	public void setTunable_bright(int tunable_bright) {
		this.tunable_bright = tunable_bright;
	}

	public int getWw() {
		return ww;
	}

	public void setWw(int ww) {
		this.ww = ww;
	}

	public int getWw_timer() {
		return ww_timer;
	}

	public void setWw_timer(int ww_timer) {
		this.ww_timer = ww_timer;
	}

	public int getWw_bright() {
		return ww_bright;
	}

	public void setWw_bright(int ww_bright) {
		this.ww_bright = ww_bright;
	}

	public int getCw() {
		return cw;
	}

	public void setCw(int cw) {
		this.cw = cw;
	}

	public int getCw_timer() {
		return cw_timer;
	}

	public void setCw_timer(int cw_timer) {
		this.cw_timer = cw_timer;
	}

	public int getCw_bright() {
		return cw_bright;
	}

	public void setCw_bright(int cw_bright) {
		this.cw_bright = cw_bright;
	}

	public void parseJSON(JSONObject existingJSON, JSONObject inputJSON) {

		if (inputJSON.has(RGB_STATE_KEY)) {
			setRgb(inputJSON.getInt(RGB_STATE_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_STATE_KEY)) {
			setRgb(existingJSON.getInt(RGB_STATE_KEY));

		} else {
			setRgb(0);
		}

		if (inputJSON.has(RGB_TIMER_KEY)) {
			setRgb_timer(inputJSON.getInt(RGB_TIMER_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_TIMER_KEY)) {
			setRgb_timer(existingJSON.getInt(RGB_TIMER_KEY));

		} else {
			setRgb_timer(0);
		}

		if (inputJSON.has(RGB_R_VAL_KEY)) {
			setRgb_r(inputJSON.getInt(RGB_R_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_R_VAL_KEY)) {
			setRgb_r(existingJSON.getInt(RGB_R_VAL_KEY));

		} else {
			setRgb_r(255);
		}

		if (inputJSON.has(RGB_G_VAL_KEY)) {
			setRgb_g(inputJSON.getInt(RGB_G_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_G_VAL_KEY)) {
			setRgb_g(existingJSON.getInt(RGB_G_VAL_KEY));

		} else {
			setRgb_g(0);
		}

		if (inputJSON.has(RGB_B_VAL_KEY)) {
			setRgb_b(inputJSON.getInt(RGB_B_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_B_VAL_KEY)) {
			setRgb_b(existingJSON.getInt(RGB_B_VAL_KEY));

		} else {
			setRgb_b(0);
		}

		if (inputJSON.has(RGB_BRIGHT_VAL_KEY)) {
			setRgb_bright(inputJSON.getInt(RGB_BRIGHT_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, RGB_BRIGHT_VAL_KEY)) {
			setRgb_bright(existingJSON.getInt(RGB_BRIGHT_VAL_KEY));

		} else {
			setRgb_bright(100);
		}

		if (inputJSON.has(TUNABLE_STATE_KEY)) {
			setTunable(inputJSON.getInt(TUNABLE_STATE_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, TUNABLE_STATE_KEY)) {
			setTunable(existingJSON.getInt(TUNABLE_STATE_KEY));

		} else {
			setTunable(0);
		}

		if (inputJSON.has(TUNABLE_TIMER_KEY)) {
			setTunable_timer(inputJSON.getInt(TUNABLE_TIMER_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, TUNABLE_TIMER_KEY)) {
			setTunable_timer(existingJSON.getInt(TUNABLE_TIMER_KEY));

		} else {
			setTunable_timer(0);
		}

		if (inputJSON.has(TUNABLE_KELVIN_VAL_KEY)) {
			setTunable_kelvin(inputJSON.getInt(TUNABLE_KELVIN_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, TUNABLE_KELVIN_VAL_KEY)) {
			setTunable_kelvin(existingJSON.getInt(TUNABLE_KELVIN_VAL_KEY));

		} else {
			setTunable_kelvin(0);
		}

		if (inputJSON.has(TUNABLE_BRIGHT_VAL_KEY)) {
			setTunable_bright(inputJSON.getInt(TUNABLE_BRIGHT_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, TUNABLE_BRIGHT_VAL_KEY)) {
			setTunable_bright(existingJSON.getInt(TUNABLE_BRIGHT_VAL_KEY));

		} else {
			setTunable_bright(100);
		}

		if (inputJSON.has(WW_STATE_KEY)) {
			setWw(inputJSON.getInt(WW_STATE_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, WW_STATE_KEY)) {
			setWw(existingJSON.getInt(WW_STATE_KEY));

		} else {
			setWw(0);
		}

		if (inputJSON.has(WW_TIMER_KEY)) {
			setWw_timer(inputJSON.getInt(WW_TIMER_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, WW_TIMER_KEY)) {
			setWw_timer(existingJSON.getInt(WW_TIMER_KEY));

		} else {
			setWw_timer(0);
		}

		if (inputJSON.has(WW_BRIGHT_VAL_KEY)) {
			setWw_bright(inputJSON.getInt(WW_BRIGHT_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, WW_BRIGHT_VAL_KEY)) {
			setWw_bright(existingJSON.getInt(WW_BRIGHT_VAL_KEY));

		} else {
			setWw_bright(100);
		}

		if (inputJSON.has(CW_STATE_KEY)) {
			setCw(inputJSON.getInt(CW_STATE_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, CW_STATE_KEY)) {
			setCw(existingJSON.getInt(CW_STATE_KEY));

		} else {
			setCw(0);
		}

		if (inputJSON.has(CW_TIMER_KEY)) {
			setCw_timer(inputJSON.getInt(CW_TIMER_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, CW_TIMER_KEY)) {
			setCw_timer(existingJSON.getInt(CW_TIMER_KEY));

		} else {
			setCw_timer(0);
		}

		if (inputJSON.has(CW_BRIGHT_VAL_KEY)) {
			setCw_bright(inputJSON.getInt(CW_BRIGHT_VAL_KEY));

		} else if (ifHasAndIsNotNull(existingJSON, CW_BRIGHT_VAL_KEY)) {
			setCw_bright(existingJSON.getInt(CW_BRIGHT_VAL_KEY));

		} else {
			setCw_bright(100);
		}

	}

	private boolean ifHasAndIsNotNull(JSONObject existingJSON, String key) {

		if (existingJSON.has(key)) {
			if (!existingJSON.isNull(key)) {
				return true;
			}
		}
		return false;
	}

	public JSONObject getLEDRequest(String mode, JSONObject controlOptions) throws Exception {

		JSONObject mqttRequestJSON = new JSONObject();

		switch (mode) {

		case "rgb":

			JSONObject rgbObject = new JSONObject();
			rgbObject.put(RGB_R_VAL_DEVICE_KEY, getRgb_r());
			rgbObject.put(RGB_G_VAL_DEVICE_KEY, getRgb_g());
			rgbObject.put(RGB_B_VAL_DEVICE_KEY, getRgb_b());
			rgbObject.put(BRIGHT_VAL_KEY, getRgb_bright());
			if (getRgb_timer() > 0) {
				rgbObject.put(TIMER_VAL_KEY, getRgb_timer());
			}

			mqttRequestJSON.put(RGB_STATE_KEY, rgbObject);

			break;

		case "tun":

			int maxTemp, minTemp;

			JSONObject controlOptionObject = getTunableControlOptions(controlOptions);
			try {
				minTemp = controlOptionObject.getInt("minTemp");
				maxTemp = controlOptionObject.getInt("maxTemp");

			} catch (JSONException je) {

				throw new Exception("Failed to toggle led: " + je.getMessage());
			}

			double adjustedMaxTemp = maxTemp - minTemp;
			double adjustedSetTemp = getTunable_kelvin() - minTemp;

			int cwTemp = 0;
			if (adjustedSetTemp > 0) {
				double scale = adjustedMaxTemp / adjustedSetTemp;
				cwTemp = (int) (100 / scale);
			}
			int wwTemp = 100 - cwTemp;

			JSONObject tunableObject = new JSONObject();
			tunableObject.put(WW_STATE_KEY, wwTemp);
			tunableObject.put(CW_STATE_KEY, cwTemp);
			tunableObject.put(BRIGHT_VAL_KEY, getTunable_bright());
			if (getTunable_timer() > 0) {
				tunableObject.put(TIMER_VAL_KEY, getTunable_timer());
			}

			mqttRequestJSON.put(TUNABLE_VAL_DEVICE_KEY, tunableObject);

			break;

		case "ww":

			JSONObject wwObject = new JSONObject();
			wwObject.put(BRIGHT_VAL_KEY, getWw_bright());
			if (getWw_timer() > 0) {
				wwObject.put(TIMER_VAL_KEY, getWw_timer());
			}

			mqttRequestJSON.put(WW_STATE_KEY, wwObject);

			break;

		case "cw":

			JSONObject cwObject = new JSONObject();
			cwObject.put(BRIGHT_VAL_KEY, getCw_bright());
			if (getCw_timer() > 0) {
				cwObject.put(TIMER_VAL_KEY, getCw_timer());
			}

			mqttRequestJSON.put(CW_STATE_KEY, cwObject);

			break;

		default:
			throw new Exception("Failed to toggle led: invalid led mode");
		}

		return mqttRequestJSON;
	}

	private JSONObject getTunableControlOptions(JSONObject controlOptions) {

		try {
			JSONObject tunableObject = controlOptions.getJSONObject("led").getJSONObject("tunable");

			return tunableObject;

		} catch (JSONException je) {

			return new JSONObject();
		}

	}

	public void setLEDModeStateOn(String mode, int timerDuration) {
		switch (mode) {

		case "rgb":
			setRgb(1);
			setRgb_timer(timerDuration);
			break;

		case "tun":
			setTunable(1);
			setTunable_timer(timerDuration);
			break;

		case "ww":
			setWw(1);
			setWw_timer(timerDuration);
			break;

		case "cw":
			setCw(1);
			setCw_timer(timerDuration);
			break;
		}
	}

	public void setLEDModeStateOff(String mode) {
		switch (mode) {

		case "rgb":
			setRgb(0);
			break;

		case "tun":
			setTunable(0);
			break;

		case "ww":
			setWw(0);
			break;

		case "cw":
			setCw(0);
			break;

		case "all":
			setRgb(0);
			setTunable(0);
			setWw(0);
			setCw(0);
			break;
		}
	}

}
