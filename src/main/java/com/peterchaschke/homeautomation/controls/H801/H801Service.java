package com.peterchaschke.homeautomation.controls.H801;

import java.util.ArrayList;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.ActionConditionalExecutionResult;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionConditional;
import com.peterchaschke.homeautomation.action.ParsedActionParameter;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlAction;
import com.peterchaschke.homeautomation.controls.ControlActionParameter;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.InvalidControlActionException;

public class H801Service extends BaseControlService {

	private final String TOGGLE_ON_KEY = "toggle_on";
	private final String TOGGLE_OFF_KEY = "toggle_off";
	private final String STATE_KEY = "state";
	private final String TIMERS_KEY = "timers";

	private final String RGB_TIMER_KEY = "rgb_timer";
	private final String TUNABLE_TIMER_KEY = "tunable_timer";
	private final String WW_TIMER_KEY = "ww_timer";
	private final String CW_TIMER_KEY = "cw_timer";

	public H801Service(Control control, ControlRepository controlRepository) {
		super(control, controlRepository);
	}

	@Override
	public ControlResource doAction(ControlAction action) throws Exception {

		Optional<String> ledMode = Optional.empty();

		switch (action.getAction()) {
		case "TOGGLE_ON":

			Optional<Integer> duration = Optional.empty();

			for (int i = 0; i < action.getParameters().size(); i++) {
				ControlActionParameter parameter = action.getParameters().get(i);
				if (parameter.getParameterName().equals("duration")) {
					if (parameter.getParameter() != null) {
						int durationParam = (int) parameter.getParameter();
						duration = Optional.of(durationParam);
					}
				}
				if (parameter.getParameterName().equals("mode")) {
					if (parameter.getParameter() != null) {
						String ledModeParam = (String) parameter.getParameter();
						ledMode = Optional.of(ledModeParam);
					}
				}
			}

			turnOn(duration, ledMode);

			break;

		case "TOGGLE_OFF":

			// TODO: Remove ledMode param once device code turns ALL channels off on toggle
			// off
			ledMode = Optional.empty();

			for (int i = 0; i < action.getParameters().size(); i++) {
				ControlActionParameter parameter = action.getParameters().get(i);
				if (parameter.getParameterName().equals("mode")) {
					if (parameter.getParameter() != null) {
						String ledModeParam = (String) parameter.getParameter();
						ledMode = Optional.of(ledModeParam);
					}
				}
			}

			turnOff(ledMode);

			break;

		default:

			throw new InvalidControlActionException("Control Action", action.getAction(),
					"Invalid control action for Switch");

		}

		return control.toResource();
	}

	@Override
	public ControlResource getDisplayStatus() {

		ArrayList<String> displayStatusList = new ArrayList<>();
		displayStatusList.add("state");
		displayStatusList.add("timers");

		ControlResource controlResource = this.control.toResource();

		String statusResult = getStatusList(displayStatusList);

		controlResource.setStatus(statusResult);

		return controlResource;
	}

	@Override
	public ControlResource updateState(JSONObject parsedStateJSON) {

		JSONObject existingState = new JSONObject(control.getState());

		H801State ledStatusObj = new H801State();

		ledStatusObj.parseJSON(existingState, parsedStateJSON);
		JSONObject combinedState = new JSONObject(ledStatusObj);

		control.setState(combinedState.toString());

		Control savedControl = controlRepository.save(control);

		return savedControl.toResource();
	}

	@Override
	public String getStatusList(ArrayList<String> requestedStatuses) {

		try {

			JSONObject responseJSON = makeRequest(buildStatusRequestJSON(requestedStatuses));

			JSONObject existingStateJson = new JSONObject(control.getState());
			JSONObject blankStateJson = new JSONObject();
			H801State existingStateObj = new H801State();
			existingStateObj.parseJSON(existingStateJson, blankStateJson);

			H801State combinedStateObject = parseLEDControllerStatusResponse(existingStateObj, responseJSON);
			JSONObject combinedStateJson = new JSONObject(combinedStateObject);

			// Save requested state to database
			control.setState(combinedStateJson.toString());
			controlRepository.save(control);

			return combinedStateJson.toString();

		} catch (Exception e) {

			JSONObject parsedResponseJSON = new JSONObject();
			parsedResponseJSON.put("error", e.getMessage());

			return parsedResponseJSON.toString();
		}
	}

	private H801State parseLEDControllerStatusResponse(H801State ledStatusObj, JSONObject responseJSON)
			throws Exception {

		try {
			JSONObject stateObject = responseJSON.getJSONObject("status").getJSONObject(STATE_KEY);

			if (getActiveStateStatus(stateObject, "rgb")) {
				ledStatusObj.setRgb(1);
			} else {
				ledStatusObj.setRgb(0);
			}
			if (getActiveStateStatus(stateObject, "tun")) {
				ledStatusObj.setTunable(1);
			} else {
				ledStatusObj.setTunable(0);
			}
			if (getActiveStateStatus(stateObject, "ww")) {
				ledStatusObj.setWw(1);
			} else {
				ledStatusObj.setWw(0);
			}
			if (getActiveStateStatus(stateObject, "cw")) {
				ledStatusObj.setCw(1);
			} else {
				ledStatusObj.setCw(0);
			}

			JSONObject timersObject = responseJSON.getJSONObject("status").getJSONObject(TIMERS_KEY);

			ledStatusObj.setRgb_timer(getActiveTimerStatus(timersObject, "rgb"));
			ledStatusObj.setTunable_timer(getActiveTimerStatus(timersObject, "tun"));
			ledStatusObj.setWw_timer(getActiveTimerStatus(timersObject, "ww"));
			ledStatusObj.setCw_timer(getActiveTimerStatus(timersObject, "cw"));

		} catch (JSONException je) {
			throw new Exception("Error getting status: error parsing response message: " + je.getMessage());
		}

		return ledStatusObj;
	}

	private boolean getActiveStateStatus(JSONObject stateObject, String mode) throws Exception {

		if (stateObject.has(mode)) {
			return stateObject.getBoolean(mode);
		} else {
			throw new Exception("Unable to get state status: unknown led mode");
		}
	}

	private int getActiveTimerStatus(JSONObject timerObject, String mode) throws Exception {

		if (timerObject.has(mode)) {
			if (timerObject.getJSONObject(mode).getBoolean("active")) {
				return timerObject.getJSONObject(mode).getInt("timer");
			} else {
				return 0;
			}
		} else {
			throw new Exception("Unable to get timer status: unknown led mode");
		}
	}

	public void turnOn(Optional<Integer> duration, Optional<String> params) throws Exception {

		if (!paramsPresent(params)) {
			throw new Exception("Missing parameter: led mode");
		}
		String ledMode = params.get();
		ledMode = convertLEDModeStringForMQTT(ledMode);

		JSONObject existingState = new JSONObject(control.getState());
		JSONObject existingStateRemovedTimers = removeLEDModeTimer(existingState, ledMode);

		H801State ledStatusObj = getLEDControllerStatus(existingStateRemovedTimers);
		JSONObject mqttRequestJSON = generateToggleOnMQTTRequest(ledStatusObj, ledMode, duration);

		try {

			JSONObject response = makeRequest(mqttRequestJSON);

			if (!(response.getJSONObject(TOGGLE_ON_KEY).getInt(ledMode) == 1)) {
				throw new Exception("Failed to toggle control on. Device response code not 1");
			}

			control.setState(generateToggleOnState(ledStatusObj, ledMode, duration).toString());
			control = controlRepository.save(control);

		} catch (JSONException je) {

			throw new Exception("Failed to toggle control on, error parsing response message: " + je.getMessage());

		} catch (Exception e) {

			throw new Exception("Failed to toggle control on: " + e);
		}

	}

	private JSONObject removeLEDModeTimer(JSONObject ledState, String ledMode) {

		switch (ledMode) {

		case "rgb":
			ledState.remove(RGB_TIMER_KEY);
			break;

		case "tun":
			ledState.remove(TUNABLE_TIMER_KEY);
			break;

		case "ww":
			ledState.remove(WW_TIMER_KEY);
			break;

		case "cw":
			ledState.remove(CW_TIMER_KEY);
			break;

		case "all":
			ledState.remove(RGB_TIMER_KEY);
			ledState.remove(TUNABLE_TIMER_KEY);
			ledState.remove(WW_TIMER_KEY);
			ledState.remove(CW_TIMER_KEY);
			break;
		}

		return ledState;
	}

	private JSONObject generateToggleOnMQTTRequest(H801State ledStatusObj, String ledMode, Optional<Integer> duration)
			throws Exception {

		ledMode = convertLEDModeStringForMQTT(ledMode);

		JSONObject controlOptions = new JSONObject();
		if (control.getProperties() != null) {
			controlOptions = new JSONObject(control.getProperties());
		}

		if (durationPresent(duration)) {
			int requestedTimer = duration.get();

			switch (ledMode) {

			case "rgb":
				ledStatusObj.setRgb_timer(requestedTimer);
				break;

			case "tun":
				ledStatusObj.setTunable_timer(requestedTimer);
				break;

			case "ww":
				ledStatusObj.setWw_timer(requestedTimer);
				break;

			case "cw":
				ledStatusObj.setCw_timer(requestedTimer);
				break;
			}
		}

		JSONObject requestLEDModeRequest = ledStatusObj.getLEDRequest(ledMode, controlOptions);
		JSONObject mqttRequestJSON = new JSONObject();
		mqttRequestJSON.put(TOGGLE_ON_KEY, requestLEDModeRequest);

		return mqttRequestJSON;
	}

	public void turnOff(Optional<String> params) throws Exception {

		String ledMode = "";
		JSONObject existingState = new JSONObject(control.getState());
		JSONObject existingStateRemovedTimers = null;

		if (paramsPresent(params)) {
			// Single led state present
			ledMode = params.get();
			ledMode = convertLEDModeStringForMQTT(ledMode);
		} else {
			// Turn off all states
			ledMode = "all";
		}

		existingStateRemovedTimers = removeLEDModeTimer(existingState, ledMode);

		H801State ledStatusObj = getLEDControllerStatus(existingStateRemovedTimers);
		JSONObject mqttRequestJSON = generateToggleOffMQTTRequest(ledMode);

		try {

			JSONObject response = makeRequest(mqttRequestJSON);

			if (!(response.getJSONObject(TOGGLE_OFF_KEY).getInt(ledMode) == 1)) {
				throw new Exception("Failed to toggle control off. Device response code not 1");
			}

			control.setState(generateToggleOffState(ledStatusObj, ledMode).toString());

			control = controlRepository.save(control);

		} catch (JSONException je) {

			throw new Exception("Failed to toggle control off, error parsing response message: " + je.getMessage());

		} catch (Exception e) {

			throw new Exception("Failed to toggle control off.");
		}
	}

	private JSONObject generateToggleOffMQTTRequest(String ledMode) throws Exception {

		JSONArray ledModeArray = new JSONArray();
		ledModeArray.put(ledMode);
		JSONObject mqttRequestJSON = new JSONObject();
		mqttRequestJSON.put(TOGGLE_OFF_KEY, ledModeArray);

		return mqttRequestJSON;
	}

	private H801State getLEDControllerStatus(JSONObject existingStateRemovedTimers) {

		// JSONObject existingState = new JSONObject(control.getState());
		JSONObject blank = new JSONObject();

		H801State ledStatusObj = new H801State();
		ledStatusObj.parseJSON(existingStateRemovedTimers, blank);

		return ledStatusObj;
	}

	private JSONObject generateToggleOnState(H801State ledStatusObj, String ledMode, Optional<Integer> duration) {

		if (durationPresent(duration)) {

			ledStatusObj.setLEDModeStateOn(ledMode, duration.get());

		} else {

			ledStatusObj.setLEDModeStateOn(ledMode, 0);
		}

		JSONObject stateJSON = new JSONObject(ledStatusObj);

		return stateJSON;
	}

	private JSONObject generateToggleOffState(H801State ledStatusObj, String ledMode) {

		ledStatusObj.setLEDModeStateOff(ledMode);
		JSONObject stateJSON = new JSONObject(ledStatusObj);

		return stateJSON;
	}

	private boolean durationPresent(Optional<Integer> duration) {
		if (duration.isPresent()) {
			return true;
		}
		return false;
	}

	private boolean paramsPresent(Optional<String> params) {
		if (params.isPresent()) {
			return true;
		}
		return false;
	}

	private String convertLEDModeStringForMQTT(String mode) {

		if (mode.equals("tunable"))
			return "tun";
		return mode;
	}

	@Override
	public ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf, boolean debug) {

		switch (parsedActionIf.getConditional()) {

		case "GET_VALUE":

			return getValue(parsedActionIf);

		case "GET_STATUS":

			return getStatus(parsedActionIf);

		default:

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
					parsedActionIf.getParameter(), "Unknown control IF action");
		}
	}

	private ActionConditionalExecutionResult getValue(ParsedActionConditional parsedActionIf) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		try {
			String field = parsedActionIf.getParameter();
			String comparator = parsedActionIf.getComparator();
			String comparedValue = parsedActionIf.getValue();

			if (field.equals("active")) {

				String output = String.valueOf(control.getActive());

				if (compareActionStringValues(comparator, comparedValue, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
							parsedActionIf.getParameter(), comparedValue, comparator, output);

				} else {
					// Comparison was false
					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);
				}
			}

			if (field.startsWith("state")) {

				String output;

				// Parameter key in form of state.jsonProperty
				if (field.split("\\.").length > 0) {
					String jsonProperty = field.split("\\.")[1];

					JSONObject parsedState = new JSONObject(control.getState());

					try {
						output = String.valueOf(parsedState.getInt(jsonProperty));

						if (compareActionStringValues(comparator, comparedValue, output)) {
							return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
									parsedActionIf.getParameter(), comparedValue, comparator, output);
						}

					} catch (JSONException e) {
						throw new Exception("Unknown or unimplemented state parameter");
					}

				} else {

					// compare the whole state
					output = control.getState();

					if (compareActionStringValues(comparator, comparedValue, output)) {
						return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
								parsedActionIf.getParameter(), comparedValue, comparator, output);
					}
				}
				// Comparison was false
				return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
						parsedActionIf.getParameter(), comparedValue, comparator, output);
			}

		} catch (Exception e) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
					parsedActionIf.getParameter(), e.getMessage());

		}

		return result;
	}

	private ActionConditionalExecutionResult getStatus(ParsedActionConditional parsedActionIf) {

		String field = parsedActionIf.getParameter();
		String comparator = parsedActionIf.getComparator();
		String comparedValue = parsedActionIf.getValue();

		ArrayList<String> requestedStatus = new ArrayList<String>();
		requestedStatus.add(field);

		String statusResult = getStatusList(requestedStatus);

		JSONObject responseJSON = new JSONObject(statusResult);

		if (responseJSON.has("error")) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
					responseJSON.get("error").toString());

		} else {

			try {
				String output = responseJSON.get(field).toString();

				if (compareActionStringValues(comparator, comparedValue, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);

				} else {
					// Comparison was false
					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);
				}

			} catch (Exception e) {

				return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
						e.getMessage());

			}

		}

	}

	private ActionConditionalExecutionResult generateTrueIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(true);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "TRUE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateFalseIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "FALSE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateErrorIfReturnMessage(String controlTitle, String action,
			String parameter, String error) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(true);
		result.setResultMessage(
				String.format("IF>> [%s] %s(%s): ERROR: %s", new Object[] { controlTitle, action, parameter, error }));

		return result;
	}

	@Override
	public ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskMode, boolean debug) {

		switch (parsedActionThen.getAction()) {

		case "SET_VALUE":
			return setValue(parsedActionThen, taskMode);

		case "TOGGLE_ON":
			return toggleOnAction(parsedActionThen, taskMode);

		case "TOGGLE_OFF":
			return toggleOffAction(parsedActionThen, taskMode);

		default:

			ActionTaskExecutionResult result = new ActionTaskExecutionResult();

			result.setResultMessage(String.format("%s>> [%s] %s: ERROR: %s", taskMode.toUpperCase(), control.getTitle(),
					parsedActionThen.getAction(), "Unknown THEN Action"));
			result.setErrorFlag(true);

			return result;

		}

	}

	private ActionTaskExecutionResult setValue(ParsedActionTask parsedActionThen, String taskMode) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();
		ArrayList<ParsedActionParameter> parameters = parsedActionThen.getParameters();
		
		try {
			
			String debugMessage = "";
			
			parameters.forEach(parameter -> {
			
				String value = parameter.getValue();
				String field = "";
	
				String key = parameter.getKey().toLowerCase();
				
				System.out.println(key);
	
				if (key.equals("active")) {
	
					int activeFlag = 0;
					if (value.equals("1"))
						activeFlag = 1;
	
					field = "active";
	
					control.setActive(activeFlag);
	
				}
	
				if (key.startsWith("state")) {
	
					field = "state";
	
					JSONObject parsedState = new JSONObject(control.getState());
	
					// Parameter key in form of state.jsonProperty
					if (key.split("\\.").length > 0) {
						String jsonProperty = key.split("\\.")[1];
	
						field = field + "." + jsonProperty;
	
						parsedState.put(jsonProperty, Integer.parseInt(value));
	
					} else {
	
						// Overwrite existing state
						parsedState = new JSONObject(value);
					}
					
					control.setState(parsedState.toString());
				}
				
				if(debugMessage.length() > 0) {
					debugMessage.concat("; ");
				} else {
					debugMessage.concat(String.format("%s>> [%s] %s: ", taskMode.toUpperCase(), control.getTitle(), "SET_VALUE"));
				}
				debugMessage.concat(String.format("%s = %s", field,value));
			});
			
			control = controlRepository.saveAndFlush(control);

			result.setErrorFlag(false);
			result.setResultMessage(debugMessage);

			return result;

		} catch (Exception e) {

			result.setErrorFlag(true);

			String debugMessage = String.format("%s>> [%s] %s: ERROR: %s", taskMode.toUpperCase(), control.getTitle(), "SET_VALUE",
					e.getMessage());
			result.setResultMessage(debugMessage);

			return result;

		}
	}

	private ActionTaskExecutionResult toggleOnAction(ParsedActionTask parsedActionThen, String taskMode) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();

		ArrayList<ParsedActionParameter> parameters = parsedActionThen.getParameters();

		try {

			Optional<Integer> duration = Optional.empty();
			Optional<String> params = Optional.empty();
			String format;
			Object[] formatParameters;

			parameters = parsedActionThen.getParameters();

			for (int i = 0; i < parameters.size(); i++) {

				ParsedActionParameter parameter = parameters.get(i);

				if (parameter.getParameterName().equals("duration")) {
					if (!parameter.getValue().isEmpty()) {
						duration = Optional.of(Integer.parseInt(parameter.getValue()));
					}
				}
				if (parameter.getParameterName().equals("LEDMode")) {
					params = Optional.of(parameter.getValue());
				}
			}

			turnOn(duration, params);

			if (duration.isPresent()) {
				format = "%s>> [%s] %s: %s = %s";
				formatParameters = new Object[] {taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON", "duration",
						duration.get().toString() };
			} else {
				format = "%s>> [%s] %s";
				formatParameters = new Object[] {taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON" };
			}

			String debugMessage = String.format(format, formatParameters);
			result.setResultMessage(debugMessage);
			result.setErrorFlag(false);

			return result;

		} catch (Exception e) {

			result.setResultMessage(
					String.format("%s>> [%s] %s: ERROR: %s", taskMode.toUpperCase(), control.getTitle(), "TOGGLE_ON", e.getMessage()));
			result.setErrorFlag(true);

			return result;
		}
	}

	private ActionTaskExecutionResult toggleOffAction(ParsedActionTask parsedActionThen, String taskMode) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();

		try {

			Optional<String> params = Optional.empty();

			turnOff(params);

			result.setErrorFlag(false);
			result.setResultMessage(String.format("%s>> [%s] %s", taskMode.toUpperCase(), control.getTitle(), "TOGGLE_OFF"));

		} catch (Exception e) {

			result.setErrorFlag(true);
			result.setResultMessage(
					String.format("%s>> [%s] %s: ERROR: %s", taskMode.toUpperCase(), control.getTitle(), "TOGGLE_OFF", e.getMessage()));

		}

		return result;
	}

}
