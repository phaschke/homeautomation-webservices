package com.peterchaschke.homeautomation.controls;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ControlRepository extends JpaRepository<Control, Long> {

	Control findByUniqueName(String name);
	List<Control> findAllByTopicIdOrderByDisplayOrderAsc(Long topicId, Pageable pageable);
	List<Control> findAllByTopicIdOrderByDisplayOrderAsc(Long topicId);
	int countAllByTopicId(Long topicId);
	List<Control> findAllByTopicIdAndActiveOrderByDisplayOrderAsc(Long topicId, int active, Pageable pageable);

}
