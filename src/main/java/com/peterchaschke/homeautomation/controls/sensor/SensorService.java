package com.peterchaschke.homeautomation.controls.sensor;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.ActionConditionalExecutionResult;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionConditional;
import com.peterchaschke.homeautomation.action.ParsedActionParameter;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlAction;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.InvalidControlActionException;

public class SensorService extends BaseControlService {

	public SensorService(Control control, ControlRepository controlRepository) {
		super(control, controlRepository);
	}

	@Override
	public ControlResource doAction(ControlAction action) throws Exception {

		throw new InvalidControlActionException("Control Action", action.getAction(),
				"Invalid control action for Sensor");
	}

	@Override
	public ControlResource updateState(JSONObject parsedStateJSON) {
		return control.toResource();
	}

	@Override
	public ControlResource getDisplayStatus() {

		ArrayList<String> displayStatusList = getDisplayStatusList();
		ControlResource controlResource = this.control.toResource();
		controlResource.setStatus(getStatusList(displayStatusList));

		return controlResource;
	}

	public String getStatusList(ArrayList<String> requestedStatuses) {

		try {

			return makeStatusListRequest(requestedStatuses);

		} catch (Exception e) {

			JSONObject parsedResponseJSON = new JSONObject();
			parsedResponseJSON.put("error", e.getMessage());

			return parsedResponseJSON.toString();

		}
	}

	private String makeStatusListRequest(ArrayList<String> requestedStatuses) throws Exception {

		if (inputStatusListEmpty(requestedStatuses)) {
			throw new Exception("No requested statuses.");
		}

		JSONObject responseJSON = makeRequest(buildStatusRequestJSON(requestedStatuses));

		if (isStatusResponseError(responseJSON)) {
			throw new Exception(responseJSON.getString("error").toString());
		}

		JSONObject parsedResponseJSON = parseStatusListResponse(responseJSON);

		return parsedResponseJSON.toString();

	}

	/**
	 * Note that as of now, the response of the devices must be translated to remove
	 * an array layer. IE. {"status":[{"relay":0},{"timer":0}]} becomes
	 * {"timer":0,"relay":0}. TODO: Eventually fix device level code.
	 * 
	 * TODO: Also note, plan to make the persisted status fields dynamic. As of
	 * today they are hard coded for relay and timer.
	 */
	private JSONObject parseStatusListResponse(JSONObject responseJSON) {

		JSONObject statusObject = responseJSON.getJSONObject("status");
		return statusObject;
	}

	// @Override
	// public Control updateState(JSONObject parsedStateJSON) {
	// TODO Auto-generated method stub
	// return control;
	// }

	@Override
	public ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf, boolean debug) {

		switch (parsedActionIf.getConditional()) {

		case "GET_VALUE":
			return getValue(parsedActionIf);

		case "GET_STATUS":
			return getStatus(parsedActionIf);

		default:

			ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();
			result.setResultMessage("Do parsed if for Status");
			result.setTrueFlag(true);
			result.setErrorFlag(false);

			return result;

		}

	}

	private ActionConditionalExecutionResult getValue(ParsedActionConditional parsedActionIf) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		try {
			String field = parsedActionIf.getParameter();
			String comparator = parsedActionIf.getComparator();
			String comparedValue = parsedActionIf.getValue();

			if (field.equals("active")) {

				String output = String.valueOf(control.getActive());

				if (compareActionStringValues(comparator, comparedValue, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
							parsedActionIf.getParameter(), comparedValue, comparator, output);

				} else {
					// Comparison was false
					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);
				}
			}

			if (field.startsWith("state")) {

				String output;

				// Parameter key in form of state.jsonProperty
				if (field.split("\\.").length > 0) {
					String jsonProperty = field.split("\\.")[1];

					JSONObject parsedState = new JSONObject(control.getState());

					try {
						output = String.valueOf(parsedState.getInt(jsonProperty));

						if (compareActionStringValues(comparator, comparedValue, output)) {
							return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
									parsedActionIf.getParameter(), comparedValue, comparator, output);
						}

					} catch (JSONException e) {
						throw new Exception("Unknown or unimplemented state parameter");
					}

				} else {

					// compare the whole state
					output = control.getState();

					if (compareActionStringValues(comparator, comparedValue, output)) {
						return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
								parsedActionIf.getParameter(), comparedValue, comparator, output);
					}
				}
				// Comparison was false
				return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
						parsedActionIf.getParameter(), comparedValue, comparator, output);
			}

		} catch (Exception e) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(),
					parsedActionIf.getParameter(), e.getMessage());

		}

		return result;
	}

	private ActionConditionalExecutionResult getStatus(ParsedActionConditional parsedActionIf) {

		String field = parsedActionIf.getParameter();
		String comparator = parsedActionIf.getComparator();
		String comparedValue = parsedActionIf.getValue();

		ArrayList<String> requestedStatus = new ArrayList<String>();
		requestedStatus.add(field);

		String statusResult = getStatusList(requestedStatus);

		JSONObject responseJSON = new JSONObject(statusResult);

		if (responseJSON.has("error")) {

			return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
					responseJSON.get("error").toString());

		} else {

			try {
				String output = responseJSON.get(field).toString();

				if (compareActionStringValues(comparator, comparedValue, output)) {

					return generateTrueIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);

				} else {
					// Comparison was false
					return generateFalseIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
							comparedValue, comparator, output);
				}

			} catch (Exception e) {

				return generateErrorIfReturnMessage(control.getTitle(), parsedActionIf.getConditional(), field,
						e.getMessage());

			}

		}

	}

	private ActionConditionalExecutionResult generateTrueIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(true);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "TRUE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateFalseIfReturnMessage(String controlTitle, String action,
			String parameter, String comparedValue, String comparator, String output) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(false);
		result.setResultMessage(String.format("IF>> [%s] %s: %s(%s): %s %s %s",
				new Object[] { controlTitle, "FALSE", action, parameter, output, comparator, comparedValue }));

		return result;
	}

	private ActionConditionalExecutionResult generateErrorIfReturnMessage(String controlTitle, String action,
			String parameter, String error) {

		ActionConditionalExecutionResult result = new ActionConditionalExecutionResult();

		result.setTrueFlag(false);
		result.setErrorFlag(true);
		result.setResultMessage(
				String.format("IF>> [%s] %s(%s): ERROR: %s", new Object[] { controlTitle, action, parameter, error }));

		return result;
	}

	@Override
	public ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskType, boolean debug) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();

		switch (parsedActionThen.getAction()) {

		case "SET_VALUE":

			ArrayList<ParsedActionParameter> parameters = parsedActionThen.getParameters();

			// for (int i = 0; i < parameters.size(); i++) {
			int i = 0;
			try {

				ParsedActionParameter parameter = parameters.get(i);
				String value = parameter.getValue();
				String field = "";

				String key = parameter.getKey().toLowerCase();

				if (key.equals("active")) {

					int activeFlag = 0;
					if (value.equals("1"))
						activeFlag = 1;

					field = "active";

					control.setActive(activeFlag);
					controlRepository.save(control);

				}

				if (key.startsWith("state")) {

					field = "state";

					JSONObject parsedState = new JSONObject(control.getState());

					// Parameter key in form of state.jsonProperty
					if (key.split("\\.").length > 0) {
						String jsonProperty = key.split("\\.")[1];

						parsedState.put(jsonProperty, value);

					} else {

						// Overwrite existing state
						parsedState = new JSONObject(value);
					}

					control.setState(parsedState.toString());
					controlRepository.save(control);
				}

				result.setResultMessage(
						String.format("%s>> [%s] %s: %s = %s", taskType.toUpperCase(), control.getTitle(), "SET_VALUE", field, value));
				result.setErrorFlag(false);

			} catch (Exception e) {

				result.setResultMessage(
						String.format("%s>> [%s] %s: ERROR: %s", taskType.toUpperCase(), control.getTitle(), "SET_VALUE", e.getMessage()));
				result.setErrorFlag(true);
			}

			break;
		// }

		default:

			result.setResultMessage(String.format("%s>> [%s] %s: ERROR: %s", taskType.toUpperCase(), control.getTitle(),
					parsedActionThen.getAction(), "Unknown THEN Action"));
			result.setErrorFlag(true);

		}

		return result;

	}

}
