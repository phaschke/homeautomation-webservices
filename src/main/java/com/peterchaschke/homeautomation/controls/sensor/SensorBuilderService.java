package com.peterchaschke.homeautomation.controls.sensor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.action.ParameterDefinition;
import com.peterchaschke.homeautomation.controls.BaseControlBuilderService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlBuilderService;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlStatusProperty;
import com.peterchaschke.homeautomation.event.EventProperty;
import com.peterchaschke.homeautomation.event.EventRepository;

public class SensorBuilderService extends BaseControlBuilderService implements ControlBuilderService {

	public SensorBuilderService(ControlResource control, ControlRepository controlRepository,
			ActionRepository actionRepository, EventRepository eventRepository) {
		super(control, controlRepository, actionRepository, eventRepository);
	}

	@Override
	public Control createControl() {

		validateJSONProperties();

		generateControlProperties();

		Control createdControl = controlRepository.save(control.toEntity());

		return createdControl;
	}

	@Override
	public Control editControl(ControlResource updatedControlDTO) {

		Control updatedControl = updatedControlDTO.toEntity();

		control.setTopicId(updatedControl.getTopicId());
		control.setActive(updatedControl.getActive());
		control.setDisplayOrder(updatedControl.getDisplayOrder());
		control.setTitle(updatedControl.getTitle());
		control.setUniqueName(updatedControl.getUniqueName());
		control.setType(updatedControl.getType());
		control.setBroker(updatedControl.getBroker());
		control.setSubtopic(updatedControl.getSubtopic());
		control.setPersistState(updatedControl.getPersistState());
		control.setProperties(updatedControl.getProperties());
		control.setEventProperties(updatedControl.getEventProperties());
		control.setStatusProperties(updatedControl.getStatusProperties());
		control.setActionProperties(updatedControl.getActionProperties());
		control.setDescription(updatedControl.getDescription());

		generateControlProperties();

		return controlRepository.save(control.toEntity());
	}

	private void generateControlProperties() {

		control.setEventProperties(generateEventProperties());
		control.setStatusProperties(generateStatusProperties());
		control.setActionProperties(generateActionProperties());
	}

	private String generateEventProperties() {

		JSONArray eventProperties = new JSONArray();
		if (control.getEventProperties() != null) {

			JSONArray eventPropertiesJSONArray = new JSONArray(control.getEventProperties());

			for (int i = 0; i < eventPropertiesJSONArray.length(); i++) {
				JSONObject json = eventPropertiesJSONArray.getJSONObject(i);
				EventProperty eventProperty = new EventProperty(json.getString(EVENT_PROPERTY_NAME_KEY), true);
				eventProperties.put(new JSONObject(eventProperty));
			}
		}

		if (eventProperties.length() <= 0) {
			return null;
		}

		return eventProperties.toString();
	}

	private String generateStatusProperties() {

		List<String> defaultStatusNames = Arrays.asList("ping");

		JSONArray statusProperties = new JSONArray();

		if (control.getStatusProperties() != null) {

			JSONArray statusPropertiesJSONArray = new JSONArray(control.getStatusProperties());

			for (int i = 0; i < statusPropertiesJSONArray.length(); i++) {
				JSONObject json = statusPropertiesJSONArray.getJSONObject(i);

				if (!json.has(STATUS_PROPERTY_STATUS_KEY))
					continue;

				if (!defaultStatusNames.contains(json.getString(STATUS_PROPERTY_STATUS_KEY))) {

					String title = json.getString(STATUS_PROPERTY_TITLE_KEY);
					String status = json.getString(STATUS_PROPERTY_STATUS_KEY);
					boolean display = json.getBoolean(STATUS_PROPERTY_DISPLAY_KEY);
					String formatter = json.getString(STATUS_PROPERTY_FORMATTER_KEY);

					ControlStatusProperty statusProperty = new ControlStatusProperty(title, status, display, true,
							formatter);

					statusProperties.put(new JSONObject(statusProperty));
				}
			}
		}

		defaultStatusNames.forEach(status -> {

			String title = status.substring(0, 1).toUpperCase() + status.substring(1);
			ControlStatusProperty statusProperty = new ControlStatusProperty(title, status, false, false, "");
			statusProperties.put(new JSONObject(statusProperty));
		});

		if (statusProperties.length() <= 0) {
			return null;
		}

		return statusProperties.toString();
	}

	private String generateActionProperties() {

		JSONObject allowedActionsJSON = generateDefaultActionProperties();
		
		allowedActionsJSON.put(ALLOWED_ACTION_TASK_KEY, generateTaskDefinitions());
		allowedActionsJSON.put(ALLOWED_ACTION_CONDITIONAL_KEY, generateConditionalDefinitions());

		return allowedActionsJSON.toString();
	}
	
	private JSONObject generateConditionalDefinitions() {
		
		JSONObject conditionalDefinitionObject = new JSONObject();
		conditionalDefinitionObject.put("GET_STATUS", new JSONObject(generateGetStatusParameterDefinition()));
		
		return conditionalDefinitionObject;
	}
	
	private ParameterDefinition generateGetStatusParameterDefinition() {
		
		ArrayList<String> statusArray = getStatusList();
		return new ParameterDefinition(statusArray, true);
	}

	private JSONObject generateTaskDefinitions() {

		JSONObject taskDefinitionObject = new JSONObject();
		taskDefinitionObject.put("SET_VALUE", new JSONObject(generateSetValueParameterDefinition()));
		
		return taskDefinitionObject;
	}

	private ParameterDefinition generateSetValueParameterDefinition() {
		
		return new ParameterDefinition(getValueArray(), true);
	}

	private ArrayList<String> getValueArray() {

		ArrayList<String> valueArrayList = new ArrayList<String>();
		valueArrayList.add("state");
		valueArrayList.add("active");

		return valueArrayList;
	}

}
