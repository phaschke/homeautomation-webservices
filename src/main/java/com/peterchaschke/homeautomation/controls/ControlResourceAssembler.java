package com.peterchaschke.homeautomation.controls;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ControlResourceAssembler extends RepresentationModelAssemblerSupport<Control, ControlResource> {

	public ControlResourceAssembler() {
		super(ControlController.class, ControlResource.class);
	}

	@Override
	public ControlResource toModel(Control entity) {

		ControlResource controlResource = instantiateModel(entity);

		controlResource.setId(entity.getId());
		controlResource.setTopicId(entity.getTopicId());
		controlResource.setTopic(entity.getTopic());
		controlResource.setUniqueName(entity.getUniqueName());
		controlResource.setActive(entity.getActive());
		controlResource.setDisplayOrder(entity.getDisplayOrder());
		controlResource.setTitle(entity.getTitle());
		controlResource.setType(entity.getType());
		controlResource.setBroker(entity.getBroker());
		controlResource.setSubtopic(entity.getSubtopic());
		controlResource.setProperties(entity.getProperties());
		controlResource.setEventProperties(entity.getEventProperties());
		controlResource.setStatusProperties(entity.getStatusProperties());
		controlResource.setActionProperties(entity.getActionProperties());
		controlResource.setState(entity.getState());
		controlResource.setPersistState(entity.getPersistState());
		controlResource.setDescription(entity.getDescription());

		return addLink(controlResource);
	}

	public ControlResource addLink(ControlResource controlResource) {

		controlResource.add(linkTo(methodOn(ControlController.class).one(controlResource.getId())).withSelfRel());

		return controlResource;
	}

	@Override
	public CollectionModel<ControlResource> toCollectionModel(Iterable<? extends Control> entities) {

		CollectionModel<ControlResource> controlDTOs = super.toCollectionModel(entities);

		return controlDTOs;
	}
}
