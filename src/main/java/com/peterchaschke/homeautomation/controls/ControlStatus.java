package com.peterchaschke.homeautomation.controls;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ControlStatus {

	private String status;
	private String value;
}
