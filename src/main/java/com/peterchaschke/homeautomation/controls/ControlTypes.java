package com.peterchaschke.homeautomation.controls;

public enum ControlTypes {
	SWITCH, SENSOR, LED_CONTROLLER
}
