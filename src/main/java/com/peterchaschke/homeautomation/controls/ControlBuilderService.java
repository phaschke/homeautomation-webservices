package com.peterchaschke.homeautomation.controls;

public interface ControlBuilderService {

	public Control createControl();

	public Control editControl(ControlResource updatedControl);

	public void deleteControl();

}
