package com.peterchaschke.homeautomation.users;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.peterchaschke.homeautomation.UserNotFoundException;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Users")
@RestController
@RequestMapping("/api/users")
public class AppUserController {
	
	@Autowired
	private AppUserDetailsServiceImpl appUserDetailsService;
	
	@Autowired
	private AppUserResourceAssembler appUserResourceAssembler;
	
	@PatchMapping("/change_admin_password")
	public ResponseEntity<?> changePassword(@RequestBody AppUserChangePasswordDTO appUserChangePasswordDTO, Principal principal) {
		
		appUserChangePasswordDTO.setUsername(principal.getName());
		appUserDetailsService.changePassword(appUserChangePasswordDTO);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/guest_users")
	public ResponseEntity<?> guestUsers() {
		
		List<AppUserDTO> guestUsers = appUserDetailsService.getGuestUsers();
		
		return new ResponseEntity<>(appUserResourceAssembler.toCollectionModel(guestUsers), HttpStatus.OK);
	}
	
	@GetMapping("/guest_user/{id}")
	public ResponseEntity<?> getGuestUser(@PathVariable Long id) throws UserNotFoundException {
		
		try {
			AppUserDTO guestUser = appUserDetailsService.getGuestUser(id);
			return new ResponseEntity<>(appUserResourceAssembler.toModel(guestUser), HttpStatus.OK);
			
		} catch (UserNotFoundException e) {
			
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/guest_user")
	public ResponseEntity<?> newGuestUser(@RequestBody AppUserDTO userDTO) throws Exception {
		
		AppUserDTO createdUserDTO = appUserDetailsService.createGuestUser(userDTO);
		
		return new ResponseEntity<>(appUserResourceAssembler.toModel(createdUserDTO), HttpStatus.OK);
	}
	
	@PutMapping("/guest_user/{id}")
	public ResponseEntity<?> updateGuestUser(@PathVariable Long id, @RequestBody AppUserDTO userDTO) throws Exception {
		
		AppUserDTO updatedUserDTO = appUserDetailsService.updateGuestUser(id, userDTO);
		
		return new ResponseEntity<>(appUserResourceAssembler.toModel(updatedUserDTO), HttpStatus.OK);
	}
	
	
	@DeleteMapping("/guest_user/{id}")
	public ResponseEntity<?> deleteGuestUser(@PathVariable Long id) throws Exception {
		
		appUserDetailsService.deleteGuestUser(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
