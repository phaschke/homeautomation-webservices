package com.peterchaschke.homeautomation.users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	AppUser findByUsername(String username);
	List<AppUser> findByRoles_id(Long roleId);
	AppUser findByIdAndRoles_id(Long id, Long roleId);
}