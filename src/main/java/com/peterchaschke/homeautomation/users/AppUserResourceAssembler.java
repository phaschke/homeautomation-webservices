package com.peterchaschke.homeautomation.users;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class AppUserResourceAssembler extends RepresentationModelAssemblerSupport<AppUserDTO, AppUserDTO> {

	public AppUserResourceAssembler() {
		super(AppUserController.class, AppUserDTO.class);
	}

	@Override
	public AppUserDTO toModel(AppUserDTO dto) {
		
		dto.add(linkTo(methodOn(AppUserController.class).getGuestUser(dto.getId())).withSelfRel());
		
		return dto;
	}
	
	@Override
	public CollectionModel<AppUserDTO> toCollectionModel(Iterable<? extends AppUserDTO> entities) {

		CollectionModel<AppUserDTO> models = super.toCollectionModel(entities);
		
		return models;
	}

}
