package com.peterchaschke.homeautomation.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.homeautomation.UserDetailsException;
import com.peterchaschke.homeautomation.UserNotFoundException;
import com.peterchaschke.homeautomation.users.roles.Role;
import com.peterchaschke.homeautomation.users.roles.RoleRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppUserDetailsServiceImpl implements UserDetailsService, AppUserDetailsService {
	
	@Autowired
	private AppUserRepository appUserRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
    private PasswordEncoder encoder;

	public AppUserDetailsServiceImpl(AppUserRepository appUserRepository) {
		this.appUserRepository = appUserRepository;
		//this.authenticationManager = authenticationManager;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = appUserRepository.findByUsername(username);

		if (appUser == null) {
			throw new UsernameNotFoundException(username);
		}

		return new User(appUser.getUsername(), appUser.getPassword(), appUser.getEnabled(), true, true, true,
				getAuthorities(appUser.getRoles()));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
			role.getPrivileges().stream().map(p -> new SimpleGrantedAuthority(p.getName())).forEach(authorities::add);
		}

		return authorities;
	}
	
	public void changePassword(AppUserChangePasswordDTO appUserChangePasswordDTO) throws UserDetailsException {
		
		AppUser user = appUserRepository.findByUsername(appUserChangePasswordDTO.getUsername());
		
		
		if (!encoder.matches(appUserChangePasswordDTO.getExistingPassword(), user.getPassword())) {
			throw new UserDetailsException("Invalid existing password");
		}
		
		if(!appUserChangePasswordDTO.getNewPassword().equals(appUserChangePasswordDTO.getNewPasswordRepeat())) {
			// New and repeat password do not match
			throw new UserDetailsException("New and repeat new password do not match");
		}
		
		user.setPassword(encoder.encode(appUserChangePasswordDTO.getNewPassword()));
		appUserRepository.save(user);
		
	}
	
	public List<AppUserDTO> getGuestUsers() {
		
		Role guestRole = roleRepository.findByName("ROLE_GUEST");
		
		List<AppUser> users = appUserRepository.findByRoles_id(guestRole.getId());
		
		List<AppUserDTO> guestUsers = users.stream().map(user -> {
			return user.toDTO();
		})
		.collect(Collectors.toList());
		
		return guestUsers;
	}
	
	public AppUserDTO getGuestUser(Long id) throws UserNotFoundException {
		
		Role guestRole = roleRepository.findByName("ROLE_GUEST");
		
		AppUser user = appUserRepository.findByIdAndRoles_id(id, guestRole.getId());
		
		if(user == null) {
			throw new UserNotFoundException("Requested user not found.");
		}
		
		return user.toDTO();
	}
	
	public AppUserDTO createGuestUser(AppUserDTO newGuestUserDTO) throws Exception {

		AppUser user = appUserRepository.findByUsername(newGuestUserDTO.getUsername());
		
		if(user != null) {
			throw new Exception("Username is already used");
		}
		
		Role guestRole = roleRepository.findByName("ROLE_GUEST");
		newGuestUserDTO.setRoles(Arrays.asList(guestRole));
		newGuestUserDTO.setPassword(encoder.encode(newGuestUserDTO.getPassword()));
		
		newGuestUserDTO.setEnabled(true);
		AppUser newGuestUser = newGuestUserDTO.toEntity();
		
		AppUser savedUser = appUserRepository.save(newGuestUser);
		
		return savedUser.toDTO();
	}
	
	public AppUserDTO updateGuestUser(Long id, AppUserDTO updatedGuestUserDTO) throws Exception {
		
		AppUser user = appUserRepository.findById(id).get();
		if(user == null) {
			throw new Exception("User not found");
		}
		
		// User name has been updated
		if(!user.getUsername().equals(updatedGuestUserDTO.getUsername())) {
			
			AppUser existingUserWithRequestedName = appUserRepository.findByUsername(updatedGuestUserDTO.getUsername());
			
			if(existingUserWithRequestedName != null) {
				throw new Exception("Username is already used");
			}
		}
		
		// If password has been changed
		if(updatedGuestUserDTO.getPassword() != null) {
			user.setPassword(encoder.encode(updatedGuestUserDTO.getPassword()));
		}
		
		if(updatedGuestUserDTO.getEnabled() != null) {
			user.setEnabled(updatedGuestUserDTO.getEnabled());
		}
		
		user.setUsername(updatedGuestUserDTO.getUsername());
		user.setTopics(updatedGuestUserDTO.getTopics());
		
		AppUser savedUser = appUserRepository.save(user);
		
		return savedUser.toDTO();
	}
	
	@Transactional
	public void deleteGuestUser(Long id) throws Exception {
		
		appUserRepository.deleteById(id);
		
	}

}
