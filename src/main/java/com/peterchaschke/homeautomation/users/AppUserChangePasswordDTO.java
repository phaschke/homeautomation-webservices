package com.peterchaschke.homeautomation.users;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppUserChangePasswordDTO {
	
	private String existingPassword;
	private String newPassword;
	private String newPasswordRepeat;
	private String username;

}
