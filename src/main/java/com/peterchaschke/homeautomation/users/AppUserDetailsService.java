package com.peterchaschke.homeautomation.users;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

public interface AppUserDetailsService {
	
	@PreAuthorize("hasRole('ADMIN')")
	public void changePassword(AppUserChangePasswordDTO appUserChangePasswordDTO);
	
	@PreAuthorize("hasRole('ADMIN')")
	public List<AppUserDTO> getGuestUsers();
	
	@PreAuthorize("hasRole('ADMIN')")
	public AppUserDTO getGuestUser(Long id) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public AppUserDTO createGuestUser(AppUserDTO newGuestUserDTO) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public AppUserDTO updateGuestUser(Long id, AppUserDTO updatedGuestUserDTO) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteGuestUser(Long id) throws Exception;

}
