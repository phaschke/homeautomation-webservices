package com.peterchaschke.homeautomation.users;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.homeautomation.users.privileges.Privilege;
import com.peterchaschke.homeautomation.users.privileges.PrivilegeRepository;
import com.peterchaschke.homeautomation.users.roles.Role;
import com.peterchaschke.homeautomation.users.roles.RoleRepository;

@Component
public class AppUserSetup implements ApplicationListener<ContextRefreshedEvent> {
	
	boolean alreadySetup = false;
	
	@Value("${users.admin.default.name}")
	private String defaultAdminName;
	
	@Value("${users.admin.default.password}")
	private String defaultAdminPassword;
	
	@Autowired
    private AppUserRepository appUserRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
	private PrivilegeRepository privilegeRepository;
	
	@Autowired
    private PasswordEncoder encoder;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if(alreadySetup) return;
		
		Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
		Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
		
		List<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege); 
		
		createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_GUEST", Arrays.asList(readPrivilege));
        
        AppUser appUser = appUserRepository.findByUsername(defaultAdminName);
        
        if(appUser == null) {
        	
        	appUser = new AppUser();
        	appUser.setUsername(defaultAdminName);
            appUser.setPassword(encoder.encode(defaultAdminPassword));
            
            Role adminRole = roleRepository.findByName("ROLE_ADMIN");
            
            appUser.setRoles(Arrays.asList(adminRole));
            appUser.setEnabled(true);
            
            appUserRepository.save(appUser);
        }

        alreadySetup = true;
		
	}
	
	@Transactional
    Privilege createPrivilegeIfNotFound(String name) {
 
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }
	
	@Transactional
    Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
 
        Role role = roleRepository.findByName(name);
        if (role == null) {
        	
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }

}
