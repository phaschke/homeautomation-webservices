package com.peterchaschke.homeautomation.users;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotBlank;

import com.peterchaschke.homeautomation.topic.Topic;
import com.peterchaschke.homeautomation.users.roles.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class AppUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	private String username;

	@NotBlank
	private String password;
	
	private Boolean enabled;
	
	@ManyToMany
    @JoinTable( 
        name = "users_roles", 
        joinColumns = @JoinColumn(
          name = "user_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(
          name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;
	
	@ManyToMany
	@JoinTable( 
	    name = "users_topics", 
	    joinColumns = @JoinColumn(
	      name = "user_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "topic_id", referencedColumnName = "id")) 
	private Collection<Topic> topics;

	AppUser(String username, String broker, String password) {
		this.username = username;
		this.password = password;
	}
	
	public AppUserDTO toDTO() {
		
		AppUserDTO appUserDTO = new AppUserDTO();
		
		appUserDTO.setId(this.getId());
		appUserDTO.setUsername(this.getUsername());
		appUserDTO.setEnabled(this.getEnabled());
		appUserDTO.setRoles(this.getRoles());
		appUserDTO.setTopics(this.getTopics());
		
		return appUserDTO;
	}

}
