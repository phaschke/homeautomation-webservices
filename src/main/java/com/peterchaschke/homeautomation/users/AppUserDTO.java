package com.peterchaschke.homeautomation.users;

import java.util.Collection;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.homeautomation.topic.Topic;
import com.peterchaschke.homeautomation.users.roles.Role;

import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "user")
@Relation(collectionRelation = "users")
@Getter
@Setter
public class AppUserDTO extends RepresentationModel<AppUserDTO> {
	
	private Long id;
	
	@JsonProperty(required = true)
	private String username;
	
	private String password;
	private Boolean enabled;
	private Collection<Role> roles;
	private Collection<Topic> topics;
	
	public AppUser toEntity() {
		
		AppUser appUser = new AppUser();
		
		appUser.setId(this.getId());
		appUser.setUsername(this.getUsername());
		appUser.setPassword(this.getPassword());
		appUser.setEnabled(this.getEnabled());
		appUser.setRoles(this.getRoles());
		appUser.setTopics(this.getTopics());
		
		return appUser;
	}

}
