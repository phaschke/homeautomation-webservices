package com.peterchaschke.homeautomation.scheduler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ScheduleResourceAssembler extends RepresentationModelAssemblerSupport<ScheduleModel, ScheduleModel> {
	
	public ScheduleResourceAssembler() {
		super(ScheduleController.class, ScheduleModel.class);
	}

	@Override
	public ScheduleModel toModel(ScheduleModel model) {
		
		model.add(linkTo(methodOn(ScheduleController.class).one(model.getId())).withSelfRel());
		
		return model;
	}
	
	@Override
	public CollectionModel<ScheduleModel> toCollectionModel(Iterable<? extends ScheduleModel> entities) {

		CollectionModel<ScheduleModel> models = super.toCollectionModel(entities);
		
		return models;
	}
}
