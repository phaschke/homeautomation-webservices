package com.peterchaschke.homeautomation.scheduler;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "scheduler")
@Getter
@Setter
@RequiredArgsConstructor
public class Schedule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private ScheduleType type;
	
	@Column(name = "minute")
	private Integer minute;
	
	@Column(name = "day")
	private Integer day;
	
	@Column(name = "days")
	private String days;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "time")
	private String time;
	
	@Column(name = "active")
	private int active;
	
	@Column(name = "action_id")
	private Long actionId;
	
	public ScheduleModel toModel() {
		
		ScheduleModel scheduleModel = new ScheduleModel();
		
		scheduleModel.setId(this.getId());
		scheduleModel.setName(this.getName());
		scheduleModel.setType(this.getType());
		if(this.getMinute() != null) {
			scheduleModel.setMinute(this.getMinute());
		}
		if(this.getDay() != null) {
			scheduleModel.setDay(this.getDay());
		}
		scheduleModel.setDays(this.getDays());
		scheduleModel.setDate(date);
		
		if(this.getTime() != null) {
			
			LocalTime localTime = LocalTime.parse(this.getTime());
			DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
			String formattedTime = localTime.format(timeFormatter).toString();
			scheduleModel.setTime(formattedTime);
		}
		
		scheduleModel.setActive(this.getActive());
		scheduleModel.setActionId(this.getActionId());
		
		return scheduleModel;
		
	}
}
