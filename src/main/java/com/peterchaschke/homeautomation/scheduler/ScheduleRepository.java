package com.peterchaschke.homeautomation.scheduler;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
	
	List<Schedule> findAllByOrderByNameAsc();
	List<Schedule> findAllByName(String name);
}
