package com.peterchaschke.homeautomation.scheduler;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@JsonRootName(value = "schedule")
@Relation(collectionRelation = "schedules")
@Getter
@Setter
@RequiredArgsConstructor
public class ScheduleModel extends RepresentationModel<ScheduleModel> {

	private Long id;
	private ScheduleType type;
	private String name;
	private Integer minute;
	private Integer day;
	private String days;
	private String date;
	private String time;
	private int active;
	private Long actionId;
}
