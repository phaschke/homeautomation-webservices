package com.peterchaschke.homeautomation.scheduler;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Schedules")
@RestController
@RequestMapping("/api/schedules")
public class ScheduleController {
	
	@Autowired
	private final ScheduleServiceImpl scheduleService;
	private final ScheduleResourceAssembler scheduleAssembler;
	
	ScheduleController(ScheduleServiceImpl scheduleService, ScheduleResourceAssembler scheduleAssembler) {
		this.scheduleService = scheduleService;
		this.scheduleAssembler = scheduleAssembler;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> one(@PathVariable Long id) {
		
		ScheduleModel scheduleModel = scheduleService.getSchedule(id);
		return new ResponseEntity<>(scheduleAssembler.toModel(scheduleModel), HttpStatus.OK);
	}
	
	@GetMapping("")
	public ResponseEntity<?> all(@RequestParam Optional<String> name, @RequestParam Optional<String> type) {
		
		HashMap<String, String> filters = new HashMap<String, String>();
		if (name.isPresent())
			filters.put("name", name.get().toUpperCase());
		if (type.isPresent())
			filters.put("type", type.get().toUpperCase());
		
		List<ScheduleModel> scheduleModels = scheduleService.getSchedules(filters);
		
		return new ResponseEntity<>(scheduleAssembler.toCollectionModel(scheduleModels), HttpStatus.OK);
		
	}
	
	@PostMapping("")
	public ResponseEntity<?> newSchedule(@RequestBody Schedule schedule) throws Exception {

		ScheduleModel scheduleModel = scheduleService.add(schedule);
		return new ResponseEntity<>(scheduleAssembler.toModel(scheduleModel), HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateEvent(@RequestBody Schedule savedSchedule, @PathVariable Long id) throws Exception {

		ScheduleModel scheduleModel = scheduleService.update(id, savedSchedule);
		return new ResponseEntity<>(scheduleAssembler.toModel(scheduleModel), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		scheduleService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@PatchMapping("/{id}/active")
	public ResponseEntity<?> setActive(@PathVariable Long id) throws Exception {

		ScheduleModel model = scheduleService.setActive(id);
		return new ResponseEntity<>(scheduleAssembler.toModel(model), HttpStatus.OK);
	}

	@PatchMapping("/{id}/inactive")
	public ResponseEntity<?> setInactive(@PathVariable Long id) throws Exception {

		ScheduleModel model = scheduleService.setInactive(id);
		return new ResponseEntity<>(scheduleAssembler.toModel(model), HttpStatus.OK);
	}

}
