package com.peterchaschke.homeautomation.scheduler;

import java.util.HashMap;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

public interface ScheduleService {
	
	@PreAuthorize("hasRole('ADMIN')")
	public ScheduleModel getSchedule(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	public List<ScheduleModel> getSchedules(HashMap<String, String> filters);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ScheduleModel add(Schedule schedule) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public ScheduleModel update(Long id, Schedule savedSchedule) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public void delete(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ScheduleModel setActive(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ScheduleModel setInactive(Long id);

}
