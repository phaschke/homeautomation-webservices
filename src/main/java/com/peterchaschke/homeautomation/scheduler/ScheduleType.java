package com.peterchaschke.homeautomation.scheduler;

public enum ScheduleType {
	HOURLY,
	DAILY,
	WEEKLY,
	MONTHLY,
	YEARLY,
	FIXED
}
