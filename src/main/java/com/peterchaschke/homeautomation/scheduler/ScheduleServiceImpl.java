package com.peterchaschke.homeautomation.scheduler;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService {
	
	@Autowired
	private final ScheduleRepository schedulerRepository;
	
	ScheduleServiceImpl(ScheduleRepository schedulerRepository) {
		this.schedulerRepository = schedulerRepository;
	}
	
	public ScheduleModel getSchedule(Long id) {
		
		Schedule schedule = schedulerRepository.findById(id).get();
		return schedule.toModel();
	}
	
	public List<ScheduleModel> getSchedules(HashMap<String, String> filters) {
		
		List<Schedule> schedulers = schedulerRepository.findAllByOrderByNameAsc();
		
		List<ScheduleModel> schedulerModels = schedulers.stream().map(scheduler -> {
				return scheduler.toModel();
			})
			.filter(filters.containsKey("name")
					? schedulerModel -> schedulerModel.getName().toUpperCase().startsWith(filters.get("name"))
					: schedulerModel -> true)
			.filter(filters.containsKey("type")
					? schedulerModel -> schedulerModel.getType().toString().toUpperCase().startsWith(filters.get("type"))
					: schedulerModel -> true)
			.collect(Collectors.toList());
			
		return schedulerModels;
	}

	public ScheduleModel add(Schedule schedule) throws Exception {
		
		if(schedulerRepository.findAllByName(schedule.getName().toUpperCase()).size() > 0) {
			throw new Exception("Schedule names must be unique.");
		}
		
		if(schedule.getDate() == "" || schedule.getDate() == null) {
			schedule.setDate(null);
		}
		
		Schedule addedSchedule = schedulerRepository.save(schedule);
		return addedSchedule.toModel();
		
	}

	public ScheduleModel update(Long id, Schedule savedSchedule) throws Exception {
		
		Schedule existing = schedulerRepository.findById(id).get();
		
		if(!savedSchedule.getName().toUpperCase().equals(existing.getName().toUpperCase())) {
			if(schedulerRepository.findAllByName(savedSchedule.getName().toUpperCase()).size() > 0) {
				throw new Exception("Schedule names must be unique.");
			}
		}
		
		existing.setName(savedSchedule.getName());
		existing.setType(savedSchedule.getType());
		existing.setActionId(savedSchedule.getActionId());
		existing.setActive(savedSchedule.getActive());
		existing.setTime(savedSchedule.getTime());
		existing.setMinute(savedSchedule.getMinute());
		existing.setDay(savedSchedule.getDay());
		existing.setDays(savedSchedule.getDays());
		existing.setDate(savedSchedule.getDate());
		
		Schedule updatedSchedule = schedulerRepository.save(existing);
		
		return updatedSchedule.toModel();
	}

	public void delete(Long id) {
		schedulerRepository.deleteById(id);
	}
	
	public ScheduleModel setActive(Long id) {
		Schedule schedule = schedulerRepository.findById(id).get();
		schedule.setActive(1);
		Schedule savedSchedule = schedulerRepository.save(schedule);
		return savedSchedule.toModel();
	}
	
	public ScheduleModel setInactive(Long id) {
		Schedule schedule = schedulerRepository.findById(id).get();
		schedule.setActive(0);
		Schedule savedSchedule = schedulerRepository.save(schedule);
		return savedSchedule.toModel();
	}
}
