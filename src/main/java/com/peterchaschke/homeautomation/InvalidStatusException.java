package com.peterchaschke.homeautomation;

public class InvalidStatusException extends RuntimeException {

	private static final long serialVersionUID = 1657790418368594384L;

	public InvalidStatusException() {
	}

	public InvalidStatusException(String message) {
		super(message);
	}

	public InvalidStatusException(Throwable cause) {
		super(cause);
	}

	public InvalidStatusException(String message, Throwable cause) {
		super(message, cause);
	}
}
