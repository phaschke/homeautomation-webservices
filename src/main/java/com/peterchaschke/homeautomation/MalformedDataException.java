package com.peterchaschke.homeautomation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)  
public class MalformedDataException extends RuntimeException {
	
	private static final long serialVersionUID = -54595413313908783L;
	
	private FieldError fieldError;
	
	public MalformedDataException(String field, Object rejectedValue, String message) {
		this.fieldError = new FieldError(field, rejectedValue, message);
	}
	
	public String getMessage() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Malformed Data Exception.\n");
		sb.append("Field: ");
		sb.append(fieldError.getFieldName());
		sb.append("\n");
		sb.append("Rejected Value: ");
		sb.append(fieldError.getRejectedValue());
		sb.append("\n");
		sb.append("Message: ");
		sb.append(fieldError.getMessage());
		sb.append("\n");
		
		return sb.toString();
	}
}
