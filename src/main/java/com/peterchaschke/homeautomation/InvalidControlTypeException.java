package com.peterchaschke.homeautomation;

public class InvalidControlTypeException extends RuntimeException {

	private static final long serialVersionUID = 1657790418368594384L;

	public InvalidControlTypeException() {
	}

	public InvalidControlTypeException(String message) {
		super(message);
	}

	public InvalidControlTypeException(Throwable cause) {
		super(cause);
	}

	public InvalidControlTypeException(String message, Throwable cause) {
		super(message, cause);
	}
}