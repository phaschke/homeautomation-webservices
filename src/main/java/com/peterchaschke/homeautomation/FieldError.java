package com.peterchaschke.homeautomation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FieldError {
	
	private String fieldName;
	private Object rejectedValue;
	private String message;
	
	public FieldError(String fieldName, Object rejectedValue, String message) {
		this.fieldName = fieldName;
		this.rejectedValue = rejectedValue;
		this.message = message;
	}

}
