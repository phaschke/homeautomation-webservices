package com.peterchaschke.homeautomation;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;  

@ResponseStatus(HttpStatus.NOT_FOUND)  
public class ResourceNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 63867767337838602L;
	
	private FieldError fieldError;
	
	public ResourceNotFoundException(String field, Object rejectedValue, String message) {
		this.fieldError = new FieldError(field, rejectedValue, message);
	}
	
	public String getMessage() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Resource not found.\n");
		sb.append("Field: ");
		sb.append(fieldError.getFieldName());
		sb.append("\n");
		sb.append("Rejected Value: ");
		sb.append(fieldError.getRejectedValue());
		sb.append("\n");
		sb.append("Message: ");
		sb.append(fieldError.getMessage());
		sb.append("\n");
		
		return sb.toString();
	}

}
