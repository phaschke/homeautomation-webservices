package com.peterchaschke.homeautomation.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.peterchaschke.homeautomation.action.ActionExecutionResult;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionTask;

public interface EventService {
	
	public List<EventModel> getUserCreatedEvents(HashMap<String, String> filters);
	public EventModel getEvent(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	public EventModel add(Event event);
	
	@PreAuthorize("hasRole('ADMIN')")
	public EventModel update(Long id, Event savedEvent);
	
	@PreAuthorize("hasRole('ADMIN')")
	public void delete(Long id);
	
	public EventModel generateActions(Long id) throws Exception;
	public EventModel deleteGeneratedActions(Long id);
	public EventModel setActive(Long id);
	public EventModel setInactive(Long id);
	public ActionTaskExecutionResult executeEventThenAction(ParsedActionTask parsedActionThen, boolean debug);
	public ArrayList<ActionExecutionResult> handleIncomingEvent(IncomingEvent incomingEvent);

}
