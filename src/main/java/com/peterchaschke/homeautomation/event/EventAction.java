package com.peterchaschke.homeautomation.event;

import com.peterchaschke.homeautomation.action.ActionConditionalExecutionResult;
import com.peterchaschke.homeautomation.action.ActionExecutorObject;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionConditional;
import com.peterchaschke.homeautomation.action.ParsedActionTask;

public class EventAction extends ActionExecutorObject {
	
	private Event event;
	
	public EventAction(Event event) {
		this.event = event;
	}
	
	@Override
	public ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf,
			boolean debug) {
		return null;
	}

	@Override
	public ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskType, boolean debug) {
		
		ActionTaskExecutionResult result = new ActionTaskExecutionResult();
		result.setErrorFlag(false);
		result.setResultMessage("TODO: Add event action logic");
		
		// TODO: Add logic to execute event action
		
		return result;
	}

}
