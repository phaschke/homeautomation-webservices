package com.peterchaschke.homeautomation.event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "events")
@Getter
@Setter
@RequiredArgsConstructor
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "control_id")
	private Long controlId;

	@Column(name = "action_id")
	private Long actionId;

	@Column(name = "event")
	private String event;

	@Column(name = "active")
	private int active;

	@Column(name = "user_created")
	private int userCreated;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Transient
	private String action;

	public Event(Long id, Long controlId, Long actionId, String event, int active, int userCreated, String title,
			String description) {
		this.id = id;
		this.controlId = controlId;
		this.actionId = actionId;
		this.event = event;
		this.active = active;
		this.title = title;
		this.userCreated = userCreated;
		this.description = description;
	}

	public Event(Long controlId, Long actionId, String event, int active, int userCreated, String title,
			String description) {
		this.controlId = controlId;
		this.actionId = actionId;
		this.event = event;
		this.active = active;
		this.title = title;
		this.userCreated = userCreated;
		this.description = description;
	}

	public Event(Long controlId, int active) {
		this.controlId = controlId;
		this.active = active;
	}
	
	public EventModel toModel() {
		
		EventModel eventModel = new EventModel();
		
		eventModel.setId(this.getId());
		eventModel.setControlId(this.getControlId());
		eventModel.setActionId(this.getActionId());
		eventModel.setEvent(this.getEvent());
		eventModel.setActive(this.getActive());
		eventModel.setUserCreated(this.getUserCreated());
		eventModel.setTitle(this.getTitle());
		eventModel.setDescription(this.getDescription());
		
		return eventModel;
		
	}

}
