package com.peterchaschke.homeautomation.event;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class EventResourceAssembler extends RepresentationModelAssemblerSupport<EventModel, EventModel> {

	public EventResourceAssembler() {
		super(EventController.class, EventModel.class);
	}

	@Override
	public EventModel toModel(EventModel eventModel) {

		eventModel.add(linkTo(methodOn(EventController.class).one(eventModel.getId())).withSelfRel());
		
		return eventModel;
	}

	@Override
	public CollectionModel<EventModel> toCollectionModel(Iterable<? extends EventModel> entities) {

		CollectionModel<EventModel> eventModels = super.toCollectionModel(entities);

		return eventModels;
	}

}
