package com.peterchaschke.homeautomation.event;

import java.util.ArrayList;

public class IncomingEvent {

	private int controlId;
	private String controlName;
	private String event;
	private ArrayList<EventParameter> parameters;

	public IncomingEvent() {
	}

	public int getControlId() {
		return controlId;
	}

	public void setControlId(int controlId) {
		this.controlId = controlId;
	}

	public String getControlName() {
		return controlName;
	}

	public void setControlName(String controlName) {
		this.controlName = controlName;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public ArrayList<EventParameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<EventParameter> parameters) {
		this.parameters = parameters;
	}

}
