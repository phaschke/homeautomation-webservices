package com.peterchaschke.homeautomation.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionExecution;
import com.peterchaschke.homeautomation.action.ActionExecutionResult;
import com.peterchaschke.homeautomation.action.ActionObjectType;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.action.ActionResource;
import com.peterchaschke.homeautomation.action.ActionServiceImpl;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedAction;
import com.peterchaschke.homeautomation.action.ParsedActionTask;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ControlRepository controlRepository;

	@Autowired
	private ActionServiceImpl actionService;
	
	@Autowired
	private ActionRepository actionRepository;

	public List<EventModel> getUserCreatedEvents(HashMap<String, String> filters) {

		List<Event> events = eventRepository.findByUserCreated(1);

		List<EventModel> eventModels = events.stream().map(event -> {
			EventModel eventModel = event.toModel();

			Control control = controlRepository.findById(event.getControlId())
					.orElseThrow(() -> new ResourceNotFoundException("controlId", event.getControlId(),
							"No controls found with id."));
			eventModel.setControl(control);

			// eventModel.setControl(controlService.getControlById());
			return eventModel;
		}).filter(filters.containsKey("title")
				? eventModel -> eventModel.getTitle().toUpperCase().startsWith(filters.get("title"))
				: eventModel -> true)
				.filter(filters.containsKey("event")
						? eventModel -> eventModel.getEvent().toUpperCase().startsWith(filters.get("event"))
						: eventModel -> true)
				.filter(filters.containsKey("controluniquename")
						? eventModel -> eventModel.getControl().getUniqueName().toUpperCase()
								.startsWith(filters.get("controluniquename"))
						: eventModel -> true)
				.collect(Collectors.toList());

		return eventModels;
	}

	public EventModel getEvent(Long id) {

		Event event = eventRepository.findById(id).get();

		EventModel eventModel = event.toModel();
		List<ActionResource> relatedActions = actionService.getActionsByNameLike(generateGeneratedActionName(id, ""));
		eventModel.setRelatedActions(relatedActions);

		Control control = controlRepository.findById(event.getControlId()).orElseThrow(
				() -> new ResourceNotFoundException("controlId", event.getControlId(), "No controls found with id."));

		eventModel.setControl(control);
		// eventModel.setControl(controlService.getControlById(event.getControlId()));

		return eventModel;
	}

	public EventModel add(Event event) {

		// Check to make sure an active same event is not already present
		Event existing = eventRepository.findByControlIdAndEventAndUserCreated(event.getControlId(), event.getEvent(),
				1);

		if (existing != null) {
			throw new ExisitingEventException("Existing Control/Event already exists. Cannot add duplicate.");
		}

		event.setUserCreated(1);
		event.setActive(1);

		Event newEvent = eventRepository.save(event);

		return newEvent.toModel();
	}

	public EventModel update(Long id, Event savedEvent) {

		// Check to make sure an active same event is not already present
		Event existing = eventRepository.findByControlIdAndEventAndUserCreated(savedEvent.getControlId(),
				savedEvent.getEvent(), 1);
		if (existing != null && existing.getId() != id) {
			throw new ExisitingEventException("Existing Control/Event already exists. Cannot add duplicate.");
		}

		// Duplicate does not exist for the requested update, pull existing event to
		// update
		existing = eventRepository.findById(id).get();

		existing.setControlId(savedEvent.getControlId());
		existing.setActionId(savedEvent.getActionId());
		existing.setAction(savedEvent.getAction());
		existing.setEvent(savedEvent.getEvent());
		existing.setActive(savedEvent.getActive());
		existing.setUserCreated(1);
		existing.setTitle(savedEvent.getTitle());
		existing.setDescription(savedEvent.getDescription());

		Event updatedEvent = eventRepository.save(existing);
		return updatedEvent.toModel();

	}

	public void delete(Long id) {

		deleteGeneratedActions(id);
		eventRepository.deleteById(id);
	}

	public EventModel generateActions(Long id) throws Exception {

		deleteGeneratedActions(id);
		Event event = eventRepository.findById(id).get();

		ParsedActionTask parsedActionThenEnable = new ParsedActionTask();
		parsedActionThenEnable.setObjectId(id);
		parsedActionThenEnable.setAction("ENABLE");
		parsedActionThenEnable.setObjectType(ActionObjectType.EVENT);
		ArrayList<ParsedActionTask> parsedActionThenArrayEnable = new ArrayList<ParsedActionTask>(
				Arrays.asList(parsedActionThenEnable));
		ParsedAction parsedActionEnable = new ParsedAction();
		parsedActionEnable.setThens(parsedActionThenArrayEnable);

		ParsedActionTask parsedActionThenDisable = new ParsedActionTask();
		parsedActionThenDisable.setObjectId(id);
		parsedActionThenDisable.setAction("DISABLE");
		parsedActionThenDisable.setObjectType(ActionObjectType.EVENT);
		ArrayList<ParsedActionTask> parsedActionThenArrayDisable = new ArrayList<ParsedActionTask>(
				Arrays.asList(parsedActionThenDisable));
		ParsedAction parsedActionDisable = new ParsedAction();
		parsedActionDisable.setThens(parsedActionThenArrayDisable);

		JSONObject actionJsonObjEnable = new JSONObject(parsedActionEnable);
		JSONObject actionJsonObjDisable = new JSONObject(parsedActionDisable);
		
		Action enableAction = new Action();
		enableAction.setName(generateGeneratedActionName(id, "ENABLE"));
		enableAction.setTitle(generateGeneratedActionTitle(event.getTitle(), "Enable"));
		enableAction.setIfExecution(ActionExecution.PARALLEL);
		enableAction.setThenExecution(ActionExecution.PARALLEL);
		enableAction.setElseExecution(ActionExecution.PARALLEL);
		enableAction.setThens(actionJsonObjEnable.toString());
		enableAction.setUserCreated(0);
		
		Action disableAction = new Action();
		disableAction.setName(generateGeneratedActionName(id, "DISABLE"));
		disableAction.setTitle(generateGeneratedActionTitle(event.getTitle(), "Disable"));
		disableAction.setIfExecution(ActionExecution.PARALLEL);
		disableAction.setThenExecution(ActionExecution.PARALLEL);
		disableAction.setElseExecution(ActionExecution.PARALLEL);
		disableAction.setThens(actionJsonObjDisable.toString());
		disableAction.setUserCreated(0);

		actionRepository.save(enableAction);
		actionRepository.save(disableAction);

		return getEvent(id);
	}

	private String generateGeneratedActionName(Long eventId, String type) {

		StringBuilder sb = new StringBuilder();
		sb.append("GENERATED-EVENT-");
		sb.append(eventId);
		sb.append("-");
		sb.append(type);

		return sb.toString();
	}

	private String generateGeneratedActionTitle(String eventTitle, String type) {

		StringBuilder sb = new StringBuilder();
		sb.append("Event_");
		sb.append(eventTitle.replace(" ", ""));
		sb.append("_");
		sb.append(type);

		return sb.toString();
	}

	public EventModel deleteGeneratedActions(Long id) {

		String name = generateGeneratedActionName(id, "");
		actionRepository.deleteByNameContaining(name);

		return getEvent(id);
	}

	public EventModel setActive(Long id) {
		Event event = eventRepository.findById(id).get();
		event.setActive(1);
		Event savedEvent = eventRepository.save(event);
		return savedEvent.toModel();
	}

	public EventModel setInactive(Long id) {
		Event event = eventRepository.findById(id).get();
		event.setActive(0);
		Event savedEvent = eventRepository.save(event);
		return savedEvent.toModel();
	}

	public ActionTaskExecutionResult executeEventThenAction(ParsedActionTask parsedActionThen, boolean debug) {

		ActionTaskExecutionResult result = new ActionTaskExecutionResult();

		if (parsedActionThen.getAction().equals("ENABLE")) {
			//setActive(parsedActionThen.getEvent().getId());
			setActive(parsedActionThen.getObjectId());
			result.setErrorFlag(false);

		} else if (parsedActionThen.getAction().equals("DISABLE")) {
			//setInactive(parsedActionThen.getEvent().getId());
			setInactive(parsedActionThen.getObjectId());
			result.setErrorFlag(false);

		} else {
			result.setResultMessage("Unknown event then action.");
			result.setErrorFlag(true);
		}

		return result;

	}

	public ArrayList<ActionExecutionResult> handleIncomingEvent(IncomingEvent incomingEvent) {

		ArrayList<ActionExecutionResult> actionExecutionResults = new ArrayList<ActionExecutionResult>();

		Control control = controlRepository.findByUniqueName(incomingEvent.getControlName());

		if (control == null) {
			return actionExecutionResults;
		}

		List<Event> events = eventRepository.findByControlIdAndEventAndActive(control.getId(), incomingEvent.getEvent(),
				1);

		events.forEach(event -> {

			ActionExecutionResult result;

			result = actionService.executeAction(event.getActionId(), false);
			actionExecutionResults.add(result);

		});

		return actionExecutionResults;
	}

}
