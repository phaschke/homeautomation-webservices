package com.peterchaschke.homeautomation.event;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.homeautomation.action.ActionResource;
import com.peterchaschke.homeautomation.controls.Control;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@JsonRootName(value = "event")
@Relation(collectionRelation = "events")
@Getter
@Setter
@RequiredArgsConstructor
public class EventModel extends RepresentationModel<EventModel> {

	private Long id;
	private Long controlId;
	private Long actionId;
	private String event;
	private int active;
	private int userCreated;
	private String title;
	private String description;
	private Control control;
	private List<ActionResource> relatedActions;

}
