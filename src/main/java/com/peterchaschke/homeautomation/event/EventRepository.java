package com.peterchaschke.homeautomation.event;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

	List<Event> findByUserCreated(int userCreated);
	List<Event> findByControlId(Long controlId);
	List<Event> findByControlIdAndEventAndActive(Long controlId, String event, int active);
	Event findByControlIdAndEventAndUserCreated(Long controlId, String event, int userCreated);
	
	
}
