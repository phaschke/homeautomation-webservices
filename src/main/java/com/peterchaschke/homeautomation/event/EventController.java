package com.peterchaschke.homeautomation.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peterchaschke.homeautomation.action.ActionExecutionResult;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Events")
@RestController
@RequestMapping("/api/events")
public class EventController {

	@Autowired
	private final EventServiceImpl eventService;
	private final EventResourceAssembler eventResourceAssembler;

	EventController(EventServiceImpl eventService, EventResourceAssembler eventResourceAssembler) {
		this.eventService = eventService;
		this.eventResourceAssembler = eventResourceAssembler;
	}

	// Get all events
	@GetMapping("")
	public ResponseEntity<?> all(@RequestParam Optional<String> title, @RequestParam Optional<String> event,
			@RequestParam Optional<String> controluniquename) {

		HashMap<String, String> filters = new HashMap<String, String>();
		if (title.isPresent())
			filters.put("title", title.get().toUpperCase());
		if (event.isPresent())
			filters.put("event", event.get().toUpperCase());
		if (controluniquename.isPresent())
			filters.put("controluniquename", controluniquename.get().toUpperCase());

		List<EventModel> eventModels = eventService.getUserCreatedEvents(filters);

		return new ResponseEntity<>(eventResourceAssembler.toCollectionModel(eventModels), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> one(@PathVariable Long id) {

		EventModel model = eventResourceAssembler.toModel(eventService.getEvent(id));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<EventModel> newEvent(@RequestBody Event event) throws ExisitingEventException {

		EventModel model = eventResourceAssembler.toModel(eventService.add(event));
		return new ResponseEntity<EventModel>(model, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<EventModel> updateEvent(@RequestBody Event savedEvent, @PathVariable Long id)
			throws ExisitingEventException {

		EventModel model = eventResourceAssembler.toModel(eventService.update(id, savedEvent));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		eventService.delete(id);

		return ResponseEntity.noContent().build();
	}
	
	@PatchMapping("/{id}/generate_actions")
	public ResponseEntity<?> generateActions(@PathVariable Long id) throws Exception {

		EventModel model = eventResourceAssembler.toModel(eventService.generateActions(id));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}
	
	@PatchMapping("/{id}/delete_actions")
	public ResponseEntity<?> deleteActions(@PathVariable Long id) throws Exception {

		EventModel model = eventResourceAssembler.toModel(eventService.deleteGeneratedActions(id));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}

	@PatchMapping("/{id}/active")
	public ResponseEntity<?> setActive(@PathVariable Long id) throws Exception {

		EventModel model = eventResourceAssembler.toModel(eventService.setActive(id));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}

	@PatchMapping("/{id}/inactive")
	public ResponseEntity<?> setInactive(@PathVariable Long id) throws Exception {

		EventModel model = eventResourceAssembler.toModel(eventService.setInactive(id));
		return new ResponseEntity<EventModel>(model, HttpStatus.OK);
	}

	// Handle an incoming event
	@PostMapping("/incoming")
	public ResponseEntity<?> handleIncomingEvent(@RequestBody IncomingEvent incomingEvent) {

		ArrayList<ActionExecutionResult> resultList = eventService.handleIncomingEvent(incomingEvent);

		return new ResponseEntity<ArrayList<ActionExecutionResult>>(resultList, HttpStatus.OK);

	}

}
