package com.peterchaschke.homeautomation.event;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.controls.Control;

@Service
public class EventBuilderService {

	@Autowired
	private EventRepository eventRepository;
	
	public List<Event> getEventsByControlId(Long controlId) {
		
		List<Event> events = eventRepository.findByControlId(controlId);
		return events;
	}
	
	public void inactivateEvent(String eventName, Control control, int userCreated) {
		
		Event event = eventRepository.findByControlIdAndEventAndUserCreated(control.getId(), eventName, userCreated);
		if(event == null) return;
		event.setActive(0);
		eventRepository.save(event);
	}
	
}
