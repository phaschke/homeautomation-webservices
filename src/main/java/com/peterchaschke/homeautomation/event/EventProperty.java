package com.peterchaschke.homeautomation.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class EventProperty {
	
	private String name;
	private boolean custom;
}
