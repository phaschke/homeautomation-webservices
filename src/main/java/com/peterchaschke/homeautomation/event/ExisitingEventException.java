package com.peterchaschke.homeautomation.event;

public class ExisitingEventException extends RuntimeException {

	private static final long serialVersionUID = 1657790418368524384L;

	public ExisitingEventException() {
	}

	public ExisitingEventException(String message) {
		super(message);
	}

	public ExisitingEventException(Throwable cause) {
		super(cause);
	}

	public ExisitingEventException(String message, Throwable cause) {
		super(message, cause);
	}
}