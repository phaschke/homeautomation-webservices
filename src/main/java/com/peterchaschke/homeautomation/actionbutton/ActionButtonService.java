package com.peterchaschke.homeautomation.actionbutton;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.peterchaschke.homeautomation.action.ActionExecutionResult;

public interface ActionButtonService {
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public ActionButton getActionButton(Long id);
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<ActionButton> getActionButtons();
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<ActionButton> getActionButtonsByTopic(Long topicId);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ActionButton add(ActionButton actionButton);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ActionButton update(Long id, ActionButton actionButtonToUpdate);
	
	@PreAuthorize("hasRole('ADMIN')")
	public void delete(Long id);
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public ActionExecutionResult runActionButton(Long id);

}
