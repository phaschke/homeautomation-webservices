package com.peterchaschke.homeautomation.actionbutton;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.peterchaschke.homeautomation.action.ActionExecutionResult;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Action Buttons")
@RestController
@RequestMapping("/api/actions/buttons/")
public class ActionButtonController {

	private final ActionButtonServiceImpl actionButtonService;
	private final ActionButtonResourceAssembler actionButtonResourceAssembler;

	ActionButtonController(ActionButtonServiceImpl actionButtonService, ActionButtonResourceAssembler actionButtonResourceAssembler) {
		this.actionButtonService = actionButtonService;
		this.actionButtonResourceAssembler = actionButtonResourceAssembler;
	}

	@GetMapping("")
	public ResponseEntity<?> all() {

		List<ActionButton> actionButtons = actionButtonService.getActionButtons();

		return new ResponseEntity<>(actionButtonResourceAssembler.toCollectionModel(actionButtons), HttpStatus.OK);
	}

	@GetMapping("/bytopic/{id}")
	public ResponseEntity<?> allByTopicId(@PathVariable Long id) {

		List<ActionButton> actionButtons = actionButtonService.getActionButtonsByTopic(id);

		return new ResponseEntity<>(actionButtonResourceAssembler.toCollectionModel(actionButtons), HttpStatus.OK);
		
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> one(@PathVariable Long id) {

		ActionButton actionButton = actionButtonService.getActionButton(id);

		ActionButtonModel model = actionButtonResourceAssembler.toModel(actionButton);
		return new ResponseEntity<ActionButtonModel>(model, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<ActionButtonModel> post(@RequestBody ActionButton newScenario) throws URISyntaxException {

		ActionButton addedScenario = actionButtonService.add(newScenario);
		ActionButtonModel model = actionButtonResourceAssembler.toModel(addedScenario);

		return new ResponseEntity<ActionButtonModel>(model, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ActionButtonModel> update(@RequestBody ActionButton scenarioToUpdate, @PathVariable Long id)
			throws URISyntaxException {

		ActionButton updatedScenario = actionButtonService.update(id, scenarioToUpdate);

		ActionButtonModel model = actionButtonResourceAssembler.toModel(updatedScenario);
		return new ResponseEntity<ActionButtonModel>(model, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		actionButtonService.delete(id);

		return ResponseEntity.noContent().build();
	}

	@PatchMapping("/{id}")
	public ResponseEntity<ActionExecutionResult> onActionButtonPress(@PathVariable Long id)
			throws Exception {

		ActionExecutionResult result = actionButtonService.runActionButton(id);

		return new ResponseEntity<ActionExecutionResult>(result, HttpStatus.OK);

	}
}
