package com.peterchaschke.homeautomation.actionbutton;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.action.ActionExecutionResult;
import com.peterchaschke.homeautomation.action.ActionServiceImpl;

@Service
public class ActionButtonServiceImpl implements ActionButtonService {

	@Autowired
	private final ActionButtonRepository actionButtonRepository;
	private final ActionServiceImpl actionService;

	public ActionButtonServiceImpl(ActionButtonRepository actionButtonRepository, ActionServiceImpl actionService) {
		this.actionButtonRepository = actionButtonRepository;
		this.actionService = actionService;
	}

	public ActionButton getActionButton(Long id) {

		return actionButtonRepository.getOne(id);
	}

	public List<ActionButton> getActionButtons() {

		return actionButtonRepository.findAllByOrderByTitleAsc();
	}

	public List<ActionButton> getActionButtonsByTopic(Long topicId) {

		return actionButtonRepository.findByTopicId(topicId);
	}

	public ActionButton add(ActionButton actionButton) {

		return actionButtonRepository.save(actionButton);
	}

	public ActionButton update(Long id, ActionButton actionButtonToUpdate) {

		ActionButton updatedActionButton = actionButtonRepository.findById(id).map(actionButton -> {
			actionButton.setTitle(actionButtonToUpdate.getTitle());
			actionButton.setTopicId(actionButtonToUpdate.getTopicId());
			actionButton.setActionId(actionButtonToUpdate.getActionId());
			actionButton.setDescription(actionButtonToUpdate.getDescription());
			actionButton.setIcon(actionButtonToUpdate.getIcon());

			return actionButtonRepository.save(actionButton);

		}).orElseGet(() -> {
			actionButtonToUpdate.setId(id);
			return actionButtonRepository.save(actionButtonToUpdate);
		});

		return updatedActionButton;
	}

	public void delete(Long id) {

		actionButtonRepository.deleteById(id);
	}

	public ActionExecutionResult runActionButton(Long id) {

		ActionButton actionButton = actionButtonRepository.getOne(id);
		ActionExecutionResult result = actionService.executeAction(actionButton.getActionId(), false);

		return result;
	}

}
