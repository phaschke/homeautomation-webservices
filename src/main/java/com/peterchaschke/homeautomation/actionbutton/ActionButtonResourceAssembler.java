package com.peterchaschke.homeautomation.actionbutton;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ActionButtonResourceAssembler extends RepresentationModelAssemblerSupport<ActionButton, ActionButtonModel> {

	public ActionButtonResourceAssembler() {
		super(ActionButtonController.class, ActionButtonModel.class);
	}

	@Override
	public ActionButtonModel toModel(ActionButton entity) {

		ActionButtonModel scenarioModel = instantiateModel(entity);

		scenarioModel.add(linkTo(methodOn(ActionButtonController.class).one(entity.getId())).withSelfRel());

		scenarioModel.setId(entity.getId());
		scenarioModel.setTopicId(entity.getTopicId());
		scenarioModel.setActionId(entity.getActionId());
		scenarioModel.setTitle(entity.getTitle());
		scenarioModel.setDescription(entity.getDescription());
		scenarioModel.setIcon(entity.getIcon());

		return scenarioModel;
	}

	@Override
	public CollectionModel<ActionButtonModel> toCollectionModel(Iterable<? extends ActionButton> entities) {

		CollectionModel<ActionButtonModel> scenarioModels = super.toCollectionModel(entities);

		return scenarioModels;
	}

}
