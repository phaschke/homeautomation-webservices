package com.peterchaschke.homeautomation.actionbutton;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionButtonRepository extends JpaRepository<ActionButton, Long> {
	
	List<ActionButton> findAllByOrderByTitleAsc();
	List<ActionButton> findByTopicId(Long id);

}
