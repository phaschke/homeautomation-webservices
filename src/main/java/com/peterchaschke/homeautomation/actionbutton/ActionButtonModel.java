package com.peterchaschke.homeautomation.actionbutton;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "scenario")
@Relation(collectionRelation = "scenarios")
@Getter
@Setter
public class ActionButtonModel extends RepresentationModel<ActionButtonModel> {

	private Long id;
	private Long topicId;
	private Long actionId;
	private String title;
	private String description;
	private String icon;

}
