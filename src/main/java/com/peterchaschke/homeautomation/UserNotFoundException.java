package com.peterchaschke.homeautomation;

//TODO: Remove this exception after refactoring to use Resource not found
public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1681790418321594354L;

	public UserNotFoundException() {
	}

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(Throwable cause) {
		super(cause);
	}

	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}