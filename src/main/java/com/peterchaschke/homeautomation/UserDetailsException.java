package com.peterchaschke.homeautomation;

public class UserDetailsException extends RuntimeException {

	private static final long serialVersionUID = 1658790412368594384L;
	
	public UserDetailsException() {
	}

	public UserDetailsException(String message) {
		super(message);
	}

	public UserDetailsException(Throwable cause) {
		super(cause);
	}

	public UserDetailsException(String message, Throwable cause) {
		super(message, cause);
	}

}
