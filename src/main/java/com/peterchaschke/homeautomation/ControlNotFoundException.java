package com.peterchaschke.homeautomation;

//TODO: Remove this exception after refactoring to use Resource not found
public class ControlNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1657790418368594354L;

	public ControlNotFoundException() {
	}

	public ControlNotFoundException(String message) {
		super(message);
	}

	public ControlNotFoundException(Throwable cause) {
		super(cause);
	}

	public ControlNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}