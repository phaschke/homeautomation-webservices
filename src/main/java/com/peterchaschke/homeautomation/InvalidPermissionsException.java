package com.peterchaschke.homeautomation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)  
public class InvalidPermissionsException extends RuntimeException {

	private static final long serialVersionUID = 5897805805383144552L;
	
	private FieldError fieldError;
	
	public InvalidPermissionsException(String field, Object rejectedValue, String message) {
		this.fieldError = new FieldError(field, rejectedValue, message);
	}
	
	public String getMessage() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Invalid permssions.\n");
		sb.append("Field: ");
		sb.append(fieldError.getFieldName());
		sb.append("\n");
		sb.append("Rejected Value: ");
		sb.append(fieldError.getRejectedValue());
		sb.append("\n");
		sb.append("Message: ");
		sb.append(fieldError.getMessage());
		sb.append("\n");
		
		return sb.toString();
	}
}
