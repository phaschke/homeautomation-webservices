package com.peterchaschke.homeautomation.notifier;

import com.peterchaschke.homeautomation.action.ActionConditionalExecutionResult;
import com.peterchaschke.homeautomation.action.ActionExecutorObject;
import com.peterchaschke.homeautomation.action.ActionTaskExecutionResult;
import com.peterchaschke.homeautomation.action.ParsedActionConditional;
import com.peterchaschke.homeautomation.action.ParsedActionTask;

public class Notifier extends ActionExecutorObject {

	@Override
	public ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf,
			boolean debug) {
		return null;
	}

	@Override
	public ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskType, boolean debug) {
		
		//TODO: Add notificaton logic
		
		System.out.println("Notification");
		
		ActionTaskExecutionResult result = new ActionTaskExecutionResult();
		result.setErrorFlag(false);
		result.setResultMessage("TODO: Add notifier action logic");
		
		return result;
	}

}
