package com.peterchaschke.homeautomation.action;

import javax.validation.constraints.NotBlank;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ParsedActionParameter {
	
	private String parameterName;
	
	@NotBlank
	private String key;
	
	@NotBlank
	private String value;

	public ParsedActionParameter(String parameterName, String key, String value) {
		this.parameterName = parameterName;
		this.key = key;
		this.value = value;
	}
	
	public ParsedActionParameter(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
}
