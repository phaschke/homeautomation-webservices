package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ParsedAction {

	private ArrayList<ParsedActionConditional> conditionals;
	private ArrayList<ParsedActionTask> thens;
	private ArrayList<ParsedActionTask> elses;
	
	public ParsedAction(ArrayList<ParsedActionConditional> conditionals, ArrayList<ParsedActionTask> thens, ArrayList<ParsedActionTask> elses) {
		this.conditionals = conditionals;
		this.thens = thens;
		this.elses = elses;
	}
	
}
