package com.peterchaschke.homeautomation.action;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ParsedActionConditional {

	@NotNull
	private Long objectId;
	
	@NotNull
	private ActionObjectType type;
	
	@NotBlank
	private String conditional;
	
	@NotBlank
	private String parameter;
	
	@NotBlank
	private String comparator;
	
	@NotBlank
	private String value;
	
	protected int order = 0;

	public ParsedActionConditional(Long objectId, ActionObjectType type, String conditional, String parameter,
			String comparator, String value, int order) {
		this.objectId = objectId;
		this.type = type;
		this.conditional = conditional;
		this.parameter = parameter;
		this.comparator = comparator;
		this.value = value;
		this.order = order;
	}

	public ParsedActionConditional(ParsedActionConditional parsedActionConditional) {
		this.objectId = parsedActionConditional.objectId;
		this.type = parsedActionConditional.type;
		this.conditional = parsedActionConditional.conditional;
		this.parameter = parsedActionConditional.parameter;
		this.comparator = parsedActionConditional.comparator;
		this.value = parsedActionConditional.value;
		this.order = parsedActionConditional.order;
	}

}
