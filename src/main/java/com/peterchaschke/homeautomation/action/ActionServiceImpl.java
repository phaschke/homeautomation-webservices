package com.peterchaschke.homeautomation.action;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;

@Service
public class ActionServiceImpl implements ActionService {
	
	@Autowired
	private ActionRepository actionRepository;
	
	@Autowired
	private ActionValidator actionValidator;
	
	@Autowired
	private ActionExecutor actionExecutor;
	

	public List<ActionResource> getActions(HashMap<String, String> filters) {
		
		List<Action> actions = null;
		if(filters.containsKey("userCreated")) {
			actions = actionRepository.findByOrderByTitleAsc();
		} else {
			actions = actionRepository.findByUserCreatedOrderByTitleAsc(1);
		}

		List<ActionResource> actionModels = actions.stream().map(action -> {
			return action.toResource();
		}).filter(filters.containsKey("title")
			? actionModel -> actionModel.getTitle().toUpperCase().startsWith(filters.get("title"))
			: actionModel -> true)
		.filter(filters.containsKey("name")
				? actionModel -> actionModel.getName().toUpperCase().startsWith(filters.get("name"))
				: actionModel -> true)
		.collect(Collectors.toList());

		return actionModels;
	}

	public ActionResource getAction(Long id) {
		
		Action action = actionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("actionId", id, "No actions found."));
		
		return action.toResource();
	}

	public ActionResource createAction(ActionResource actionResource) throws Exception, UniqueConstraintViolationException {
		
		actionResource.setId(null);
		
		if (actionRepository.findByName(actionResource.getName()) != null) {
			throw new UniqueConstraintViolationException("name", actionResource.getName(), "Action name must be a unique string.");
		}
		
		ParsedAction parsedAction = ActionParser.parseAction(actionResource);
		actionValidator.validateAction(parsedAction);

		actionResource.setUserCreated(1);

		Action newAction = actionRepository.save(actionResource.toEntity());

		return newAction.toResource();
	}

	public ActionResource updateAction(Long id, ActionResource updatedActionResource) throws Exception {
		
		Action action = actionRepository.findById(updatedActionResource.getId())
				.orElseThrow(() -> new ResourceNotFoundException("actionId", updatedActionResource.getId(), "No actions found."));
		
		Action actionWithSameName = actionRepository.findByName(updatedActionResource.getName());
		if (actionWithSameName.getId() != action.getId()) {
			throw new UniqueConstraintViolationException("actionName", action.getName(), "Action name must be a unique string.");
		}
		
		ActionParser.parseAction(updatedActionResource);
		
		action.setTitle(updatedActionResource.getTitle());
		action.setName(updatedActionResource.getName());
		action.setDescription(updatedActionResource.getDescription());
		action.setIfExecution(updatedActionResource.getIfExecution());
		action.setThenExecution(updatedActionResource.getThenExecution());
		action.setElseExecution(updatedActionResource.getElseExecution());
		action.setIfs(updatedActionResource.getIfs());
		action.setThens(updatedActionResource.getThens());
		action.setElses(updatedActionResource.getElses());
		action.setUserCreated(1);
		
		Action savedAction = actionRepository.save(action);

		return savedAction.toResource();
		
	}
	
	public void deleteAction(Long actionId) {
		
		actionRepository.deleteById(actionId);
	}
	
	public void executeActionWrapper(Long actionId) throws Exception {
		
		ActionExecutionResult actionExecutionResult = executeAction(actionId, true);
		
		if(!actionExecutionResult.isSuccessFlag()) {
			String lastMessage = actionExecutionResult.getLog().get(actionExecutionResult.getLog().size() - 1);
			
			throw new Exception(lastMessage);
		}
	}
	
	
	public ActionExecutionResult executeAction(Long actionId, Boolean debug) {
		
		ActionExecutionResult actionExecutionResult = new ActionExecutionResult();
		
		Instant start = Instant.now();
		
		Optional<Action> optAction = actionRepository.findById(actionId);
		if(optAction.isEmpty()) {
			throw new ResourceNotFoundException("actionId", actionId, "No actions with id found.");
		}
		Action action = optAction.get();
		
		ActionResource actionResource = action.toResource();
		
		ParsedAction parsedAction = null;
		try {
			parsedAction = ActionParser.parseAction(actionResource);
			actionResource.setParsedAction(parsedAction);
			actionExecutionResult.appendLog("Parsed action successfully");
			
		} catch (Exception e) {
			
			actionExecutionResult.appendLog(String.format("Failed To parse action: %s", e.getMessage()));
			actionExecutionResult.setTimeDuration(getTotalDuration(start));
			return actionExecutionResult;
		}
		
		
		ExecutableAction executableAction = null;
		try {
			executableAction = actionValidator.validateAndBuildExecutableAction(actionResource);
			actionExecutionResult.appendLog("Validated and build action successfully");
		
		} catch (Exception e) {
			
			actionExecutionResult.appendLog(String.format("Failed To validate and build action: %s", e.getMessage()));
			actionExecutionResult.setTimeDuration(getTotalDuration(start));
			return actionExecutionResult;
		}
		
		try {
			//ActionExecutor actionExecutor = new ActionExecutor(actionExecutionResult);
			actionExecutor.setActionExecutionResult(actionExecutionResult);
			actionExecutionResult = actionExecutor.executeAction(executableAction, actionResource);
			actionExecutionResult.appendLog("Executed action successfully");
			
		} catch (Exception e) {
			
			actionExecutionResult.appendLog(String.format("Failed To execute action: %s", e.getMessage()));
			actionExecutionResult.setTimeDuration(getTotalDuration(start));
			return actionExecutionResult;
		}
		
		actionExecutionResult.setTimeDuration(getTotalDuration(start));
		
		return actionExecutionResult;
	}
	
	private long getTotalDuration(Instant start) {
		Instant end = Instant.now();
		Duration timeElapsed = Duration.between(start, end);
		
		return timeElapsed.getNano();
	}
	
	public List<ActionResource> getActionsByNameLike(String name) {
		List<Action> actions = actionRepository.findByNameContaining(name);
		List<ActionResource> actionModels = actions.stream().map(action -> action.toResource()).collect(Collectors.toList());
		return actionModels;
	}
	
}
