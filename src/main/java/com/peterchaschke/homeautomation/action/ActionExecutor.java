package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import lombok.Setter;

@Component
@Setter
public class ActionExecutor {

	private ActionExecutionResult actionExecutionResult;

	public ActionExecutionResult executeAction(ExecutableAction executableAction, ActionResource actionResource) {

		actionExecutionResult.appendLog("Starting action execution");

		ArrayList<ActionConditionalExecutionResult> conditionalResults = executeConditionals(
				executableAction.getConditionals(), actionResource.getIfExecution());

		validateConditionalsResults(conditionalResults);

		if (!actionExecutionResult.isSuccessFlag()) {
			// Conditional is ERROR, end action execution
			return actionExecutionResult;
		}

		if (actionExecutionResult.isConditionalEvaluation()) {
			// Conditional is TRUE, execute then tasks
			ArrayList<ActionTaskExecutionResult> thenTaskResults = executeTasks(executableAction.getThens(),
					actionResource.getThenExecution(), "Then");
			validateTasksResults(thenTaskResults, "Then");

		} else {
			// Conditional is ELSE, execute else tasks
			ArrayList<ActionTaskExecutionResult> elseTaskResults = executeTasks(executableAction.getElses(),
					actionResource.getElseExecution(), "Else");
			validateTasksResults(elseTaskResults, "Else");

		}

		return actionExecutionResult;
	}

	private ArrayList<ActionConditionalExecutionResult> executeConditionals(
			ArrayList<ExecutableActionConditional> conditionals, ActionExecution executionMode) {

		ArrayList<ActionConditionalExecutionResult> conditionalResults = new ArrayList<ActionConditionalExecutionResult>();
		
		if(conditionals == null ) return conditionalResults;

		actionExecutionResult.appendLog(String.format("If execution mode is %s", executionMode));

		if (executionMode.equals(ActionExecution.PARALLEL)) {

			conditionals.stream().parallel().forEach(executableActionConditional -> {

				ActionConditionalExecutionResult result = executableActionConditional.getExecutableObject()
						.executeConditionalAction(executableActionConditional, true);
				conditionalResults.add(result);
			});

		} else {

			for (int i = 0; i < conditionals.size(); i++) {

				ExecutableActionConditional executableActionConditional = conditionals.get(i);
				ActionConditionalExecutionResult result = executableActionConditional.getExecutableObject()
						.executeConditionalAction(executableActionConditional, true);
				conditionalResults.add(result);
			}
		}

		return conditionalResults;
	}

	private void validateConditionalsResults(ArrayList<ActionConditionalExecutionResult> conditionalResults) {
		
		if(conditionalResults.size() == 0) {
			actionExecutionResult.appendLog("No if conditionals, returning true");
			actionExecutionResult.setSuccessFlag(true);
			actionExecutionResult.setConditionalEvaluation(true);
			return;
		}

		boolean continueAction = true;
		boolean isError = false;

		for (int i = 0; i < conditionalResults.size(); i++) {
			ActionConditionalExecutionResult result = conditionalResults.get(i);

			actionExecutionResult.appendLog(result.getResultMessage());

			if (result.isErrorFlag()) {
				continueAction = false;
				isError = true;
			}
			if (!result.isTrueFlag()) {
				continueAction = false;
			}
		}

		if (!continueAction) {
			if (isError) {
				actionExecutionResult.appendLog("If evaluation resulted in an error. Ending action execution");
				actionExecutionResult.setSuccessFlag(false);

			} else {
				actionExecutionResult.appendLog("If evaluation returned false");
				actionExecutionResult.setSuccessFlag(true);
				actionExecutionResult.setConditionalEvaluation(false);
			}
		} else {
			actionExecutionResult.appendLog("If evaluation returned true");
			actionExecutionResult.setSuccessFlag(true);
			actionExecutionResult.setConditionalEvaluation(true);
		}
	}

	private ArrayList<ActionTaskExecutionResult> executeTasks(ArrayList<ExecutableActionTask> tasks,
			ActionExecution executionMode, String taskType) {

		ArrayList<ActionTaskExecutionResult> taskResults = new ArrayList<ActionTaskExecutionResult>();
		
		if(tasks == null) return taskResults;

		actionExecutionResult.appendLog(String.format("%s execution mode is %s", taskType, executionMode));

		if (executionMode.equals(ActionExecution.PARALLEL)) {

			tasks.stream().parallel().forEach(executableActionTask -> {

				ActionTaskExecutionResult result = executableActionTask.getExecutableObject()
						.executeTaskAction(executableActionTask, taskType, true);
				taskResults.add(result);
			});

		} else {

			for (int i = 0; i < tasks.size(); i++) {

				ExecutableActionTask executableActionTask = tasks.get(i);
				ActionTaskExecutionResult result = executableActionTask.getExecutableObject()
						.executeTaskAction(executableActionTask, taskType, true);
				taskResults.add(result);
			}
		}

		return taskResults;
	}

	private void validateTasksResults(ArrayList<ActionTaskExecutionResult> taskResults, String taskType) {
		
		if(taskResults.size() == 0) {
			actionExecutionResult.appendLog(String.format("%s execution has no tasks, returning successfully", taskType));
			actionExecutionResult.setSuccessFlag(true);
			return;
		}

		boolean isError = false;

		for (int i = 0; i < taskResults.size(); i++) {
			ActionTaskExecutionResult result = taskResults.get(i);

			actionExecutionResult.appendLog(result.getResultMessage());

			if (result.isErrorFlag()) {
				break;
			}
		}

		if (isError) {

			actionExecutionResult.appendLog(String.format("%s execution resulted in an error", taskType));
			actionExecutionResult.setSuccessFlag(false);

		} else {
			actionExecutionResult.appendLog(String.format("%s execution ended successfully", taskType));
			actionExecutionResult.setSuccessFlag(true);
		}

	}

}
