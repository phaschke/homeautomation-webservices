package com.peterchaschke.homeautomation.action;

public abstract class ActionExecutorObject {
	
	public abstract ActionConditionalExecutionResult executeConditionalAction(ParsedActionConditional parsedActionIf, boolean debug);

	public abstract ActionTaskExecutionResult executeTaskAction(ParsedActionTask parsedActionThen, String taskMode,
			boolean debug);
}
