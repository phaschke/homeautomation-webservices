package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExecutableAction {
	
	private ArrayList<ExecutableActionConditional> conditionals;
	private ArrayList<ExecutableActionTask> thens;
	private ArrayList<ExecutableActionTask> elses;

}
