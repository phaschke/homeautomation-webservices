package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlServiceFactory;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.event.EventAction;
import com.peterchaschke.homeautomation.event.EventRepository;
import com.peterchaschke.homeautomation.notifier.Notifier;

@Component
public class ActionValidator {

	@Autowired
	private ControlRepository controlRepository;
	
	@Autowired
	private ControlServiceFactory serviceFactory;

	@Autowired
	private EventRepository eventRepository;

	public void validateAction(ParsedAction parsedAction) throws Exception {

		ArrayList<ParsedActionConditional> conditionals = parsedAction.getConditionals();
		if(conditionals != null) {
			conditionals.forEach(conditional -> {
				if (conditional.getType().equals(ActionObjectType.CONTROL)) {
					if (!controlExists(conditional.getObjectId())) {
						throw new ResourceNotFoundException("ConditionalObjectId", conditional.getObjectId(),
								"Control not found");
					}
				}
			});
		}

		ArrayList<ParsedActionTask> thens = parsedAction.getThens();
		validateActionTasks(thens, "Then");

		ArrayList<ParsedActionTask> elses = parsedAction.getElses();
		validateActionTasks(elses, "Else");
	}

	private void validateActionTasks(ArrayList<ParsedActionTask> tasks, String step) {
		
		if(tasks != null) {
			tasks.forEach(task -> {
	
				if (task.getObjectType().equals(ActionObjectType.CONTROL)) {
					if (!controlExists(task.getObjectId())) {
						throw new ResourceNotFoundException(String.format("%sObjectId", step), task.getObjectId(),
								"Control not found");
					}
				}
				if (task.getObjectType().equals(ActionObjectType.EVENT)) {
					if (!eventExists(task.getObjectId())) {
						throw new ResourceNotFoundException(String.format("%sObjectId", step), task.getObjectId(),
								"Event not found");
					}
				}
	
			});
		}
	}

	public ExecutableAction validateAndBuildExecutableAction(ActionResource actionResource) throws Exception {

		if (actionResource.getParsedAction() == null) {
			throw new Exception("No provided parsed action");
		}

		ExecutableAction executableAction = new ExecutableAction();

		ParsedAction parsedAction = actionResource.getParsedAction();

		// IFS
		ArrayList<ExecutableActionConditional> executableIfs = new ArrayList<ExecutableActionConditional>();
		
		
		ArrayList<ParsedActionConditional> conditionals = parsedAction.getConditionals();
		
		if(conditionals != null) {
			conditionals.forEach(parsedConditional -> {
				
				//ExecutableActionConditional executableConditional = (ExecutableActionConditional) parsedConditional;
				ExecutableActionConditional executableConditional = new ExecutableActionConditional(parsedConditional);

				if (executableConditional.getType().equals(ActionObjectType.CONTROL)) {
					BaseControlService control = getControlServiceById(parsedConditional.getObjectId());
					
					executableConditional.setExecutableObject(control);
				}

				executableIfs.add(executableConditional);
			});
		}
		

		if (actionResource.getIfExecution().equals(ActionExecution.SERIAL)) {
			Collections.sort(executableIfs);
		}

		executableAction.setConditionals(executableIfs);

		// THENS
		ArrayList<ExecutableActionTask> executableThens = buildExecutableTaskList(parsedAction.getThens());

		if (actionResource.getThenExecution().equals(ActionExecution.SERIAL)) {
			Collections.sort(executableThens);
		}

		executableAction.setThens(executableThens);

		// ELSES
		ArrayList<ExecutableActionTask> executableElses = buildExecutableTaskList(parsedAction.getElses());

		if (actionResource.getElseExecution().equals(ActionExecution.SERIAL)) {
			Collections.sort(executableElses);
		}

		executableAction.setElses(executableElses);

		return executableAction;
	}

	private ArrayList<ExecutableActionTask> buildExecutableTaskList(ArrayList<ParsedActionTask> taskList) {

		ArrayList<ExecutableActionTask> executableTasks = new ArrayList<ExecutableActionTask>();
		
		if(taskList != null) {
			taskList.forEach(parsedTask -> {
	
				ExecutableActionTask executableTask = new ExecutableActionTask(parsedTask);
				
				if (parsedTask.getObjectType().equals(ActionObjectType.CONTROL)) {
					BaseControlService control = getControlServiceById(parsedTask.getObjectId());
					executableTask.setExecutableObject(control);
				}
				if (parsedTask.getObjectType().equals(ActionObjectType.EVENT)) {
					Event event = getEventById(parsedTask.getObjectId()).get();
					executableTask.setExecutableObject(new EventAction(event));
				}
				if (parsedTask.getObjectType().equals(ActionObjectType.NOTIFICATION)) {
					Notifier notifier = new Notifier();
					executableTask.setExecutableObject(notifier);
				}
	
				executableTasks.add(executableTask);
			});
		}

		return executableTasks;
	}

	private boolean controlExists(Long controlId) {
		return controlRepository.existsById(controlId);
	}

	private boolean eventExists(Long eventId) {
		return eventRepository.existsById(eventId);
	}
	
	private BaseControlService getControlServiceById(Long controlId) {
		
		Control control = controlRepository.findById(controlId).get();
		return serviceFactory.createControlService(control);
	}

	private Optional<Event> getEventById(Long eventId) {
		return eventRepository.findById(eventId);
	}
}
