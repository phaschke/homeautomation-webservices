package com.peterchaschke.homeautomation.action;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ActionResourceAssembler extends RepresentationModelAssemblerSupport<ActionResource, ActionResource> {

	public ActionResourceAssembler() {
		super(ActionController.class, ActionResource.class);
	}

	@Override
	public ActionResource toModel(ActionResource model) {

		try {
			model.add(linkTo(methodOn(ActionController.class).one(model.getId())).withSelfRel());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}

	@Override
	public CollectionModel<ActionResource> toCollectionModel(Iterable<? extends ActionResource> models) {

		CollectionModel<ActionResource> actionModels = super.toCollectionModel(models);

		return actionModels;
	}
}