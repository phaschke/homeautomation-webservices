package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.peterchaschke.homeautomation.MalformedDataException;

public class ActionParser {

	public static ParsedAction parseAction(ActionResource actionResource) throws Exception {

		ArrayList<ParsedActionConditional> conditionals = null;
		if(actionResource.getIfs() != null) {
			try {
	
				conditionals = parseConditionals(actionResource.getIfs());
	
			} catch (Exception e) {
	
				throw new Exception("Failed to parse if conditionals: " + e.getMessage());
			}
		}

		ArrayList<ParsedActionTask> thens = null;
		try {
			
			if(actionResource.getThens() == null) {
				throw new Exception("At least one then task is required");
			}

			thens = parseThens(actionResource.getThens());

		} catch (Exception e) {

			throw new Exception("Failed to parse then tasks: " + e.getMessage());
		}

		ArrayList<ParsedActionTask> elses = null;
		if(actionResource.getElses() != null) {
			try {
	
				elses = parseElses(actionResource.getElses());
	
			} catch (Exception e) {
	
				throw new Exception("Failed to parse else tasks: " + e.getMessage());
			}
		}

		ParsedAction parsedAction = new ParsedAction(conditionals, thens, elses);

		return parsedAction;
	}

	private static ArrayList<ParsedActionConditional> parseConditionals(String ifs) throws Exception {

		JSONArray ifJSON = new JSONArray(ifs);
		ArrayList<ParsedActionConditional> actionIfList = new ArrayList<>();

		for (int i = 0; i < ifJSON.length(); i++) {

			JSONObject ifConditionalJSON = ifJSON.getJSONObject(i);

			try {

				Long controlId = Long.parseLong(ifConditionalJSON.get("objectId").toString());
				String conditional = ifConditionalJSON.get("conditional").toString();
				String parameter = ifConditionalJSON.get("parameter").toString();
				String comparator = ifConditionalJSON.get("comparator").toString();
				String value = ifConditionalJSON.get("value").toString();
				
				int order = 0;
				if(ifConditionalJSON.has("order")) {
					order = ifConditionalJSON.getInt("order");
				}

				ParsedActionConditional parsedActionIf = new ParsedActionConditional(controlId,
						ActionObjectType.CONTROL, conditional, parameter, comparator, value, order);

				actionIfList.add(parsedActionIf);

			} catch (JSONException e) {

				throw new RuntimeException("Failed to parse conditional JSON " + e.getMessage());

			} catch (MalformedDataException e) {

				throw new RuntimeException("Failed to parse conditional segment " + e.getMessage());
			}
		}

		return actionIfList;
	}

	private static ArrayList<ParsedActionTask> parseThens(String thens) throws Exception {

		JSONArray thenJSON = new JSONArray(thens);
		ArrayList<ParsedActionTask> thenTasks = new ArrayList<ParsedActionTask>();

		for (int i = 0; i < thenJSON.length(); i++) {

			JSONObject thenTaskJSON = thenJSON.getJSONObject(i);
			try {
				thenTasks.add(parseTask(thenTaskJSON));

			} catch (JSONException e) {

				throw new RuntimeException("Failed to parse then task JSON " + e.getMessage());

			} catch (MalformedDataException e) {

				throw new RuntimeException("Failed to parse then task " + e.getMessage());
			}
		}

		return thenTasks;
	}

	private static ArrayList<ParsedActionTask> parseElses(String elses) throws Exception {

		JSONArray elseJSON = new JSONArray(elses);
		ArrayList<ParsedActionTask> elseTasks = new ArrayList<ParsedActionTask>();

		for (int i = 0; i < elseJSON.length(); i++) {

			JSONObject elseTaskJSON = elseJSON.getJSONObject(i);
			try {

				elseTasks.add(parseTask(elseTaskJSON));

			} catch (JSONException e) {

				throw new RuntimeException("Failed to parse else task JSON " + e.getMessage());

			} catch (MalformedDataException e) {

				throw new RuntimeException("Failed to parse else task " + e.getMessage());
			}
		}

		return elseTasks;

	}

	private static ParsedActionTask parseTask(JSONObject taskJSON) throws JSONException, MalformedDataException {

		

		Long objectId = Long.parseLong(taskJSON.get("objectId").toString());
		ActionObjectType type = getTaskObjectType(taskJSON.get("objectType").toString());
		String action = taskJSON.get("action").toString();
		
		ArrayList<ParsedActionParameter> parsedParameters = null;
		if (taskJSON.has("parameters")) {
			JSONArray parameterListJSON = taskJSON.getJSONArray("parameters");
			parsedParameters = parseTaskParameters(parameterListJSON);
		}
		
		int order = 0;
		if(taskJSON.has("order")) {
			order = taskJSON.getInt("order");
		}
		
		ParsedActionTask parsedTask = new ParsedActionTask(objectId, type, action, parsedParameters, order);

		return parsedTask;
	}

	private static ActionObjectType getTaskObjectType(String type) throws MalformedDataException {

		if (type.equals(ActionObjectType.CONTROL.toString())) {
			return ActionObjectType.CONTROL;

		} else if (type.equals(ActionObjectType.EVENT.toString())) {
			return ActionObjectType.EVENT;

		} else if (type.equals(ActionObjectType.NOTIFICATION.toString())) {
			return ActionObjectType.NOTIFICATION;

		} else {
			throw new MalformedDataException("ActionTaskObjectType", type, "Invalid action task object type");
		}
	}

	private static ArrayList<ParsedActionParameter> parseTaskParameters(JSONArray parameterListJSON) {

		ArrayList<ParsedActionParameter> taskParameters = new ArrayList<ParsedActionParameter>();

		for (int i = 0; i < parameterListJSON.length(); i++) {

			JSONObject parameterJSON = parameterListJSON.getJSONObject(i);

			String parameter = parameterJSON.get("key").toString();
			String value = parameterJSON.get("value").toString();

			// Parameter name is optional
			if (parameterJSON.has("parameterName")) {
				String parameterName = parameterJSON.get("parameterName").toString();
				taskParameters.add(new ParsedActionParameter(parameterName, parameter, value));
			} else {
				taskParameters.add(new ParsedActionParameter(parameter, value));
			}
		}

		return taskParameters;
	}

}
