package com.peterchaschke.homeautomation.action;

public enum ActionObjectType {
	CONTROL, EVENT, NOTIFICATION
}
