package com.peterchaschke.homeautomation.action;

public class ActionTaskExecutionResult {

	private boolean errorFlag;
	private String resultMessage;

	public boolean isErrorFlag() {
		return errorFlag;
	}

	public void setErrorFlag(boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
