package com.peterchaschke.homeautomation.action;

import java.util.HashMap;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

public interface ActionService {
	
	@PreAuthorize("hasAnyRole('ADMIN', 'GUEST')")
	public List<ActionResource> getActions(HashMap<String, String> filters);
	
	@PreAuthorize("permitAll()")
	public ActionResource getAction(Long id);
	
	@PreAuthorize("hasRole('ADMIN')")
	public ActionResource createAction(ActionResource actionResource) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public ActionResource updateAction(Long id, ActionResource newActionResource) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteAction(Long actionId);
	
	@PreAuthorize("permitAll()")
	public void executeActionWrapper(Long actionId) throws Exception;
	
	@PreAuthorize("permitAll()")
	public ActionExecutionResult executeAction(Long actionId, Boolean debug);
	
	@PreAuthorize("permitAll()")
	public List<ActionResource> getActionsByNameLike(String name);
	
}
