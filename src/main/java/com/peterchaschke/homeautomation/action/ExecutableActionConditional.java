package com.peterchaschke.homeautomation.action;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExecutableActionConditional extends ParsedActionConditional implements Comparable<ExecutableActionConditional> {
	
	private ActionExecutorObject executableObject;
	
	public ExecutableActionConditional(ParsedActionConditional parsedActionConditional){
        super(parsedActionConditional);
    }
	
	@Override
	public int compareTo(ExecutableActionConditional otherConditional) {
		
		int compareOrder = otherConditional.getOrder();
		
		return this.order - compareOrder;
	}
}
