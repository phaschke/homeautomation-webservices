package com.peterchaschke.homeautomation.action;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExecutableActionTask extends ParsedActionTask implements Comparable<ExecutableActionTask> {
	
	private ActionExecutorObject executableObject;
	
	public ExecutableActionTask(ParsedActionTask parsedActionTask) {
		super(parsedActionTask);
	}

	@Override
	public int compareTo(ExecutableActionTask otherTask) {
		
		int compareOrder = otherTask.getOrder();
		
		return this.order - compareOrder;
	}

}
