package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ParameterDefinition {

	private ArrayList<String> parameterKeys;
	private boolean parameterRequired;
	
	public ParameterDefinition() {
		this.parameterRequired = false;
	}
	
}
