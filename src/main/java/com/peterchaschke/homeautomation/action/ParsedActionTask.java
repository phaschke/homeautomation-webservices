package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class ParsedActionTask {

	@NotNull
	private Long objectId;

	@NotNull
	private ActionObjectType objectType;

	@NotBlank
	private String action;

	private ArrayList<@Valid ParsedActionParameter> parameters;
	protected int order = 0;

	public ParsedActionTask(Long objectId, ActionObjectType objectType, String action,
			ArrayList<ParsedActionParameter> parameters, int order) {
		this.objectId = objectId;
		this.objectType = objectType;
		this.action = action;
		this.parameters = parameters;
		this.order = order;
	}
	
	public ParsedActionTask(ParsedActionTask parsedActionTask) {
		this.objectId = parsedActionTask.objectId;
		this.objectType = parsedActionTask.objectType;
		this.action = parsedActionTask.action;
		this.parameters = parsedActionTask.parameters;
		this.order = parsedActionTask.order;
	}

}
