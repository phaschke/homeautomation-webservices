package com.peterchaschke.homeautomation.action;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository extends JpaRepository<Action, Long> {
	
	List<Action> findByOrderByTitleAsc();
	List<Action> findByUserCreatedOrderByTitleAsc(int userCreated);
	Action findByNameAndUserCreated(String name, int userCreated);
	Action findByName(String name);
	List<Action> findByNameContaining(String name);
	void deleteByNameContaining(String name);
	List<Action> findByNameContainingAndUserCreated(String name, int userCreated);

}