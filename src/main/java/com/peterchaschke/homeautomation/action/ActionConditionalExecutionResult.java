package com.peterchaschke.homeautomation.action;

public class ActionConditionalExecutionResult {

	private boolean trueFlag;
	private boolean errorFlag;
	private String resultMessage;

	public boolean isTrueFlag() {
		return trueFlag;
	}

	public void setTrueFlag(boolean trueFlag) {
		this.trueFlag = trueFlag;
	}

	public boolean isErrorFlag() {
		return errorFlag;
	}

	public void setErrorFlag(boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

}
