package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActionExecutionResult {
	
	private Long actionId;
	private boolean conditionalEvaluation;
	private boolean successFlag;
	private long timeDuration;
	private ArrayList<String> log;
	
	ActionExecutionResult() {
		this.log = new ArrayList<String>();
	}
	
	public void appendLog(String entry) {
		this.log.add(entry);
	}
}
