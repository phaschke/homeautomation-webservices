package com.peterchaschke.homeautomation.action;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.homeautomation.validators.ObjectNameConstraint;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "action")
@Relation(collectionRelation = "actions")
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
public class ActionResource extends RepresentationModel<ActionResource> {
	
	private Long id;
	
	@ObjectNameConstraint(message = "Action name must be greater than length 1 and only include alphanumeric characters, dashes and underscores.")
	private String name;
	
	@NotBlank
	private String title;
	
	private String description;
	
	@NotNull
	private ActionExecution ifExecution;
	
	@NotNull
	private ActionExecution thenExecution;
	
	@NotNull
	private ActionExecution elseExecution;
	
	private String ifs;
	private String thens;
	private String elses;
	private int userCreated;
	private ParsedAction parsedAction;

	public Action toEntity() {

		Action action = new Action();

		action.setId(this.id);
		action.setName(this.name);
		action.setTitle(this.title);
		action.setDescription(this.description);
		action.setIfExecution(this.ifExecution);
		action.setThenExecution(this.thenExecution);
		action.setElseExecution(this.elseExecution);
		action.setIfs(this.ifs);
		action.setThens(this.thens);
		action.setElses(this.elses);
		action.setUserCreated(this.userCreated);

		return action;
	}
}
