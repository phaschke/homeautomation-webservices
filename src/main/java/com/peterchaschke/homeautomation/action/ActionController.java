package com.peterchaschke.homeautomation.action;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Actions")
@Validated
@RestController
@RequestMapping("/api/actions")
public class ActionController {

	private final ActionService actionService;
	private final ActionResourceAssembler actionResourceAssembler;
	
	public ActionController(ActionService actionService, ActionResourceAssembler actionResourceAssembler) {
		this.actionService = actionService;
		this.actionResourceAssembler = actionResourceAssembler;
	}

	@GetMapping("")
	public ResponseEntity<?> all(@RequestParam Optional<String> title, @RequestParam Optional<String> name,
			@RequestParam Optional<String> user_created) {

		HashMap<String, String> filters = new HashMap<String, String>();
		if (title.isPresent())
			filters.put("title", title.get().toUpperCase());
		if (name.isPresent())
			filters.put("name", name.get().toUpperCase());
		if (user_created.isPresent())
			filters.put("userCreated", user_created.get());

		List<ActionResource> actions = actionService.getActions(filters);

		return new ResponseEntity<>(actionResourceAssembler.toCollectionModel(actions), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> one(@PathVariable Long id) throws Exception {

		ActionResource model = actionResourceAssembler.toModel(actionService.getAction(id));
		return new ResponseEntity<ActionResource>(model, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<ActionResource> newAction(@RequestBody ActionResource action) throws Exception {

		ActionResource model = actionResourceAssembler.toModel(actionService.createAction(action));

		return new ResponseEntity<ActionResource>(model, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ActionResource> updateAction(@RequestBody ActionResource updatedAction, @PathVariable Long id)
			throws Exception {

		ActionResource model = actionResourceAssembler.toModel(actionService.updateAction(id, updatedAction));
		return new ResponseEntity<ActionResource>(model, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteAction(@PathVariable Long id) {

		actionService.deleteAction(id);
		return ResponseEntity.noContent().build();
	}

	@PatchMapping("/execute/{id}")
	public ResponseEntity<?> executeAction(@PathVariable Long id) throws Exception {

		actionService.executeActionWrapper(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PatchMapping("/test/{id}")
	public ResponseEntity<?> testAction(@PathVariable Long id) throws Exception {

		ActionExecutionResult actionResult = actionService.executeAction(id, true);
		return new ResponseEntity<ActionExecutionResult>(actionResult, HttpStatus.OK);
	}

}
