package com.peterchaschke.homeautomation.action;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "actions")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class Action {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@NotBlank
	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;
	
	@Column(name = "if_execution")
	@Enumerated(EnumType.STRING)
	private ActionExecution ifExecution;
	
	@Column(name = "then_execution")
	@Enumerated(EnumType.STRING)
	private ActionExecution thenExecution;
	
	@Column(name = "else_execution")
	@Enumerated(EnumType.STRING)
	private ActionExecution elseExecution;
	
	@Column(name = "ifs")
	private String ifs;
	
	@Column(name = "thens")
	private String thens;
	
	@Column(name = "elses")
	private String elses;

	@Column(name = "user_created")
	private int userCreated;
	
	
	public ActionResource toResource() {
		
		ActionResource actionResource = new ActionResource();
		
		actionResource.setId(this.getId());
		actionResource.setName(this.getName());
		actionResource.setTitle(this.getTitle());
		actionResource.setDescription(this.getDescription());
		actionResource.setIfExecution(this.getIfExecution());
		actionResource.setThenExecution(this.getThenExecution());
		actionResource.setElseExecution(this.getElseExecution());
		actionResource.setIfs(this.ifs);
		actionResource.setThens(this.thens);
		actionResource.setElses(this.elses);
		actionResource.setUserCreated(this.getUserCreated());

		return actionResource;
	}

}
