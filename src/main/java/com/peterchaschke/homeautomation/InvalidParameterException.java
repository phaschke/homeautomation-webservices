package com.peterchaschke.homeautomation;

public class InvalidParameterException extends RuntimeException {

	private static final long serialVersionUID = 1657790418368594384L;

	public InvalidParameterException() {
	}

	public InvalidParameterException(String message) {
		super(message);
	}

	public InvalidParameterException(Throwable cause) {
		super(cause);
	}

	public InvalidParameterException(String message, Throwable cause) {
		super(message, cause);
	}
}
