package com.peterchaschke.homeautomation;

//TODO: Remove this exception after refactoring to use Resource not found
public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7329609422480662638L;

	public EntityNotFoundException(Long id) {
		super("EntityModel not found with parameters {" + id + "}");
	}

	public EntityNotFoundException(String deviceName) {
		super("EntityModel not found with parameters {" + deviceName + "}");
	}

}