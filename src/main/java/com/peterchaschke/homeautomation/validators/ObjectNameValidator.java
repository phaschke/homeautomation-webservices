package com.peterchaschke.homeautomation.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ObjectNameValidator implements ConstraintValidator<ObjectNameConstraint, String>{
	
	@Override
    public void initialize(ObjectNameConstraint objectName) {
    }

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		return value != null && value.matches("[a-zA-Z0-9\\-\\_]*") && (value.length() > 0);
	}

}
