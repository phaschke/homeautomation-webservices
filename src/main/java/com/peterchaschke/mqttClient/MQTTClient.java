package com.peterchaschke.mqttClient;

import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MQTTClient implements MqttCallbackExtended {

	private String broker;
	private String topic;
	
	MqttClient client;
	String publisherId = UUID.randomUUID().toString();
	IMqttClient publisher;
	
	final int MQTT_TIMEOUT_IN_SECONDS = 4;

	int qos = 2;

	static Random random = new Random();
	private String msgId;
	private volatile String returnMessage;

	public MQTTClient(String broker, String topic) {
		this.broker = broker;
		this.topic = topic;
	}
	
	/*private String generateMessageId() {
		
		int n = 10000 + random.nextInt(90000);
		return Integer.toString(n);
	}*/

	public String sendMessage(JSONObject requestJSON) {
		
		String clientId = UUID.randomUUID().toString();

		try {
			client = new MqttClient(broker, clientId);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			connOpts.setKeepAliveInterval(30);

			client.connect(connOpts);
			client.setCallback(this);
			int n = 10000 + random.nextInt(90000);
			msgId = Integer.toString(n);

			client.subscribe(topic, 1);
			
			requestJSON.put("msgId", msgId);

			//System.out.println("Publishing message: " + requestJSON.toString());
			MqttMessage message = new MqttMessage(requestJSON.toString().getBytes());
			message.setQos(qos);

			client.publish(topic, message);
			//System.out.println("Message published to topic: "+topic);

		} catch (IllegalArgumentException e) {

			throw new RuntimeException("Given broker URI is invalid.");

		} catch (MqttException e) {

			e.printStackTrace();
		}

		long timeoutExpiredMs = System.currentTimeMillis() + (MQTT_TIMEOUT_IN_SECONDS*1000);

		while (returnMessage == null) {

			long waitMillis = timeoutExpiredMs - System.currentTimeMillis();
			if (waitMillis <= 0) { // timeout expired
				// System.out.println("timed out");

				try {
					client.disconnect();
					client.close();
				} catch (MqttException e) {

					e.printStackTrace();
				}

				throw new RuntimeException("Timed out while attempting connection with device.");
			}
			if (returnMessage != null) {
				// return returnMessage;
				break;
			}
			// we assume we are in a synchronized (object) here
			// this.wait(waitMillis);
			// we might be improperly awoken here so we loop around to see if the
			// condition is still true or if we timed out
		}

		try {
			client.disconnect();
			client.close();

		} catch (MqttException e) {
			e.printStackTrace();
		}

		return returnMessage;

	}

	public void connectionLost(Throwable cause) {
		//System.out.println("Connection lost: " + cause.getMessage());

	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// Do something on delivery complete...
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {

		//System.out.println("Recieved Response: [" + topic + "] " + message);

		JSONObject responseJSON = new JSONObject(message.toString());
		
		// Check for resId
		if(responseJSON.has("resId")) {
			
			returnMessage = processMessage(responseJSON);
		}
			
		return;

	}

	public void connectComplete(boolean reconnect, String serverURI) {
		// Do something on connection complete...
	}

	public String processMessage(JSONObject responseJSON) throws MQTTResponseException {
		
		responseJSON.remove("resId");
		
		if(responseJSON.has("status")) {
			
			JSONObject parsedResponseJSON = new JSONObject();
			
			// TODO: Phase this out after updating existing devices
			// Old structure with array: IE: {"status":[{"timer":0},{"relay":0}]}
			// Convert old structure to new structure
			try {
				JSONArray statusArray = responseJSON.getJSONArray("status");
				
				for(int i = 0; i < statusArray.length(); i++) {
					JSONObject obj = statusArray.getJSONObject(i);
					Iterator<String> keys = obj.keys();
					String key = keys.next();
					parsedResponseJSON.put(key, obj.get(key));
				}
				responseJSON.put("status", parsedResponseJSON);
				
			} catch(JSONException je) {
				
				// New structure with array: IE: {"status": {"timer":0,"relay":0}}
				parsedResponseJSON = responseJSON.getJSONObject("status");
				responseJSON.put("status", parsedResponseJSON);
			}
		}
		
		//System.out.println(responseJSON.toString());
		
		return responseJSON.toString();
		
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
