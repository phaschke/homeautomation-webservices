package com.peterchaschke.mqttClient;

public class MQTTResponseException extends Exception {

	private static final long serialVersionUID = 635977577337255911L;

	public MQTTResponseException() {
	}

	public MQTTResponseException(String message) {
		super(message);
	}

	public MQTTResponseException(Throwable cause) {
		super(cause);
	}

	public MQTTResponseException(String message, Throwable cause) {
		super(message, cause);
	}

}