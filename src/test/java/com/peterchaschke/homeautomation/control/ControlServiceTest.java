package com.peterchaschke.homeautomation.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.InvalidPermissionsException;
import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.controls.BaseControlBuilderService;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlAction;
import com.peterchaschke.homeautomation.controls.ControlBuilderServiceFactory;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlServiceFactory;
import com.peterchaschke.homeautomation.controls.ControlServiceImpl;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.topic.Topic;
import com.peterchaschke.homeautomation.topic.TopicRepository;
import com.peterchaschke.homeautomation.topic.TopicService;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ControlServiceTest {

	@MockBean
	private TopicRepository topicRepository;

	@MockBean
	private TopicService topicService;

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ControlServiceFactory serviceFactory;

	@MockBean
	private ControlBuilderServiceFactory controlBuilderServiceFactory;

	@InjectMocks
	private ControlServiceImpl controlService;

	Control CONTROL_1 = Control.builder().id(1l).topicId(1l).uniqueName("control1").active(1).displayOrder(1)
			.title("Control 1").type(ControlTypes.SWITCH).broker("192.168.0.1").subtopic("test_switch").persistState(1)
			.description("Test description").build();

	Control CONTROL_2 = Control.builder().id(2l).topicId(1l).uniqueName("control2").active(1).displayOrder(2)
			.title("Control 2").type(ControlTypes.SENSOR).broker("192.168.0.1").subtopic("test_sensor").persistState(1)
			.description("Test description 2").build();

	Control CONTROL_3 = Control.builder().id(3l).topicId(2l).uniqueName("control3").active(1).displayOrder(1)
			.title("Control 2").type(ControlTypes.LED_CONTROLLER).broker("192.168.0.1").subtopic("test_led_controller")
			.persistState(1).description("Test description 3").build();

	Topic TOPIC_1 = new Topic(1l, "topic1", "Topic 1", 1);
	Topic TOPIC_2 = new Topic(2l, "topic2", "Topic 2", 1);

	@Test
	public void testGetAllControls() {

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2, CONTROL_3));

		Mockito.when(controlRepository.findAll()).thenReturn(controls);

		List<Control> expectedControls = controlService.getAllControls();

		assertThat(expectedControls).isNotNull();
		assertEquals(controls, expectedControls);
	}

	@Test
	public void testGetControlsByTopicId_noTopicFound() {

		Optional<Boolean> emptyOptionalBoolean = Optional.empty();
		Optional<Topic> emptyOptionalTopic = Optional.empty();

		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(emptyOptionalTopic);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.getControlsByTopicId(1l, emptyOptionalBoolean, emptyOptionalBoolean, null);
		});
	}

	@Test
	@WithMockUser(username = "MyHomeGuest", authorities = { "ROLE_GUEST" })
	@WithUserDetails("MyHomeGuest")
	public void testGetControlsByTopicId_invalidPermissionsForTopic() {

		Optional<Boolean> emptyOptionalBoolean = Optional.empty();
		Optional<Topic> optionalTopic = Optional.of(TOPIC_2);

		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		List<Topic> allowedTopics = new ArrayList<>(Arrays.asList(TOPIC_1));

		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(allowedTopics);

		assertThrows(InvalidPermissionsException.class, () -> {
			controlService.getControlsByTopicId(1l, emptyOptionalBoolean, emptyOptionalBoolean, null);
		});
	}

	/*@Test
	public void testGetControlsByTopicId_active() {

		Optional<Boolean> optionalBoolean = Optional.of(true);
		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);

		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		List<Topic> allowedTopics = new ArrayList<>(Arrays.asList(TOPIC_1));

		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(allowedTopics);

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2, CONTROL_3));
		Mockito.when(
				controlRepository.findAllByTopicIdAndActiveOrderByDisplayOrderAsc(Mockito.anyLong(), Mockito.anyInt(), Mockito.any()))
				.thenReturn(controls);

		List<Control> expectedControls = controlService.getControlsByTopicId(1l, optionalBoolean, optionalBoolean, null);

		assertThat(expectedControls).isNotNull();
		assertEquals(controls, expectedControls);
	}*/

	/*@Test
	public void testGetControlsByTopicId_notActive() {

		Optional<Boolean> optionalBoolean = Optional.of(false);

		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		List<Topic> allowedTopics = new ArrayList<>(Arrays.asList(TOPIC_1));
		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(allowedTopics);

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2, CONTROL_3));
		Mockito.when(controlRepository.findAllByTopicIdOrderByDisplayOrderAsc(Mockito.anyLong())).thenReturn(controls);

		List<Control> expectedControls = controlService.getControlsByTopicId(1l, optionalBoolean, optionalBoolean, null);

		assertThat(expectedControls).isNotNull();
		assertEquals(controls, expectedControls);
	}*/

	@Test
	public void testGetOneControl() {

		Control control = CONTROL_1;
		control.setTopic(TOPIC_1);
		Optional<Control> optionalControl = Optional.of(control);
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		List<Topic> allowedTopics = new ArrayList<>(Arrays.asList(TOPIC_1));
		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(allowedTopics);

		Control expectedControl = controlService.getOneControl(Mockito.anyLong());

		assertThat(expectedControl).isNotNull();
		assertEquals(CONTROL_1, expectedControl);
	}

	@Test
	public void testGetOneControl_noControlFound() {

		Optional<Control> emptyOptionalControl = Optional.empty();
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(emptyOptionalControl);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.getOneControl(Mockito.anyLong());
		});
	}

	@Test
	@WithMockUser(username = "MyHomeGuest", authorities = { "ROLE_GUEST" })
	@WithUserDetails("MyHomeGuest")
	public void testGetOneControl_invalidPermissionsForTopic() {

		Control control = CONTROL_1;
		control.setTopic(TOPIC_1);
		Optional<Control> optionalControl = Optional.of(control);
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		List<Topic> allowedTopics = new ArrayList<>(Arrays.asList(TOPIC_2));
		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(allowedTopics);

		assertThrows(InvalidPermissionsException.class, () -> {
			controlService.getOneControl(Mockito.anyLong());
		});
	}

	@Test
	public void testCreateControl() {

		ControlResource controlDTOToAdd = CONTROL_1.toResource();

		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		Mockito.when(controlRepository.findByUniqueName(Mockito.anyString())).thenReturn(null);

		BaseControlBuilderService builderServiceMock = mock(BaseControlBuilderService.class);
		Mockito.when(controlBuilderServiceFactory.createControlBuilderService(Mockito.any(ControlResource.class)))
				.thenReturn(builderServiceMock);

		Mockito.when(builderServiceMock.createControl()).thenReturn(CONTROL_1);

		Control addedControl = controlService.createControl(controlDTOToAdd);

		assertThat(addedControl).isNotNull();
		assertEquals(CONTROL_1, addedControl);
	}

	@Test
	public void testCreateControl_topicNotFound() {

		Optional<Topic> emptyOptionalTopic = Optional.empty();

		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(emptyOptionalTopic);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.createControl(CONTROL_1.toResource());
		});
	}

	@Test
	public void testCreateControl_uniqueConstraintViolation() {

		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		Mockito.when(controlRepository.findByUniqueName(Mockito.anyString())).thenReturn(CONTROL_1);

		assertThrows(UniqueConstraintViolationException.class, () -> {
			controlService.createControl(CONTROL_1.toResource());
		});
	}

	@Test
	public void testUpdateControl() {

		ControlResource controlDTOToUpdate = CONTROL_1.toResource();
		controlDTOToUpdate.setTitle("Updated Title");

		Optional<Control> optionalControl = Optional.of(CONTROL_1);
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		Mockito.when(controlRepository.findByUniqueName(Mockito.anyString())).thenReturn(CONTROL_1);

		BaseControlBuilderService builderServiceMock = mock(BaseControlBuilderService.class);
		Mockito.when(controlBuilderServiceFactory.createControlBuilderService(Mockito.any(ControlResource.class)))
				.thenReturn(builderServiceMock);

		Mockito.when(builderServiceMock.editControl(Mockito.any(ControlResource.class)))
				.thenReturn(controlDTOToUpdate.toEntity());

		Control addedControl = controlService.updateControl(controlDTOToUpdate);

		assertThat(addedControl).isNotNull();
		assertEquals("Updated Title", addedControl.getTitle());
	}

	@Test
	public void testUpdateControl_controlNotFound() {

		ControlResource controlDTOToUpdate = CONTROL_1.toResource();
		controlDTOToUpdate.setTitle("Updated Title");

		Optional<Control> optionalEmptyControl = Optional.empty();
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyControl);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.updateControl(controlDTOToUpdate);
		});
	}

	@Test
	public void testUpdateControl_topicNotFound() {

		ControlResource controlDTOToUpdate = CONTROL_1.toResource();
		controlDTOToUpdate.setTitle("Updated Title");

		Optional<Control> optionalControl = Optional.of(CONTROL_1);
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		Optional<Topic> optionalEmptyTopic = Optional.empty();
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyTopic);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.updateControl(controlDTOToUpdate);
		});
	}

	@Test
	public void testUpdateControl_uniqueConstraintViolation() {

		ControlResource controlDTOToUpdate = CONTROL_1.toResource();
		controlDTOToUpdate.setTitle("Updated Title");

		Optional<Control> optionalControl = Optional.of(controlDTOToUpdate.toEntity());
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);

		Control controlWithSameNameDifferentId = CONTROL_1;
		controlWithSameNameDifferentId.setId(7l);

		Mockito.when(controlRepository.findByUniqueName(Mockito.anyString()))
				.thenReturn(controlWithSameNameDifferentId);

		assertThrows(UniqueConstraintViolationException.class, () -> {
			controlService.updateControl(controlDTOToUpdate);
		});
	}

	@Test
	public void testDeleteControl_controlNotFound() {

		Optional<Control> optionalEmptyControl = Optional.empty();
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyControl);

		controlService.deleteControl(Mockito.anyLong());
	}

	@Test
	public void testDeleteControl() {

		Optional<Control> optionalControl = Optional.of(CONTROL_1);
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		BaseControlBuilderService builderServiceMock = mock(BaseControlBuilderService.class);
		Mockito.when(controlBuilderServiceFactory.createControlBuilderService(Mockito.any(ControlResource.class)))
				.thenReturn(builderServiceMock);

		controlService.deleteControl(Mockito.anyLong());
	}

	@Test
	public void testDoControlAction() throws Exception {

		Optional<Control> optionalControl = Optional.of(CONTROL_1);

		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		BaseControlService serviceMock = mock(BaseControlService.class);
		Mockito.when(serviceFactory.createControlService(Mockito.any(Control.class))).thenReturn(serviceMock);

		Mockito.when(serviceMock.doAction(Mockito.any(ControlAction.class))).thenReturn(CONTROL_1.toResource());

		ControlAction controlAction = new ControlAction();

		ControlResource returnedControlResource = controlService.doAction(1l, controlAction);

		assertNotNull(returnedControlResource);
	}

	@Test
	public void testGetStatus() throws Exception {

		Optional<Control> optionalControl = Optional.of(CONTROL_1);

		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		BaseControlService serviceMock = mock(BaseControlService.class);
		Mockito.when(serviceFactory.createControlService(Mockito.any(Control.class))).thenReturn(serviceMock);

		Mockito.when(serviceMock.getDisplayStatus()).thenReturn(CONTROL_1.toResource());

		ControlResource returnedControlResource = controlService.getStatus(1l);

		assertNotNull(returnedControlResource);
	}

	@Test
	public void testGetStatus_controlNotFound() {

		Optional<Control> optionalEmptyControl = Optional.empty();
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyControl);

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.updateControl(controlService.getStatus(1l));
		});
	}

	@Test
	public void testUpdateControlState() throws Exception {

		Optional<Control> optionalControl = Optional.of(CONTROL_1);

		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalControl);

		BaseControlService serviceMock = mock(BaseControlService.class);
		Mockito.when(serviceFactory.createControlService(Mockito.any(Control.class))).thenReturn(serviceMock);

		Mockito.when(serviceMock.updateState(Mockito.any(JSONObject.class))).thenReturn(CONTROL_1.toResource());

		String state = "{}";

		ControlResource returnedControlResource = controlService.updateControlState(1l, state);

		assertNotNull(returnedControlResource);
	}

	@Test
	public void testUpdateControlState_controlNotFound() {

		Optional<Control> optionalEmptyControl = Optional.empty();
		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyControl);

		String state = "{}";

		assertThrows(ResourceNotFoundException.class, () -> {
			controlService.updateControl(controlService.updateControlState(1l, state));
		});
	}

}
