package com.peterchaschke.homeautomation.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.controls.relayswitch.SwitchBuilderService;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.event.EventRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class SwitchBuilderServiceTest {

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ActionRepository actionRepository;

	@MockBean
	private EventRepository eventRepository;

	@Test
	public void testCreateControl_notPersisted() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_switch");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Switch");
		control.setType(ControlTypes.SWITCH);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setPersistState(0);
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		SwitchBuilderService builderService = new SwitchBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(1), savedControl.getTopicId());
		assertEquals("new_switch", savedControl.getUniqueName());
		assertEquals(1, savedControl.getActive());
		assertEquals(0, savedControl.getDisplayOrder());
		assertEquals("New Switch", savedControl.getTitle());
		assertEquals(ControlTypes.SWITCH, savedControl.getType());
		assertEquals("tcp//:192.168.0.1:1883", savedControl.getBroker());
		assertEquals("test", savedControl.getSubtopic());
		assertEquals("Test description", savedControl.getDescription());
		assertEquals(0, savedControl.getPersistState());

		String expectedState = "{\"timer\":0,\"relay\":0}";

		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertTrue(expectedStateJSON.similar(actualStateJSON));

		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Relay\",\"status\":\"relay\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Timer\",\"status\":\"timer\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"state.timer\",\"state.relay\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"relay\",\"timer\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		assertNull(savedControl.getEventProperties());

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testCreateControl_persisted() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_switch");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Switch");
		control.setType(ControlTypes.SWITCH);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setPersistState(1);
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		SwitchBuilderService builderService = new SwitchBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		String expectedEventProperties = "[{\"custom\":false,\"name\":\"PRESS_ON\"},{\"custom\":false,\"name\":\"PRESS_OFF\"},{\"custom\":false,\"name\":\"TIMER_END\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Relay\",\"status\":\"relay\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Timer\",\"status\":\"timer\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"state.timer\",\"state.relay\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"relay\",\"timer\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testCreateControl_persistedWithCustomProperties() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_switch");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Switch");
		control.setType(ControlTypes.SWITCH);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setPersistState(1);
		control.setProperties(null);
		control.setEventProperties("[{\"custom\":true,\"name\":\"\"}]");
		control.setStatusProperties(
				"[{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Test\",\"status\":\"test\"}]");
		control.setActionProperties(null);
		// Since the state exists, expect the existing state to not be overwritten
		control.setState("{\"timer\":60,\"relay\":1}");

		SwitchBuilderService builderService = new SwitchBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		// Since the state exists, do not overwrite it
		String expectedState = "{\"timer\":0,\"relay\":0}";
		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertFalse(expectedStateJSON.similar(actualStateJSON));

		String expectedEventProperties = "[{\"custom\":true,\"name\":\"\"},{\"custom\":false,\"name\":\"PRESS_ON\"},{\"custom\":false,\"name\":\"PRESS_OFF\"},{\"custom\":false,\"name\":\"TIMER_END\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Test\",\"status\":\"test\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Relay\",\"status\":\"relay\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Timer\",\"status\":\"timer\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"state.timer\",\"state.relay\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"test\",\"ping\",\"relay\",\"timer\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testUpdateControl_notPersisted() {

		ControlResource control = new ControlResource();
		control.setId(1l);
		control.setTopicId(1l);
		control.setUniqueName("new_switch");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Switch");
		control.setType(ControlTypes.SWITCH);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setPersistState(0);
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		ControlResource updatedControl = new ControlResource();
		updatedControl.setId(1l);
		updatedControl.setTopicId(1l);
		updatedControl.setUniqueName("new_switch");
		updatedControl.setActive(1);
		updatedControl.setDisplayOrder(0);
		updatedControl.setTitle("New Switch");
		updatedControl.setType(ControlTypes.SWITCH);
		updatedControl.setBroker("tcp//:192.168.0.1:1883");
		updatedControl.setSubtopic("test");
		updatedControl.setDescription("Updated control");
		updatedControl.setPersistState(0);
		updatedControl.setProperties(null);
		updatedControl.setEventProperties(null);
		updatedControl.setStatusProperties(null);
		updatedControl.setActionProperties(null);

		SwitchBuilderService builderService = new SwitchBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.editControl(updatedControl);
		assertNotNull(savedControl);

		String expectedState = "{\"timer\":0,\"relay\":0}";

		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertTrue(expectedStateJSON.similar(actualStateJSON));

		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Relay\",\"status\":\"relay\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Timer\",\"status\":\"timer\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"state.timer\",\"state.relay\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"relay\",\"timer\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		assertNull(savedControl.getEventProperties());

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testUpdateControl_persistedWithCustomProperties() {

		ControlResource control = new ControlResource();
		control.setId(1l);
		control.setTopicId(1l);
		control.setUniqueName("new_switch");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Switch");
		control.setType(ControlTypes.SWITCH);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setPersistState(0);
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		ControlResource updatedControl = new ControlResource();
		updatedControl.setId(1l); // For testing purposes only
		updatedControl.setTopicId(2l);
		updatedControl.setUniqueName("new_switch2");
		updatedControl.setActive(0);
		updatedControl.setDisplayOrder(1);
		updatedControl.setTitle("New Switch 2");
		updatedControl.setType(ControlTypes.SWITCH);
		updatedControl.setBroker("tcp//:192.168.0.2:1883");
		updatedControl.setSubtopic("new_test");
		updatedControl.setDescription("Updated test description");
		updatedControl.setPersistState(1);
		updatedControl.setProperties(null);
		updatedControl.setEventProperties("[{\"custom\":true,\"name\":\"\"}]");
		updatedControl.setStatusProperties(
				"[{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Test\",\"status\":\"test\"}]");
		updatedControl.setActionProperties(null);

		SwitchBuilderService builderService = new SwitchBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.editControl(updatedControl);
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(2), savedControl.getTopicId());
		assertEquals("new_switch2", savedControl.getUniqueName());
		assertEquals(0, savedControl.getActive());
		assertEquals(1, savedControl.getDisplayOrder());
		assertEquals("New Switch 2", savedControl.getTitle());
		assertEquals(ControlTypes.SWITCH, savedControl.getType());
		assertEquals("tcp//:192.168.0.2:1883", savedControl.getBroker());
		assertEquals("new_test", savedControl.getSubtopic());
		assertEquals("Updated test description", savedControl.getDescription());

		String expectedEventProperties = "[{\"custom\":true,\"name\":\"\"},{\"custom\":false,\"name\":\"PRESS_ON\"},{\"custom\":false,\"name\":\"PRESS_OFF\"},{\"custom\":false,\"name\":\"TIMER_END\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Test\",\"status\":\"test\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Relay\",\"status\":\"relay\"},{\"formatter\":\"\",\"editable\":false,\"display\":true,\"title\":\"Timer\",\"status\":\"timer\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"state.timer\",\"state.relay\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"test\",\"ping\",\"relay\",\"timer\"],\"parameterRequired\":true}}}";
		
		assertNull(builderService.getControl().getProperties());

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

}
