package com.peterchaschke.homeautomation.control;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.controls.BaseControlBuilderService;
import com.peterchaschke.homeautomation.controls.ControlBuilderServiceFactory;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.controls.H801.H801BuilderService;
import com.peterchaschke.homeautomation.controls.relayswitch.SwitchBuilderService;
import com.peterchaschke.homeautomation.controls.sensor.SensorBuilderService;
import com.peterchaschke.homeautomation.event.EventRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ControlBuilderServiceFactoryTest {

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ActionRepository actionRepository;

	@MockBean
	private EventRepository eventRepository;

	@InjectMocks
	private ControlBuilderServiceFactory controlBuilderService;

	@Test
	public void testSwitchCreate() {

		ControlResource control = new ControlResource();
		control.setType(ControlTypes.SWITCH);

		BaseControlBuilderService builderService = controlBuilderService.createControlBuilderService(control);

		assertThat(builderService, instanceOf(SwitchBuilderService.class));
	}

	@Test
	public void testSensorCreate() {

		ControlResource control = new ControlResource();
		control.setType(ControlTypes.SENSOR);

		BaseControlBuilderService builderService = controlBuilderService.createControlBuilderService(control);

		assertThat(builderService, instanceOf(SensorBuilderService.class));
	}

	@Test
	public void testLEDControllerCreate() {

		ControlResource control = new ControlResource();
		control.setType(ControlTypes.LED_CONTROLLER);

		BaseControlBuilderService builderService = controlBuilderService.createControlBuilderService(control);

		assertThat(builderService, instanceOf(H801BuilderService.class));
	}

}
