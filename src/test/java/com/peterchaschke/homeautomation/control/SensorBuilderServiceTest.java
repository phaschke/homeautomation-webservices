package com.peterchaschke.homeautomation.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.controls.sensor.SensorBuilderService;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.event.EventRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class SensorBuilderServiceTest {

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ActionRepository actionRepository;

	@MockBean
	private EventRepository eventRepository;

	@Test
	public void testCreateControl() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_sensor");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Sensor");
		control.setType(ControlTypes.SENSOR);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		SensorBuilderService builderService = new SensorBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(1), savedControl.getTopicId());
		assertEquals("new_sensor", savedControl.getUniqueName());
		assertEquals(1, savedControl.getActive());
		assertEquals(0, savedControl.getDisplayOrder());
		assertEquals("New Sensor", savedControl.getTitle());
		assertEquals(ControlTypes.SENSOR, savedControl.getType());
		assertEquals("tcp//:192.168.0.1:1883", savedControl.getBroker());
		assertEquals("test", savedControl.getSubtopic());
		assertEquals("Test description", savedControl.getDescription());

		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true}},\"conditional\":{\"GET_STATUS\":{\"parameterKeys\":[\"ping\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		assertNull(savedControl.getEventProperties());

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testCreateControl_customStatusAndEvents() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_sensor");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Sensor");
		control.setType(ControlTypes.SENSOR);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(null);
		control.setEventProperties("[{\"custom\":true,\"name\":\"MOTION\"}]");
		control.setStatusProperties(
				"[{\"title\":\"Temperature\",\"status\":\"temp\",\"display\":true,\"editable\":true,\"formatter\":\"{{}}&#176;F\"},{\"title\":\"Humidity\",\"status\":\"humid\",\"display\":true,\"editable\":true,\"formatter\":\"{{}}%\"},{\"title\":\"Lux\",\"status\":\"lux\",\"display\":true,\"editable\":true,\"formatter\":\"\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]");
		control.setActionProperties(null);

		SensorBuilderService builderService = new SensorBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		String expectedEventProperties = "[{\"custom\":true,\"name\":\"MOTION\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"{{}}&#176;F\",\"editable\":true,\"display\":true,\"title\":\"Temperature\",\"status\":\"temp\"},{\"formatter\":\"{{}}%\",\"editable\":true,\"display\":true,\"title\":\"Humidity\",\"status\":\"humid\"},{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Lux\",\"status\":\"lux\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true}},\"conditional\":{\"GET_STATUS\":{\"parameterKeys\":[\"temp\",\"humid\",\"lux\",\"ping\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testUpdateControl() {

		ControlResource control = new ControlResource();
		control.setId(1l);
		control.setTopicId(1l);
		control.setUniqueName("new_sensor");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Sensor");
		control.setType(ControlTypes.SENSOR);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		ControlResource updatedControl = new ControlResource();
		updatedControl.setId(1l);
		updatedControl.setTopicId(2l);
		updatedControl.setUniqueName("new_sensor1");
		updatedControl.setActive(0);
		updatedControl.setDisplayOrder(5);
		updatedControl.setTitle("New Sensor 1");
		updatedControl.setType(ControlTypes.SENSOR);
		updatedControl.setBroker("tcp//:192.168.0.2:1883");
		updatedControl.setSubtopic("new_test");
		updatedControl.setDescription("Updated test description");
		updatedControl.setProperties(null);
		updatedControl.setEventProperties(null);
		updatedControl.setStatusProperties(null);
		updatedControl.setActionProperties(null);

		SensorBuilderService builderService = new SensorBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.editControl(updatedControl);
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(2), savedControl.getTopicId());
		assertEquals("new_sensor1", savedControl.getUniqueName());
		assertEquals(0, savedControl.getActive());
		assertEquals(5, savedControl.getDisplayOrder());
		assertEquals("New Sensor 1", savedControl.getTitle());
		assertEquals(ControlTypes.SENSOR, savedControl.getType());
		assertEquals("tcp//:192.168.0.2:1883", savedControl.getBroker());
		assertEquals("new_test", savedControl.getSubtopic());
		assertEquals("Updated test description", savedControl.getDescription());

		assertEquals(null, builderService.getControl().getProperties());
		assertNull(savedControl.getEventProperties());

		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true}},\"conditional\":{\"GET_STATUS\":{\"parameterKeys\":[\"ping\"],\"parameterRequired\":true}}}";
		
		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testUpdateControl_customStatusAndEvents() {

		ControlResource control = new ControlResource();
		control.setId(1l);
		control.setTopicId(1l);
		control.setUniqueName("new_sensor");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New Sensor");
		control.setType(ControlTypes.SENSOR);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		ControlResource updatedControl = new ControlResource();
		updatedControl.setId(1l);
		updatedControl.setTopicId(1l);
		updatedControl.setUniqueName("new_sensor");
		updatedControl.setActive(1);
		updatedControl.setDisplayOrder(0);
		updatedControl.setTitle("New Sensor");
		updatedControl.setType(ControlTypes.SENSOR);
		updatedControl.setBroker("tcp//:192.168.0.1:1883");
		updatedControl.setSubtopic("test");
		updatedControl.setDescription("Test description");
		updatedControl.setProperties(null);
		updatedControl.setEventProperties("[{\"custom\":true,\"name\":\"MOTION\"}]");
		updatedControl.setStatusProperties(
				"[{\"title\":\"Temperature\",\"status\":\"temp\",\"display\":true,\"editable\":true,\"formatter\":\"{{}}&#176;F\"},{\"title\":\"Humidity\",\"status\":\"humid\",\"display\":true,\"editable\":true,\"formatter\":\"{{}}%\"},{\"title\":\"Lux\",\"status\":\"lux\",\"display\":true,\"editable\":true,\"formatter\":\"\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]");
		updatedControl.setActionProperties(
				"{\"then\":{\"SET_VALUE\":{\"actionThen\":\"SET_VALUE\",\"noParameters\":false,\"requiredParameters\":[{\"keys\":[\"state\",\"active\"],\"parameterName\":\"field\",\"value\":\"\",\"required\":\"REQUIRED\",\"key\":\"\"}],\"optionalParameters\":[]}},\"if\":{\"GET_VALUE\":{\"parameter\":\"required\",\"parameters\":[\"state\",\"active\"]},\"GET_STATUS\":{\"parameter\":\"required\",\"parameters\":[\"ping\"]}}}");

		SensorBuilderService builderService = new SensorBuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.editControl(updatedControl);
		assertNotNull(savedControl);

		String expectedEventProperties = "[{\"custom\":true,\"name\":\"MOTION\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"{{}}&#176;F\",\"editable\":true,\"display\":true,\"title\":\"Temperature\",\"status\":\"temp\"},{\"formatter\":\"{{}}%\",\"editable\":true,\"display\":true,\"title\":\"Humidity\",\"status\":\"humid\"},{\"formatter\":\"\",\"editable\":true,\"display\":true,\"title\":\"Lux\",\"status\":\"lux\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true}},\"conditional\":{\"GET_STATUS\":{\"parameterKeys\":[\"temp\",\"humid\",\"lux\",\"ping\"],\"parameterRequired\":true}}}";
		
		assertEquals(null, builderService.getControl().getProperties());

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

}
