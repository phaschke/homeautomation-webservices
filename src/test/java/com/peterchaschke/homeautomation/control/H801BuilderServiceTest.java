package com.peterchaschke.homeautomation.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.action.Action;
import com.peterchaschke.homeautomation.action.ActionRepository;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.controls.H801.H801BuilderService;
import com.peterchaschke.homeautomation.event.Event;
import com.peterchaschke.homeautomation.event.EventRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class H801BuilderServiceTest {

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ActionRepository actionRepository;

	@MockBean
	private EventRepository eventRepository;

	@Test
	public void testCreateControl_noProperties() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_led_controller");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New LED Controller");
		control.setType(ControlTypes.LED_CONTROLLER);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(null);
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		H801BuilderService builderService = new H801BuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(1), savedControl.getTopicId());
		assertEquals("new_led_controller", savedControl.getUniqueName());
		assertEquals(1, savedControl.getActive());
		assertEquals(0, savedControl.getDisplayOrder());
		assertEquals("New LED Controller", savedControl.getTitle());
		assertEquals(ControlTypes.LED_CONTROLLER, savedControl.getType());
		assertEquals("tcp//:192.168.0.1:1883", savedControl.getBroker());
		assertEquals("test", savedControl.getSubtopic());
		assertEquals("Test description", savedControl.getDescription());

		String expectedState = "{\"cw_bright\":100,\"ww\":0,\"tunable_bright\":100,\"rgb_g\":0,\"rgb_b\":0,\"ww_bright\":100,\"ww_timer\":0,\"rgb\":0,\"cw_timer\":0,\"tunable_timer\":0,\"tunable\":0,\"cw\":0,\"rgb_r\":255,\"rgb_timer\":0,\"tunable_kelvin\":0,\"rgb_bright\":100}";

		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertTrue(expectedStateJSON.similar(actualStateJSON));

		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"State\",\"status\":\"state\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Timers\",\"status\":\"timers\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"active\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"state\",\"timers\"],\"parameterRequired\":true}}}";
		
		assertNull(builderService.getControl().getProperties());

		assertNull(savedControl.getEventProperties());

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testCreateControl_allModes() {

		ControlResource control = new ControlResource();
		control.setId(1l); // For testing purposes only
		control.setTopicId(1l);
		control.setUniqueName("new_led_controller");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("New LED Controller");
		control.setType(ControlTypes.LED_CONTROLLER);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(
				"{\"led\":{\"ww\":{\"brightness\":true,\"enabled\":true},\"tunable\":{\"brightness\":true,\"maxTemp\":\"7000\",\"enabled\":true,\"minTemp\":\"3000\"},\"cw\":{\"brightness\":true,\"enabled\":true},\"rgb\":{\"brightness\":true,\"enabled\":true}}}");
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		H801BuilderService builderService = new H801BuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.createControl();
		assertNotNull(savedControl);

		String expectedState = "{\"cw_bright\":100,\"ww\":0,\"tunable_bright\":100,\"rgb_g\":0,\"rgb_b\":0,\"ww_bright\":100,\"ww_timer\":0,\"rgb\":0,\"cw_timer\":0,\"tunable_timer\":0,\"tunable\":0,\"cw\":0,\"rgb_r\":255,\"rgb_timer\":0,\"tunable_kelvin\":0,\"rgb_bright\":100}";

		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertTrue(expectedStateJSON.similar(actualStateJSON));

		String expectedProperties = "{\"led\":{\"ww\":{\"brightness\":true,\"enabled\":true},\"tunable\":{\"brightness\":true,\"maxTemp\":\"7000\",\"enabled\":true,\"minTemp\":\"3000\"},\"cw\":{\"brightness\":true,\"enabled\":true},\"rgb\":{\"brightness\":true,\"enabled\":true}}}";
		String expectedEventProperties = "[{\"custom\":false,\"name\":\"TIMER_END_RGB\"},{\"custom\":false,\"name\":\"TIMER_END_TUNABLE\"},{\"custom\":false,\"name\":\"TIMER_END_WW\"},{\"custom\":false,\"name\":\"TIMER_END_CW\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"State\",\"status\":\"state\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Timers\",\"status\":\"timers\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Rgb_brightness\",\"status\":\"rgb_brightness\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Tunable_brightness\",\"status\":\"tunable_brightness\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ww_brightness\",\"status\":\"ww_brightness\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Cw_brightness\",\"status\":\"cw_brightness\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\",\"state.rgb\",\"state.rgb_r\",\"state.rgb_g\",\"state.rgb_b\",\"state.rgb_bright\",\"state.tunable\",\"state.tunable_kelvin\",\"state.tunable_bright\",\"state.ww\",\"state.ww_bright\",\"state.cw\",\"state.cw_bright\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"active\",\"state.rgb\",\"state.rgb_r\",\"state.rgb_g\",\"state.rgb_b\",\"state.rgb_bright\",\"state.tunable\",\"state.tunable_kelvin\",\"state.tunable_bright\",\"state.ww\",\"state.ww_bright\",\"state.cw\",\"state.cw_bright\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"state\",\"timers\",\"rgb_brightness\",\"tunable_brightness\",\"ww_brightness\",\"cw_brightness\"],\"parameterRequired\":true}}}";
		
		JSONObject expectedPropertiesJSON = new JSONObject(expectedProperties);
		JSONObject actualPropertiesJSON = new JSONObject(savedControl.getProperties());
		assertTrue(expectedPropertiesJSON.similar(actualPropertiesJSON));

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}

	@Test
	public void testUpdateControl_allModes() {

		ControlResource control = new ControlResource();
		control.setId(1l);
		control.setTopicId(1l);
		control.setUniqueName("new_led_controller");
		control.setActive(1);
		control.setDisplayOrder(0);
		control.setTitle("LED Controller");
		control.setType(ControlTypes.LED_CONTROLLER);
		control.setBroker("tcp//:192.168.0.1:1883");
		control.setSubtopic("test");
		control.setDescription("Test description");
		control.setProperties(
				"{\"led\":{\"ww\":{\"brightness\":false,\"enabled\":false},\"tunable\":{\"brightness\":false,\"maxTemp\":\"7000\",\"enabled\":false,\"minTemp\":\"3000\"},\"cw\":{\"brightness\":false,\"enabled\":false},\"rgb\":{\"brightness\":false,\"enabled\":false}}}");
		control.setEventProperties(null);
		control.setStatusProperties(null);
		control.setActionProperties(null);

		ControlResource updatedControl = new ControlResource();
		updatedControl.setId(1l);
		updatedControl.setTopicId(2l);
		updatedControl.setUniqueName("new_led_controller1");
		updatedControl.setActive(0);
		updatedControl.setDisplayOrder(69);
		updatedControl.setTitle("New LED Controller 1");
		updatedControl.setType(ControlTypes.LED_CONTROLLER);
		updatedControl.setBroker("tcp//:192.168.0.2:1883");
		updatedControl.setSubtopic("new_test");
		updatedControl.setDescription("Updated test description");
		updatedControl.setProperties("{\"led\":{\"ww\":{\"brightness\":false,\"enabled\":true},\"tunable\":{\"brightness\":true,\"maxTemp\":\"7000\",\"enabled\":true,\"minTemp\":\"3000\"},\"cw\":{\"brightness\":false,\"enabled\":false},\"rgb\":{\"brightness\":false,\"enabled\":true}}}");
		updatedControl.setEventProperties(null);
		updatedControl.setStatusProperties(null);
		updatedControl.setActionProperties(null);

		H801BuilderService builderService = new H801BuilderService(control, controlRepository, actionRepository,
				eventRepository);

		Mockito.when(controlRepository.save(Mockito.any(Control.class))).thenAnswer(i -> i.getArguments()[0]);

		Action action = new Action();
		action.setId(1l);
		action.setThens("test_action");

		Mockito.when(actionRepository.findByNameAndUserCreated(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(action);

		Event event = new Event();

		Mockito.when(eventRepository.findByControlIdAndEventAndUserCreated(Mockito.anyLong(), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(null);
		Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);

		Control savedControl = builderService.editControl(updatedControl);
		assertNotNull(savedControl);

		assertEquals(Long.valueOf(1), savedControl.getId());
		assertEquals(Long.valueOf(2), savedControl.getTopicId());
		assertEquals("new_led_controller1", savedControl.getUniqueName());
		assertEquals(0, savedControl.getActive());
		assertEquals(69, savedControl.getDisplayOrder());
		assertEquals("New LED Controller 1", savedControl.getTitle());
		assertEquals(ControlTypes.LED_CONTROLLER, savedControl.getType());
		assertEquals("tcp//:192.168.0.2:1883", savedControl.getBroker());
		assertEquals("new_test", savedControl.getSubtopic());
		assertEquals("Updated test description", savedControl.getDescription());

		String expectedState = "{\"cw_bright\":100,\"ww\":0,\"tunable_bright\":100,\"rgb_g\":0,\"rgb_b\":0,\"ww_bright\":100,\"ww_timer\":0,\"rgb\":0,\"cw_timer\":0,\"tunable_timer\":0,\"tunable\":0,\"cw\":0,\"rgb_r\":255,\"rgb_timer\":0,\"tunable_kelvin\":0,\"rgb_bright\":100}";

		JSONObject expectedStateJSON = new JSONObject(expectedState);
		JSONObject actualStateJSON = new JSONObject(savedControl.getState());
		assertTrue(expectedStateJSON.similar(actualStateJSON));

		String expectedProperties = "{\"led\":{\"ww\":{\"brightness\":false,\"enabled\":true},\"tunable\":{\"brightness\":true,\"maxTemp\":\"7000\",\"enabled\":true,\"minTemp\":\"3000\"},\"cw\":{\"brightness\":false,\"enabled\":false},\"rgb\":{\"brightness\":false,\"enabled\":true}}}";
		String expectedEventProperties = "[{\"custom\":false,\"name\":\"TIMER_END_RGB\"},{\"custom\":false,\"name\":\"TIMER_END_TUNABLE\"},{\"custom\":false,\"name\":\"TIMER_END_WW\"}]";
		String expectedStatusProperties = "[{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Ping\",\"status\":\"ping\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"State\",\"status\":\"state\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Timers\",\"status\":\"timers\"},{\"formatter\":\"\",\"editable\":false,\"display\":false,\"title\":\"Tunable_brightness\",\"status\":\"tunable_brightness\"}]";
		String expectedActionProperties = "{\"task\":{\"SET_VALUE\":{\"parameterKeys\":[\"state\",\"active\",\"state.rgb\",\"state.rgb_r\",\"state.rgb_g\",\"state.rgb_b\",\"state.tunable\",\"state.tunable_kelvin\",\"state.tunable_bright\",\"state.ww\"],\"parameterRequired\":true},\"TOGGLE_ON\":{\"parameterKeys\":[\"duration\"],\"parameterRequired\":false},\"TOGGLE_OFF\":{\"parameterRequired\":false}},\"conditional\":{\"GET_VALUE\":{\"parameterKeys\":[\"state\",\"active\",\"state.rgb\",\"state.rgb_r\",\"state.rgb_g\",\"state.rgb_b\",\"state.tunable\",\"state.tunable_kelvin\",\"state.tunable_bright\",\"state.ww\"],\"parameterRequired\":true},\"GET_STATUS\":{\"parameterKeys\":[\"ping\",\"state\",\"timers\",\"tunable_brightness\"],\"parameterRequired\":true}}}";
		
		JSONObject expectedPropertiesJSON = new JSONObject(expectedProperties);
		JSONObject actualPropertiesJSON = new JSONObject(savedControl.getProperties());
		assertTrue(expectedPropertiesJSON.similar(actualPropertiesJSON));

		JSONArray expectedEventPropertiesJSON = new JSONArray(expectedEventProperties);
		JSONArray actualEventPropertiesJSON = new JSONArray(savedControl.getEventProperties());
		assertTrue(expectedEventPropertiesJSON.similar(actualEventPropertiesJSON));

		JSONArray expectedStatusPropertiesJSON = new JSONArray(expectedStatusProperties);
		JSONArray actualStatusPropertiesJSON = new JSONArray(savedControl.getStatusProperties());
		assertTrue(expectedStatusPropertiesJSON.similar(actualStatusPropertiesJSON));

		JSONObject expectedActionPropertiesJSON = new JSONObject(expectedActionProperties);
		JSONObject actualActionPropertiesJSON = new JSONObject(savedControl.getActionProperties());
		assertTrue(expectedActionPropertiesJSON.similar(actualActionPropertiesJSON));

	}
}
