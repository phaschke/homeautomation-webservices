package com.peterchaschke.homeautomation.control;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlAction;
import com.peterchaschke.homeautomation.controls.ControlController;
import com.peterchaschke.homeautomation.controls.ControlResource;
import com.peterchaschke.homeautomation.controls.ControlResourceAssembler;
import com.peterchaschke.homeautomation.controls.ControlService;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.controls.InvalidControlActionException;
import com.peterchaschke.homeautomation.users.AppUserDetailsServiceImpl;

@WebMvcTest(ControlController.class)
@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
public class ControlControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private AppUserDetailsServiceImpl appUserDetailsServiceImpl;

	@SpyBean
	private ControlResourceAssembler assembler;

	@MockBean
	private ControlService controlService;

	Control CONTROL_1 = Control.builder().id(1l).topicId(1l).uniqueName("control1").active(1).displayOrder(1)
			.title("Control 1").type(ControlTypes.SWITCH).broker("192.168.0.1").subtopic("test_switch").persistState(1)
			.description("Test description").build();

	Control CONTROL_2 = Control.builder().id(2l).topicId(1l).uniqueName("control2").active(1).displayOrder(2)
			.title("Control 2").type(ControlTypes.SENSOR).broker("192.168.0.1").subtopic("test_sensor").persistState(1)
			.description("Test description 2").build();

	Control CONTROL_3 = Control.builder().id(3l).topicId(2l).uniqueName("control3").active(1).displayOrder(1)
			.title("Control 2").type(ControlTypes.LED_CONTROLLER).broker("192.168.0.1").subtopic("test_led_controller")
			.persistState(1).description("Test description 3").build();

	/*@Test
	public void testGetAllControls() throws Exception {

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2, CONTROL_3));

		Mockito.when(controlService.getAllControls()).thenReturn(controls);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$._embedded.controls", hasSize(3)))
				.andExpect(jsonPath("$._embedded.controls[0]._links.self.href", is("http://localhost/api/controls/1")))
				.andExpect(jsonPath("$._embedded.controls[1]._links.self.href", is("http://localhost/api/controls/2")))
				.andExpect(jsonPath("$._embedded.controls[2]._links.self.href", is("http://localhost/api/controls/3")));
		// .andDo(print());
	}*/

	/*@Test
	public void testGetControlsByTopic_noActiveParam() throws Exception {

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2));
		Optional<Boolean> optionalBoolean = Optional.empty();

		Mockito.when(controlService.getControlsByTopicId(1l, optionalBoolean, optionalBoolean, null)).thenReturn(controls);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls/bytopic/1").contentType(MediaType.APPLICATION_JSON))
				//.andExpect(status().isOk()).andExpect(jsonPath("$._embedded.controls", hasSize(2)))
				//.andExpect(jsonPath("$._embedded.controls[0]._links.self.href", is("http://localhost/api/controls/1")))
				//.andExpect(jsonPath("$._embedded.controls[1]._links.self.href", is("http://localhost/api/controls/2")))
		.andDo(print());
	}*/

	/*@Test
	public void testGetControlsByTopic_withActiveParam() throws Exception {

		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_1, CONTROL_2));
		Optional<Boolean> optionalBoolean = Optional.of(true);
		Pageable pageable = PageRequest.of(0, 20);

		Mockito.when(controlService.getControlsByTopicId(1l, optionalBoolean, optionalBoolean, pageable)).thenReturn(controls);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls/bytopic/1").param("active", "true")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.controls", hasSize(2)))
				.andExpect(jsonPath("$._embedded.controls[0]._links.self.href", is("http://localhost/api/controls/1")))
				.andExpect(jsonPath("$._embedded.controls[1]._links.self.href", is("http://localhost/api/controls/2")));
	}*/

	/*@Test
	public void testGetControlsByTopic_withInvalidTopic() throws Exception {

		Optional<Boolean> optionalBoolean = Optional.of(true);
		Pageable pageable = PageRequest.of(0, 20);

		Mockito.when(controlService.getControlsByTopicId(1l, optionalBoolean, optionalBoolean, pageable))
				.thenThrow(new ResourceNotFoundException("topicId", 1l, "No topics with id found."));

		mockMvc.perform(MockMvcRequestBuilders
				.get("/api/controls/bytopic/1").param("active", "true").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(result -> assertEquals(
						"Resource not found.\nField: topicId\nRejected Value: 1\nMessage: No topics with id found.\n",
						result.getResolvedException().getMessage()));
	}*/

	@Test
	public void testGetOneControl() throws Exception {

		Mockito.when(controlService.getOneControl(Mockito.anyLong())).thenReturn(CONTROL_1);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/controls/1")));
	}

	@Test
	public void testCreateControl() throws Exception {

		ControlResource controlDTO = CONTROL_1.toResource();

		Mockito.when(controlService.createControl(controlDTO)).thenReturn(controlDTO.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isCreated()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.topicId", is(1)))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/controls/1")));
	}

	@Test
	public void testCreateControl_invalidTopicId() throws Exception {

		ControlResource controlDTO = CONTROL_1.toResource();

		Mockito.when(controlService.createControl(Mockito.any(ControlResource.class)))
				.thenThrow(new ResourceNotFoundException("topicId", 1l, "No topics with id found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound())
				.andExpect(result -> assertEquals(
						"Resource not found.\nField: topicId\nRejected Value: 1\nMessage: No topics with id found.\n",
						result.getResolvedException().getMessage()));
	}

	@Test
	public void testCreateControl_uniqueConstraintViolation() throws Exception {

		ControlResource controlDTO = CONTROL_1.toResource();

		Mockito.when(controlService.createControl(Mockito.any(ControlResource.class)))
				.thenThrow(new UniqueConstraintViolationException("uniqueName", controlDTO.getUniqueName(),
						"Control name must be a unique string."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isConflict()).andExpect(
				result -> assertTrue(result.getResolvedException() instanceof UniqueConstraintViolationException))
				.andExpect(result -> assertEquals(
						"Unique constaint violation.\nField: uniqueName\nRejected Value: control1\nMessage: Control name must be a unique string.\n",
						result.getResolvedException().getMessage()));
	}

	@Test
	public void testUpdateControl() throws Exception {

		ControlResource updatedControlDTO = CONTROL_1.toResource();
		updatedControlDTO.setTitle("Updated control");

		Mockito.when(controlService.updateControl(Mockito.any(ControlResource.class)))
				.thenReturn(updatedControlDTO.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedControlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.title", is("Updated control")))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/controls/1")));
	}

	@Test
	public void testUpdateControl_invalidControlId() throws Exception {

		ControlResource updatedControlDTO = CONTROL_1.toResource();
		updatedControlDTO.setTitle("Updated control");

		Mockito.when(controlService.updateControl(Mockito.any(ControlResource.class)))
				.thenThrow(new ResourceNotFoundException("controlId", 1l, "No control with id found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedControlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andExpect(result -> assertEquals(
				"Resource not found.\nField: controlId\nRejected Value: 1\nMessage: No control with id found.\n",
				result.getResolvedException().getMessage()));
	}

	@Test
	public void testUpdateControl_invalidTopicId() throws Exception {

		ControlResource updatedControlDTO = CONTROL_1.toResource();
		updatedControlDTO.setTitle("Updated control");

		Mockito.when(controlService.updateControl(Mockito.any(ControlResource.class)))
				.thenThrow(new ResourceNotFoundException("topicId", 1l, "No topics with id found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedControlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound())
				.andExpect(result -> assertEquals(
						"Resource not found.\nField: topicId\nRejected Value: 1\nMessage: No topics with id found.\n",
						result.getResolvedException().getMessage()));
	}

	@Test
	public void testUpdateControl_uniqueConstraintViolation() throws Exception {

		ControlResource updatedControlDTO = CONTROL_1.toResource();
		updatedControlDTO.setTitle("Updated control");

		Mockito.when(controlService.updateControl(Mockito.any(ControlResource.class)))
				.thenThrow(new UniqueConstraintViolationException("uniqueName", updatedControlDTO.getUniqueName(),
						"Control name must be a unique string."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedControlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isConflict()).andExpect(result -> assertEquals(
				"Unique constaint violation.\nField: uniqueName\nRejected Value: control1\nMessage: Control name must be a unique string.\n",
				result.getResolvedException().getMessage()));
	}

	@Test
	public void testUpdateControl_withNullRequiredValue() throws Exception {

		ControlResource updatedControlDTO = CONTROL_1.toResource();
		updatedControlDTO.setId(null);
		updatedControlDTO.setTopicId(null);
		updatedControlDTO.setTitle("");

		Mockito.when(controlService.updateControl(Mockito.any(ControlResource.class)))
				.thenReturn(updatedControlDTO.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/controls")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedControlDTO));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest());
	}

	@Test
	public void testReorderControls() throws Exception {

		ControlResource CONTROL_DTO_1 = CONTROL_1.toResource();
		ControlResource CONTROL_DTO_2 = CONTROL_2.toResource();

		List<ControlResource> controlDTOs = new ArrayList<>(Arrays.asList(CONTROL_DTO_1, CONTROL_DTO_2));
		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_DTO_1.toEntity(), CONTROL_DTO_2.toEntity()));

		Mockito.when(controlService.updateControlDisplayOrder(controlDTOs)).thenReturn(controls);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/reorder")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlDTOs));

		mockMvc.perform(mockRequest).andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$._embedded.controls[0].displayOrder", is(1)))
				.andExpect(jsonPath("$._embedded.controls[1].displayOrder", is(2)));
	}

	@Test
	public void testReorderControls_withNullRequiredValue() throws Exception {

		ControlResource CONTROL_DTO_1 = CONTROL_1.toResource();
		CONTROL_DTO_1.setUniqueName("");
		ControlResource CONTROL_DTO_2 = CONTROL_2.toResource();
		CONTROL_DTO_2.setId(null);

		List<ControlResource> controlDTOs = new ArrayList<>(Arrays.asList(CONTROL_DTO_1, CONTROL_DTO_2));
		List<Control> controls = new ArrayList<>(Arrays.asList(CONTROL_DTO_1.toEntity(), CONTROL_DTO_2.toEntity()));

		Mockito.when(controlService.updateControlDisplayOrder(controlDTOs)).thenReturn(controls);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/reorder")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlDTOs));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest());
	}

	@Test
	public void testDeleteControl() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.delete("/api/controls/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
	}

	@Test
	public void testDoControlAction() throws Exception {

		ControlResource CONTROL_RESOURCE_1 = CONTROL_1.toResource();

		ControlAction controlAction = new ControlAction();

		Mockito.when(controlService.doAction(Mockito.anyLong(), Mockito.any(ControlAction.class)))
				.thenReturn(CONTROL_RESOURCE_1);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/1/action")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlAction));

		mockMvc.perform(mockRequest).andExpect(status().isOk());

	}

	@Test
	public void testDoControlAction_invalidControlAction() throws Exception {

		ControlAction controlAction = new ControlAction();
		controlAction.setAction("TEST_ACTION");

		Mockito.when(controlService.doAction(Mockito.anyLong(), Mockito.any(ControlAction.class)))
				.thenThrow(new InvalidControlActionException("Control Action", controlAction.getAction(),
						"Invalid control action for Control"));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/1/action")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(controlAction));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest()).andExpect(result -> assertEquals(
				"Invalid Control Action Exception.\nField: Control Action\nRejected Value: TEST_ACTION\nMessage: Invalid control action for Control\n",
				result.getResolvedException().getMessage()));
	}

	@Test
	public void testUpdateContolState() throws Exception {

		ControlResource CONTROL_RESOURCE_1 = CONTROL_1.toResource();

		String state = "";

		Mockito.when(controlService.updateControlState(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(CONTROL_RESOURCE_1);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/1/state")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(state));

		mockMvc.perform(mockRequest).andExpect(status().isOk());

	}

	@Test
	public void testUpdateContolState_invalidControlId() throws Exception {

		String state = "";

		Mockito.when(controlService.updateControlState(Mockito.anyLong(), Mockito.anyString()))
				.thenThrow(new ResourceNotFoundException("controlId", 1l, "No control with id found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/controls/1/state")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(state));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andExpect(result -> assertEquals(
				"Resource not found.\nField: controlId\nRejected Value: 1\nMessage: No control with id found.\n",
				result.getResolvedException().getMessage()));
	}

	@Test
	public void testGetStatus() throws Exception {

		ControlResource CONTROL_RESOURCE_1 = CONTROL_1.toResource();

		Mockito.when(controlService.getStatus(Mockito.anyLong())).thenReturn(CONTROL_RESOURCE_1);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls/1/status").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	@Test
	public void testGetStatus_invalidControlId() throws Exception {

		Mockito.when(controlService.getStatus(Mockito.anyLong()))
				.thenThrow(new ResourceNotFoundException("controlId", 1l, "No control with id found."));

		mockMvc.perform(MockMvcRequestBuilders.get("/api/controls/1/status").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(result -> assertEquals(
						"Resource not found.\nField: controlId\nRejected Value: 1\nMessage: No control with id found.\n",
						result.getResolvedException().getMessage()));
	}
}
