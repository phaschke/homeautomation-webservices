package com.peterchaschke.homeautomation.topic;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.users.AppUserDetailsServiceImpl;

@WebMvcTest(TopicController.class)
@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
public class TopicControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@MockBean
	AppUserDetailsServiceImpl appUserDetailsServiceImpl;

	@SpyBean
	TopicResourceAssembler topicResourceAssembler;

	@MockBean
	TopicService topicService;

	Topic TOPIC_1 = new Topic(1l, "topic1", "Topic 1", 1);
	Topic TOPIC_2 = new Topic(2l, "topic2", "Topic 2", 1);
	Topic TOPIC_3 = new Topic(3l, "topic3", "Topic 3", 1);

	@Test
	public void testGetAllTopics() throws Exception {

		List<Topic> topics = new ArrayList<>(Arrays.asList(TOPIC_1, TOPIC_2, TOPIC_3));

		Mockito.when(topicService.getAllTopicsForDisplay()).thenReturn(topics);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/topics").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.topics", hasSize(3)))
				.andExpect(jsonPath("$._embedded.topics[0]._links.self.href", is("http://localhost/api/topics/1")))
				.andExpect(jsonPath("$._embedded.topics[1]._links.self.href", is("http://localhost/api/topics/2")))
				.andExpect(jsonPath("$._embedded.topics[2]._links.self.href", is("http://localhost/api/topics/3")));
	}

	@Test
	public void testGetTopicById() throws Exception {

		Mockito.when(topicService.getOneTopic(Mockito.anyLong())).thenReturn(TOPIC_1);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/topics/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.topic", is("topic1")))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/topics/1")));
	}

	@Test
	public void testCreateTopic() throws Exception {

		TopicDTO topic = TopicDTO.builder()
				.topic("new_topic")
				.topicTitle("New Topic")
				.active(1)
				.displayOrder(0)
				.build();

		Mockito.when(topicService.addTopic(topic)).thenReturn(topic.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(topic));

		mockMvc.perform(mockRequest).andExpect(status().isCreated())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.topic", is("new_topic")))
				.andExpect(jsonPath("$.topicTitle", is("New Topic")))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/topics/{id}")))
				.andExpect(jsonPath("$._links.self.templated", is(true)));
	}

	@Test
	public void testCreateTopic_withNullRequiredValue() throws Exception {

		TopicDTO topic = TopicDTO.builder().topic(null).topicTitle("Invalid Topic").active(1).displayOrder(0).build();

		Mockito.when(topicService.addTopic(topic)).thenReturn(topic.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(topic));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest());

	}

	@Test
	public void testUpdateTopic() throws Exception {

		TopicDTO updatedTopic = TopicDTO.builder()
				.id(1l)
				.topic("test_topic")
				.topicTitle("Test Topic")
				.active(1)
				.displayOrder(0)
				.build();

		Mockito.when(topicService.updateTopic(updatedTopic)).thenReturn(updatedTopic.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedTopic));

		mockMvc.perform(mockRequest).andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.topic", is("test_topic")))
				.andExpect(jsonPath("$.topicTitle", is("Test Topic")))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/topics/1")));
	}

	@Test
	public void testUpdateTopic_withNullRequiredValue() throws Exception {

		TopicDTO updatedTopic = TopicDTO.builder()
				.id(1l)
				.topic("")
				.topicTitle("")
				.active(1)
				.displayOrder(0)
				.build();

		Mockito.when(topicService.updateTopic(updatedTopic)).thenReturn(updatedTopic.toEntity());

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedTopic));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest());
	}

	@Test
	public void testUpdateTopic_topicNotFound() throws Exception {

		TopicDTO updatedTopic = TopicDTO.builder()
				.id(5l)
				.topic("test_topic")
				.topicTitle("Test Topic")
				.active(1)
				.displayOrder(0)
				.build();

		Mockito.when(topicService.updateTopic(updatedTopic))
				.thenThrow(new ResourceNotFoundException("id", updatedTopic.getId(), "No topics found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedTopic));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound())
				.andExpect(result -> assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
				.andExpect(result -> assertEquals(
						"Resource not found.\nField: id\nRejected Value: 5\nMessage: No topics found.\n",
						result.getResolvedException().getMessage()));
	}
	
	@Test
	public void testUpdateTopic_invalidObjectNameConstraint() throws Exception {

		TopicDTO updatedTopic = TopicDTO.builder()
				.id(5l)
				.topic("test_topic @#$@#d 23$#")
				.topicTitle("Test Topic")
				.active(1)
				.displayOrder(0)
				.build();

		Mockito.when(topicService.updateTopic(updatedTopic))
				.thenThrow(new ResourceNotFoundException("id", updatedTopic.getId(), "No topics found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/topics")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedTopic));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest())
				.andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
	}

	@Test
	public void testReorderTopics() throws Exception {

		TopicDTO TOPIC_DTO_1 = new TopicDTO(1l, 1, 1, "topic1", "Topic 1");
		TopicDTO TOPIC_DTO_2 = new TopicDTO(2l, 1, 0, "topic2", "Topic 2");

		List<TopicDTO> topicDTOs = new ArrayList<>(Arrays.asList(TOPIC_DTO_1, TOPIC_DTO_2));
		List<Topic> topics = new ArrayList<>(Arrays.asList(TOPIC_DTO_1.toEntity(), TOPIC_DTO_2.toEntity()));

		Mockito.when(topicService.updateTopicDisplayOrder(topicDTOs)).thenReturn(topics);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/topics/reorder")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(topicDTOs));

		mockMvc.perform(mockRequest).andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$._embedded.topics[0].displayOrder", is(1)))
				.andExpect(jsonPath("$._embedded.topics[1].displayOrder", is(0)));
		// .andDo(print());
	}

	@Test
	public void testReorderTopics_withNullRequiredValue() throws Exception {

		TopicDTO TOPIC_DTO_1 = new TopicDTO(1l, 1, 1, null, "");
		TopicDTO TOPIC_DTO_2 = new TopicDTO(2l, 1, 0, "", null);

		List<TopicDTO> topicDTOs = new ArrayList<>(Arrays.asList(TOPIC_DTO_1, TOPIC_DTO_2));
		List<Topic> topics = new ArrayList<>(Arrays.asList(TOPIC_DTO_1.toEntity(), TOPIC_DTO_2.toEntity()));

		Mockito.when(topicService.updateTopicDisplayOrder(topicDTOs)).thenReturn(topics);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch("/api/topics/reorder")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(topicDTOs));

		mockMvc.perform(mockRequest).andExpect(status().isBadRequest());

	}
	
	@Test
	public void testDeleteTopic() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders
	            .delete("/api/topics/1")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isNoContent());
	}

}
