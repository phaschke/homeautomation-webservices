package com.peterchaschke.homeautomation.topic;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.users.AppUser;
import com.peterchaschke.homeautomation.users.AppUserRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class TopicServiceTest {
	
	@MockBean
	TopicRepository topicRepository;
	
	@MockBean
	ControlRepository controlRepository;
	
	@MockBean
	AppUserRepository appUserRepository;
	
	@InjectMocks
    TopicServiceImpl topicService;
	
	Topic TOPIC_1 = new Topic(1l, "topic1", "Topic 1", 1);
	Topic TOPIC_2 = new Topic(2l, "topic2", "Topic 2", 1);
	Topic TOPIC_3 = new Topic(3l, "topic3", "Topic 3", 1);
	
	@Test
	public void testGetAllTopicsForDisplay() {
		
		List<Topic> topics = new ArrayList<>(Arrays.asList(TOPIC_1, TOPIC_2, TOPIC_3));
		
		Mockito.when(topicRepository.findAllByOrderByDisplayOrderAsc()).thenReturn(topics);
		Mockito.when(topicRepository.findByActiveAndUsers_IdOrderByDisplayOrderAsc(Mockito.anyInt(), Mockito.anyLong())).thenReturn(topics);
		
		List<Topic> expected = topicService.getAllTopicsForDisplay();
		
		assertThat(expected).isNotNull();
		assertEquals(topics, expected);
	}
	
	@Test
	@WithMockUser(username = "MyHomeGuest", authorities = { "ROLE_GUEST" })
	@WithUserDetails("MyHomeGuest")
	public void testGetAllTopicsForDisplay_guest() {
		
		List<Topic> topics = new ArrayList<>(Arrays.asList(TOPIC_1, TOPIC_2, TOPIC_3));
		AppUser appUser = AppUser.builder()
				.id(1l)
				.password("password")
				.username("username")
				.build();
		
		Mockito.when(topicRepository.findByActiveAndUsers_IdOrderByDisplayOrderAsc(1, 1l)).thenReturn(topics);
		
		Mockito.when(appUserRepository.findByUsername(Mockito.anyString())).thenReturn(appUser);
		
		List<Topic> expected = topicService.getAllTopicsForDisplay();
		
		assertThat(expected).isNotNull();
		assertEquals(topics, expected);
	}
	
	@Test
	public void testGetOneTopic() {
		
		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);
		
		Topic returnedTopic = topicService.getOneTopic(Mockito.anyLong());
		
		assertThat(returnedTopic).isNotNull();
		assertEquals(optionalTopic.get(), returnedTopic);
	}
	
	@Test
	public void testAddTopic() {
		
		Mockito.when(topicRepository.findByTopic(Mockito.anyString())).thenReturn(null);
		Mockito.when(topicRepository.save(Mockito.any(Topic.class))).thenReturn(TOPIC_1);
		
		Topic addedTopic = topicService.addTopic(TOPIC_1.toDTO());

		assertThat(addedTopic).isNotNull();
		assertEquals(TOPIC_1, addedTopic);
	}
	
	@Test
	public void testAddTopic_uniqueConstraintViolation() {
		
		Mockito.when(topicRepository.findByTopic(Mockito.anyString())).thenReturn(TOPIC_1);
		
		assertThrows(UniqueConstraintViolationException.class, () -> {
			topicService.addTopic(TOPIC_1.toDTO());
		});
		
	}
	
	@Test
	public void testUpdateTopic() {
		
		TopicDTO topicToUpdate = TOPIC_1.toDTO();
		topicToUpdate.setTopicTitle("New Title");
		topicToUpdate.setActive(0);
		
		Optional<Topic> optionalTopic = Optional.of(TOPIC_1);
		
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(optionalTopic);
		Mockito.when(topicRepository.findByTopic(Mockito.anyString())).thenReturn(TOPIC_1);
		Mockito.when(topicRepository.save(Mockito.any(Topic.class))).thenReturn(topicToUpdate.toEntity());
		
		Topic updatedTopic = topicService.updateTopic(topicToUpdate);
		
		assertThat(updatedTopic).isNotNull();
		assertEquals(topicToUpdate, updatedTopic.toDTO());
	}
	
	@Test
	public void testUpdateTopic_topicNotFound() {
		
		Optional<Topic> emptyOptional = Optional.empty();
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(emptyOptional);
		
		assertThrows(ResourceNotFoundException.class, () -> {
			topicService.updateTopic(TOPIC_1.toDTO());
		});
	}
	
	@Test
	public void testUpdateTopic_uniqueConstraintViolation() {
		
		Topic topicWithSameTopicButDifferentId = new Topic(7l, "topic1", "Topic 5", 1);
		
		Mockito.when(topicRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(TOPIC_1));
		Mockito.when(topicRepository.findByTopic(Mockito.anyString())).thenReturn(topicWithSameTopicButDifferentId);
		
		assertThrows(UniqueConstraintViolationException.class, () -> {
			topicService.updateTopic(TOPIC_1.toDTO());
		});
	}
	
	@Test
	public void testReorderTopics_topicNotFound() {
		
		TopicDTO TOPIC_DTO_1 = new TopicDTO(1l, 1, 1, "topic1", "Topic 1");
		TopicDTO TOPIC_DTO_2 = new TopicDTO(2l, 1, 0, "topic2", "Topic 2");
		
		List<TopicDTO> topicDTOs = new ArrayList<>(Arrays.asList(TOPIC_DTO_1, TOPIC_DTO_2));
		
		Mockito.when(topicRepository.findById(Mockito.anyLong()))
			.thenReturn(Optional.of(TOPIC_DTO_1.toEntity()))
			.thenReturn(Optional.empty());
		Mockito.when(topicRepository.save(Mockito.any(Topic.class)))
			.thenReturn(TOPIC_DTO_1.toEntity());
		
		assertThrows(ResourceNotFoundException.class, () -> {
			topicService.updateTopicDisplayOrder(topicDTOs);
		});
	}
	
	@Test
	public void testReorderTopics() {
		
		TopicDTO TOPIC_DTO_1 = new TopicDTO(1l, 1, 1, "topic1", "Topic 1");
		TopicDTO TOPIC_DTO_2 = new TopicDTO(2l, 1, 0, "topic2", "Topic 2");
		
		List<TopicDTO> topicDTOs = new ArrayList<>(Arrays.asList(TOPIC_DTO_1, TOPIC_DTO_2));
		
		Mockito.when(topicRepository.findById(Mockito.anyLong()))
			.thenReturn(Optional.of(TOPIC_DTO_1.toEntity()))
			.thenReturn(Optional.of(TOPIC_DTO_2.toEntity()));
		Mockito.when(topicRepository.save(Mockito.any(Topic.class)))
			.thenReturn(TOPIC_DTO_1.toEntity())
			.thenReturn(TOPIC_DTO_2.toEntity());
		
		List<Topic> savedTopics = topicService.updateTopicDisplayOrder(topicDTOs);
		
		assertThat(savedTopics).isNotNull();
		assertEquals(2, savedTopics.size());
		assertEquals(TOPIC_DTO_1.getTopic(), savedTopics.get(0).getTopic());
		assertEquals(TOPIC_DTO_2.getTopic(), savedTopics.get(1).getTopic());
	}	
	
	@Test
	public void testDeleteTopic() {
		
		List<Control> controlsToBeRemoved = new ArrayList<Control>();
		Mockito.when(controlRepository.findAllByTopicIdOrderByDisplayOrderAsc(Mockito.anyLong())).thenReturn(controlsToBeRemoved);
		
		topicService.deleteTopic(1l);
	}
}
