package com.peterchaschke.homeautomation.action;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(SpringExtension.class)
public class ActionParserTest {

	@Test
	public void testParseAction() throws Exception {
		
		ActionResource actionResource = new ActionResource();
		actionResource.setId(1l);
		actionResource.setName("test_action");
		actionResource.setTitle("Test Action");
		actionResource.setUserCreated(1);
		actionResource.setIfExecution(ActionExecution.SERIAL);
		actionResource.setThenExecution(ActionExecution.SERIAL);
		actionResource.setElseExecution(ActionExecution.SERIAL);
		actionResource.setIfs("[{\"value\":\"0\",\"conditional\":\"GET_STATUS\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameter\":\"relay\",\"comparator\":\"STR_EQ\"}]");
		actionResource.setThens("[{\"action\":\"TOGGLE_ON\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"order\":0,\"parameters\":[{\"key\":\"duration\",\"value\":\"30\"}]}]");
		actionResource.setElses("[{\"action\":\"TOGGLE_OFF\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameters\":[]}]");
		
		ParsedAction parsedActionExpected = ActionTestHelper.getParsedAction_Controls();
		
		ParsedAction parsedActionActual = ActionParser.parseAction(actionResource);
		
		assertEquals(parsedActionExpected, parsedActionActual);
	}
	
	// TODO: Add test for action task involving an event ActionObjectType
	
	// TODO: Add test for action task involving an notifier ActionObjectType
}
