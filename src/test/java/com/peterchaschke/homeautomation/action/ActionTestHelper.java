package com.peterchaschke.homeautomation.action;

import java.util.ArrayList;

public class ActionTestHelper {
	
	public static ParsedAction getParsedAction_Controls() {
		
		ArrayList<ParsedActionConditional> conditionals = new ArrayList<ParsedActionConditional>();
		ParsedActionConditional parsedActionConditional = new ParsedActionConditional();
		parsedActionConditional.setObjectId(17l);
		parsedActionConditional.setType(ActionObjectType.CONTROL);
		parsedActionConditional.setConditional("GET_STATUS");
		parsedActionConditional.setValue("0");
		parsedActionConditional.setParameter("relay");
		parsedActionConditional.setComparator("STR_EQ");
		parsedActionConditional.setOrder(0);
		conditionals.add(parsedActionConditional);
		
		ArrayList<ParsedActionTask> thens = new ArrayList<ParsedActionTask>();
		ParsedActionTask thenTask = new ParsedActionTask();
		thenTask.setObjectId(17l);
		thenTask.setAction("TOGGLE_ON");
		thenTask.setObjectType(ActionObjectType.CONTROL);
		thenTask.setOrder(0);
		
		ArrayList<ParsedActionParameter> thenParameters = new ArrayList<ParsedActionParameter>();
		ParsedActionParameter parsedThenActionParameter = new ParsedActionParameter();
		parsedThenActionParameter.setKey("duration");
		parsedThenActionParameter.setValue("30");
		thenParameters.add(parsedThenActionParameter);
		thenTask.setParameters(thenParameters);
		
		thens.add(thenTask);
		
		ArrayList<ParsedActionTask> elses = new ArrayList<ParsedActionTask>();
		ParsedActionTask elseTask = new ParsedActionTask();
		elseTask.setObjectId(17l);
		elseTask.setAction("TOGGLE_OFF");
		elseTask.setObjectType(ActionObjectType.CONTROL);
		elseTask.setOrder(0);
		ArrayList<ParsedActionParameter> elseParameters = new ArrayList<ParsedActionParameter>();
		elseTask.setParameters(elseParameters);
		elses.add(elseTask);
		
		ParsedAction parsedAction = new ParsedAction();
		parsedAction.setConditionals(conditionals);
		parsedAction.setThens(thens);
		parsedAction.setElses(elses);
		
		return  parsedAction;
	}
	
	public static ActionResource getActionResourceWithParsedAction_Controls() {
		
		ActionResource actionResource = new ActionResource();
		actionResource.setId(1l);
		actionResource.setName("test_action");
		actionResource.setTitle("Test Action");
		actionResource.setUserCreated(1);
		actionResource.setIfExecution(ActionExecution.SERIAL);
		actionResource.setThenExecution(ActionExecution.PARALLEL);
		actionResource.setElseExecution(ActionExecution.SERIAL);
		actionResource.setParsedAction(getParsedAction_Controls());
		
		return actionResource;
	}
	
	public static Action getAction1() {
		
		return new Action(1l, "action_test_1", "Action 1", "Description 1", ActionExecution.SERIAL,
				ActionExecution.PARALLEL, ActionExecution.SERIAL,
				"[{\"value\":\"0\",\"conditional\":\"GET_STATUS\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameter\":\"relay\",\"comparator\":\"STR_EQ\"}]",
				"[{\"action\":\"TOGGLE_ON\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameters\":[]}]",
				"[{\"action\":\"TOGGLE_OFF\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameters\":[]}]", 1);
	}
	
	public static Action getAction2() {
		
		return new Action(2l, "action_test_1", "Action 1", "Description 1", ActionExecution.PARALLEL,
				ActionExecution.PARALLEL, ActionExecution.PARALLEL,
				"[{\"value\":\"0\",\"conditional\":\"GET_STATUS\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameter\":\"relay\",\"comparator\":\"STR_EQ\"}]",
				"[{\"action\":\"TOGGLE_ON\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameters\":[]}]", null, 1);
	}
	
	public static Action getAction3() {
		return new Action(3l, "action_test_1", "Action 1", "Description 1", ActionExecution.SERIAL,
				ActionExecution.SERIAL, ActionExecution.SERIAL, null,
				"[{\"action\":\"TOGGLE_ON\",\"objectId\":\"17\",\"objectType\":\"CONTROL\",\"parameters\":[]}]", null, 1);
	}

}
