package com.peterchaschke.homeautomation.action;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ActionServiceTest {
	
	@MockBean
	private ActionRepository actionRepository;
	
	@MockBean
	private ActionValidator actionValidator;
	
	@MockBean
	private ActionExecutor actionExecutor;
	
	@InjectMocks
	private ActionServiceImpl actionService;
	
	Action ACTION_1 = ActionTestHelper.getAction1();
	Action ACTION_2 = ActionTestHelper.getAction2();
	Action ACTION_3 = ActionTestHelper.getAction3();
	
	@Test
	public void testGetActions_noFilters() {
		
		List<Action> actions = new ArrayList<>(Arrays.asList(ACTION_1, ACTION_2, ACTION_3));
		List<ActionResource> expectedActions = new ArrayList<>(Arrays.asList(ACTION_1.toResource(), ACTION_2.toResource(), ACTION_3.toResource()));

		Mockito.when(actionRepository.findByUserCreatedOrderByTitleAsc(Mockito.anyInt())).thenReturn(actions);
		
		HashMap<String, String> filters = new HashMap<String, String>();
		
		List<ActionResource> actualActions = actionService.getActions(filters);

		assertThat(actualActions).isNotNull();
		assertEquals(expectedActions, actualActions);
	}
	
	@Test
	public void testGetActions_userCreatedFilter() {
		
		List<Action> actions = new ArrayList<>(Arrays.asList(ACTION_1, ACTION_2, ACTION_3));
		List<ActionResource> expectedActions = new ArrayList<>(Arrays.asList(ACTION_1.toResource(), ACTION_2.toResource(), ACTION_3.toResource()));

		Mockito.when(actionRepository.findByOrderByTitleAsc()).thenReturn(actions);
		
		HashMap<String, String> filters = new HashMap<String, String>();
		filters.put("userCreated", "true");
		
		List<ActionResource> actualActions = actionService.getActions(filters);

		assertThat(actualActions).isNotNull();
		assertEquals(expectedActions, actualActions);
	}
	
	@Test
	public void testGetAction() {
		
		Optional<Action> optionalAction = Optional.of(ACTION_1);
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(optionalAction);

		ActionResource actualAction = actionService.getAction(Mockito.anyLong());

		assertThat(actualAction).isNotNull();
		assertEquals(ACTION_1.toResource(), actualAction);
	}
	
	@Test
	public void testGetAction_actionNotFound() {
		
		Optional<Action> emptyOptionalAction = Optional.empty();
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(emptyOptionalAction);

		assertThrows(ResourceNotFoundException.class, () -> {
			actionService.getAction(Mockito.anyLong());
		});
	}
	
	@Test
	public void testCreateAction() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();

		Mockito.when(actionRepository.findByName(Mockito.anyString())).thenReturn(null);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(ACTION_1);

		ActionResource actualControl = actionService.createAction(actionResource);

		assertThat(actualControl).isNotNull();
	}
	
	@Test
	public void testCreateAction_uniqueConstraintViolation() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();

		Mockito.when(actionRepository.findByName(Mockito.anyString())).thenReturn(ACTION_1);

		assertThrows(UniqueConstraintViolationException.class, () -> {
			actionService.createAction(actionResource);
		});
	}
	
	@Test
	public void testUpdateAction() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();
		actionResource.setTitle("Updated Title");

		Optional<Action> optionalAction = Optional.of(ACTION_1);
		
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(optionalAction);
		Mockito.when(actionRepository.findByName(Mockito.anyString())).thenReturn(ACTION_1);
		Mockito.when(actionRepository.save(Mockito.any(Action.class))).thenReturn(actionResource.toEntity());

		ActionResource actualAction = actionService.updateAction(1l, actionResource);

		assertThat(actualAction).isNotNull();
		assertEquals(actionResource.getTitle(), actualAction.getTitle());
	}
	
	@Test
	public void testUpdateAction_actionNotFound() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();
		actionResource.setTitle("Updated Title");
		
		Optional<Action> optionalEmptyAction = Optional.empty();
		
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(optionalEmptyAction);

		assertThrows(ResourceNotFoundException.class, () -> {
			actionService.updateAction(1l, actionResource);
		});
	}
	
	@Test
	public void testUpdateAction_uniqueConstraintViolation() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();
		actionResource.setTitle("Updated Title");

		Optional<Action> optionalAction = Optional.of(ACTION_1);
		
		Action conflictingAction = ACTION_2;
		
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(optionalAction);
		Mockito.when(actionRepository.findByName(Mockito.anyString())).thenReturn(conflictingAction);

		assertThrows(UniqueConstraintViolationException.class, () -> {
			actionService.updateAction(1l, actionResource);
		});
	}
	
	@Test
	public void testDeleteControl() {

		actionService.deleteAction(Mockito.anyLong());
	}
	
	@Test
	public void testExecuteAction() throws Exception {
		
		Optional<Action> optionalAction = Optional.of(ACTION_1);
		Mockito.when(actionRepository.findById(Mockito.anyLong())).thenReturn(optionalAction);
		
		ExecutableAction executableAction = new ExecutableAction();
		Mockito.when(actionValidator.validateAndBuildExecutableAction(Mockito.any(ActionResource.class))).thenReturn(executableAction);
		
		ActionExecutionResult actionExecutionResult = new ActionExecutionResult();
		Mockito.when(actionExecutor.executeAction(Mockito.any(ExecutableAction.class), Mockito.any(ActionResource.class))).thenReturn(actionExecutionResult);
		
		ActionExecutionResult result = actionService.executeAction(1l, true);
		
		assertNotNull(result);
	}
	
}
