package com.peterchaschke.homeautomation.action;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.controls.BaseControlService;
import com.peterchaschke.homeautomation.controls.Control;
import com.peterchaschke.homeautomation.controls.ControlRepository;
import com.peterchaschke.homeautomation.controls.ControlServiceFactory;
import com.peterchaschke.homeautomation.controls.ControlTypes;
import com.peterchaschke.homeautomation.event.EventRepository;

@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ActionValidatorTest {

	@MockBean
	private ControlRepository controlRepository;

	@MockBean
	private ControlServiceFactory serviceFactory;

	@MockBean
	private EventRepository eventRepository;

	@InjectMocks
	private ActionValidator actionValidator;

	@Test
	public void testValidateAction_controlActionType() throws Exception {

		ParsedAction parsedAction = ActionTestHelper.getParsedAction_Controls();

		Mockito.when(controlRepository.existsById(Mockito.anyLong())).thenReturn(true);

		actionValidator.validateAction(parsedAction);
	}

	@Test
	public void testValidateAction_invalidControlId() throws Exception {

		ParsedAction parsedAction = ActionTestHelper.getParsedAction_Controls();

		Mockito.when(controlRepository.existsById(Mockito.anyLong())).thenReturn(false);

		assertThrows(ResourceNotFoundException.class, () -> {
			actionValidator.validateAction(parsedAction);
		});
	}

	@Test
	public void testValidateAndBuildExecutableAction_nullParsedAction() {

		ActionResource actionResource = new ActionResource();

		assertThrows(Exception.class, () -> {
			actionValidator.validateAndBuildExecutableAction(actionResource);
		});
	}

	@Test
	public void testValidateAndBuildExecutableAction() throws Exception {

		Control control = Control.builder().id(17l).topicId(1l).uniqueName("control1").active(1).displayOrder(1)
				.title("Control 1").type(ControlTypes.SWITCH).broker("192.168.0.1").subtopic("test_switch")
				.persistState(1).description("Test description").build();

		Optional<Control> optControl = Optional.of(control);

		ActionResource actionResource = ActionTestHelper.getActionResourceWithParsedAction_Controls();

		Mockito.when(controlRepository.findById(Mockito.anyLong())).thenReturn(optControl);

		BaseControlService serviceMock = mock(BaseControlService.class);
		Mockito.when(serviceFactory.createControlService(Mockito.any(Control.class))).thenReturn(serviceMock);

		ExecutableAction executableAction = actionValidator.validateAndBuildExecutableAction(actionResource);

		assertNotNull(executableAction);

	}
	
	// TODO: Add validate and andlidateAndBuildExecutable action for Event ActionObjectType
	
	// TODO: Add validate and andlidateAndBuildExecutable action for Notifier ActionObjectType
}
