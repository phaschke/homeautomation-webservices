package com.peterchaschke.homeautomation.action;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peterchaschke.homeautomation.ResourceNotFoundException;
import com.peterchaschke.homeautomation.UniqueConstraintViolationException;
import com.peterchaschke.homeautomation.users.AppUserDetailsServiceImpl;

@WebMvcTest(ActionController.class)
@WithMockUser(username = "MyHomeAdmin", authorities = { "ROLE_ADMIN" })
@WithUserDetails("MyHomeAdmin")
public class ActionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private AppUserDetailsServiceImpl appUserDetailsServiceImpl;

	@SpyBean
	ActionResourceAssembler actionResourceAssembler;

	@MockBean
	ActionService actionService;

	Action ACTION_1 = ActionTestHelper.getAction1();
	Action ACTION_2 = ActionTestHelper.getAction2();
	Action ACTION_3 = ActionTestHelper.getAction3();
	
	@Test
	public void testGetAllActions() throws Exception {

		List<ActionResource> actions = new ArrayList<>(Arrays.asList(ACTION_1.toResource(), ACTION_2.toResource(), ACTION_3.toResource()));
		
		HashMap<String, String> filters = new HashMap<String, String>();

		Mockito.when(actionService.getActions(filters)).thenReturn(actions);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/actions").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$._embedded.actions", hasSize(3)))
				.andExpect(jsonPath("$._embedded.actions[0]._links.self.href", is("http://localhost/api/actions/1")))
				.andExpect(jsonPath("$._embedded.actions[1]._links.self.href", is("http://localhost/api/actions/2")))
				.andExpect(jsonPath("$._embedded.actions[2]._links.self.href", is("http://localhost/api/actions/3")));
		// .andDo(print());
	}

	@Test
	public void testGetOneAction() throws Exception {

		Mockito.when(actionService.getAction(Mockito.anyLong())).thenReturn(ACTION_1.toResource());

		mockMvc.perform(MockMvcRequestBuilders.get("/api/actions/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/actions/1")));
	}
	
	@Test
	public void testGetOneAction_actionNotFound() throws Exception {

		Mockito.when(actionService.getAction(Mockito.anyLong())).thenThrow(new ResourceNotFoundException("actionId", 1l, "No actions found."));

		mockMvc.perform(MockMvcRequestBuilders.get("/api/actions/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}
	
	@Test
	public void testCreateAction() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();

		Mockito.when(actionService.createAction(Mockito.any(ActionResource.class))).thenReturn(actionResource);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/actions")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(actionResource));

		mockMvc.perform(mockRequest).andExpect(status().isCreated()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/actions/1")));
	}
	
	@Test
	public void testCreateAction_uniqueConstraintViolation() throws Exception {
		
		ActionResource actionResource = ACTION_1.toResource();

		Mockito.when(actionService.createAction(Mockito.any(ActionResource.class)))
				.thenThrow(new UniqueConstraintViolationException("name", actionResource.getName(),
						"Action name must be a unique string."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/actions")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(actionResource));

		mockMvc.perform(mockRequest).andExpect(status().isConflict()).andExpect(
				result -> assertTrue(result.getResolvedException() instanceof UniqueConstraintViolationException))
				.andExpect(result -> assertEquals(
						"Unique constaint violation.\nField: name\nRejected Value: action_test_1\nMessage: Action name must be a unique string.\n",
						result.getResolvedException().getMessage()));
	}
	
	@Test
	public void testUpdateAction() throws Exception {

		ActionResource updatedActionResource = ACTION_1.toResource();
		updatedActionResource.setTitle("Updated action");

		Mockito.when(actionService.updateAction(Mockito.anyLong(), Mockito.any(ActionResource.class)))
				.thenReturn(updatedActionResource);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/actions/1")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedActionResource));

		mockMvc.perform(mockRequest).andExpect(status().isOk()).andExpect(jsonPath("$", notNullValue()))
				.andExpect(jsonPath("$.title", is("Updated action")))
				.andExpect(jsonPath("$._links.self.href", is("http://localhost/api/actions/1")));
	}
	
	@Test
	public void testUpdateAction_invalidId() throws Exception {

		ActionResource updatedActionResource = ACTION_1.toResource();
		updatedActionResource.setTitle("Updated action");

		Mockito.when(actionService.updateAction(Mockito.anyLong(), Mockito.any(ActionResource.class)))
				.thenThrow(new ResourceNotFoundException("actionId", 1l, "No action with id found."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/actions/1")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedActionResource));

		mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andExpect(result -> assertEquals(
				"Resource not found.\nField: actionId\nRejected Value: 1\nMessage: No action with id found.\n",
				result.getResolvedException().getMessage()));
	}
	
	@Test
	public void testUpdateAction_uniqueConstraintViolation() throws Exception {

		ActionResource updatedActionResource = ACTION_1.toResource();
		updatedActionResource.setTitle("Updated action");


		Mockito.when(actionService.updateAction(Mockito.anyLong(), Mockito.any(ActionResource.class)))
				.thenThrow(new UniqueConstraintViolationException("name", updatedActionResource.getName(),
						"Action name must be a unique string."));

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/actions/1")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(updatedActionResource));

		mockMvc.perform(mockRequest).andExpect(status().isConflict()).andExpect(result -> assertEquals(
				"Unique constaint violation.\nField: name\nRejected Value: action_test_1\nMessage: Action name must be a unique string.\n",
				result.getResolvedException().getMessage()));
	}
	
	@Test
	public void testDeleteAction() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.delete("/api/actions/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());
	}
	
	@Test
	public void testExecuteAction() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.patch("/api/actions/execute/{id}", Mockito.anyLong())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void testTestAction() throws Exception {
		
		ActionExecutionResult actionResult = new ActionExecutionResult();
		actionResult.setActionId(1l);
		actionResult.setSuccessFlag(true);
		actionResult.appendLog("Test");
		
		Mockito.when(actionService.executeAction(1l, true)).thenReturn(actionResult);
		
		mockMvc.perform(MockMvcRequestBuilders.patch("/api/actions/test/1")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}
